dist = load('diagnostic/dist_ntime/dist_pp_time_1.dat');

nth = 30
npsi = 10
ith = 1;
%ipsi = 1
%i1start = (ipsi-1)*nth + ith;
%i1end = (ipsi)*nth+ith-1;

f = figure;
%pause
%set(f, "visible", "off")

%graphics_toolkit gnuplot
%figure('Visible','off')
%figure(f, "visible", "off");
%set(gca,'XTick')
%set(gca,'XTickLabel','')
set(gca,'XTick',-pi:pi:pi)
%set xtics('-pi' -pi,0,'pi,' pi)
set(gca,'XTickLabel',{'-pi','0','pi'})


hold on
for ipsi = 1: npsi
   i1start = (ipsi-1)*nth + ith;
   i1end = (ipsi)*nth+ith-1;


%scatter3(dist(i1start:i1end,3),dist(i1start:i1end,4),dist(i1start:i1end,5),'k.','MarkerSize', 4)
%scatter3(dist(i1start:i1end,8),dist(i1start:i1end,9),dist(i1start:i1end,10), '.','color',[1.0,0.0,0.0],'MarkerSize', 4)
%scatter3(dist(i1start:i1end,13),dist(i1start:i1end,14),dist(i1start:i1end,15),'.','color',[0.2,0.0,0.0],'MarkerSize', 4)
%scatter3(dist(i1start:i1end,18),dist(i1start:i1end,19),dist(i1start:i1end,20),'.','color',[0.0,1.0,0.0],'MarkerSize', 4)
%scatter3(dist(i1start:i1end,23),dist(i1start:i1end,24),dist(i1start:i1end,25),'.','color',[0.0,0.2,0.0],'MarkerSize', 4)
%
   scatter3(dist(i1start:i1end,3),dist(i1start:i1end,4),dist(i1start:i1end,5),'filled')
   scatter3(dist(i1start:i1end,8),dist(i1start:i1end,9),dist(i1start:i1end,10), 'filled')
   scatter3(dist(i1start:i1end,13),dist(i1start:i1end,14),dist(i1start:i1end,15),'filled')
   scatter3(dist(i1start:i1end,18),dist(i1start:i1end,19),dist(i1start:i1end,20),'filled')
   scatter3(dist(i1start:i1end,23),dist(i1start:i1end,24),dist(i1start:i1end,25),'filled')


endfor

%hold on
%do i = 1:1
%do i = 1:1

 % plot(dist(i1start:i1end,3),dist(i1start:i1end,5),'ko')
 % plot(dist(i1start:i1end,8),dist(i1start:i1end,10), 'x','color',[1.0,0.0,0.0])
 % plot(dist(i1start:i1end,13),dist(i1start:i1end,15),'+','color',[0.5,0.0,0.0])
 % plot(dist(i1start:i1end,18),dist(i1start:i1end,20),'x','color',[0.0,1.0,0.0])
 % plot(dist(i1start:i1end,23),dist(i1start:i1end,25),'+','color',[0.0,0.5,0.0])
%
% plot(dist(i1start:i1end,3),dist(i1start:i1end,5),'k.','MarkerSize', 4)
% plot(dist(i1start:i1end,8),dist(i1start:i1end,10), '.','color',[1.0,0.0,0.0],'MarkerSize', 4)
% plot(dist(i1start:i1end,13),dist(i1start:i1end,15),'.','color',[0.2,0.0,0.0],'MarkerSize', 4)
% plot(dist(i1start:i1end,18),dist(i1start:i1end,20),'.','color',[0.0,1.0,0.0],'MarkerSize', 4)
% plot(dist(i1start:i1end,23),dist(i1start:i1end,25),'.','color',[0.0,0.2,0.0],'MarkerSize', 4)
%

 %plot(dist(i1start,3),dist(i1start,5),'ko')
 %plot(dist(i1start,8),dist(i1start,10), 'o','color',[1.0,0.0,0.0])
 %plot(dist(i1start,13),dist(i1start,15),'o','color',[0.5,0.0,0.0])
 %plot(dist(i1start,18),dist(i1start,20),'o','color',[0.0,1.0,0.0])
 %plot(dist(i1start,23),dist(i1start,25),'o','color',[0.0,0.5,0.0])
 
 %print -depsc diagnostic/dist_pp_time_1.eps
 
 %set(f, 'MarkerSize', 5)
%end do
legend('grid','alpha','alpha 2', 'beta', 'beta 2')

hold off

%set(gca,'XTickLabel','')
 %set(gca,'XTick',-pi:pi:pi)
 %set(gca,'XTickLabel',{'-pi','0','pi'})

 print -depsc diagnostic/dist_pp_time_1.eps

%pause
