%Plot the cfs of the grid

true = load('./diagnostic/geom_g_true.dat');
false = load('./diagnostic/geom_g_false.dat');
cfs = load('geom_g_cfs.dat');

figure

hold on
g1 = plot (true(:,1),true(:,2),'b@')
g2 = plot (cfs(:,2),cfs(:,3),'k@')
g3 = plot (false(:,1),false(:,2),'rx', "markersize", 5)
hold off

xlabel('R')
ylabel('Z')

L = legend([g2,g1,g3],'closed flux surface','open flux surface','ghost points');

print -depsc diagnostic/geom_g.eps
