## grid = load('geom_g.dat');

graphics_toolkit('gnuplot')

ntime = 20

for i = 0: ntime
  i
  filename = sprintf('dist_time_%d.dat',i);
  filepath = sprintf('diagnostic/dist_ntime/%s',filename);
  filepath
  dist = load(filepath);
  

  fig = figure('visible','off');

  scatter(dist(:,1),dist(:,2),4,dist(:,3),'filled');
  colorbar

  xlabel('R')
  ylabel('Z')

  name = sprintf('diagnostic/dist_ntime/dist_time_%d.eps',i);
 
  %print -dp
  print (fig, name, '-depsc')% diagnostic/grid_circular.eps
  
end
