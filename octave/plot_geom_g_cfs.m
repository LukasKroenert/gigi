%Plot the cfs of the grid

cfs = load('geom_g_cfs.dat');

figure

plot (cfs(:,2),cfs(:,3),'k@')

xlabel('R')
ylabel('Z')

print -depsc diagnostic/geom_g_cfs.eps

