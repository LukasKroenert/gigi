g = load('geom_g_test.dat');

pointsize = 3;
hold on
plot(g(:,1),g(:,2),'k.','markersize',pointsize)
plot(g(:,3),g(:,4),'.','markersize',pointsize,'color',[0 1 0])
plot(g(:,5),g(:,6),'.','markersize',pointsize,'color',[0 0.8 0])
plot(g(:,7),g(:,8),'.','markersize',pointsize,'color',[0 0 1])
plot(g(:,9),g(:,10),'.','markersize',pointsize,'color',[0 0 0.8])

plot(g(1,1),g(1,2),'rx','markersize',10)
plot(g(1,3),g(1,4),'x','markersize',10,'color',[0 1.0 0])
plot(g(1,5),g(1,6),'x','markersize',10,'color',[0 0.8 0])
plot(g(1,7),g(1,8),'x','markersize',10, 'color',[0 0 1])
plot(g(1,9),g(1,10),'x','markersize',10,'color',[0 0 1]) 
hold off
## axis("equal");
legend('grid','alpha','alpha_2','beta','beta_2')

print -color -depsc geom_g_test.eps
