module diagnostic

  implicit none

  private

  public :: diagnostic_init, diagnostic_initial_output, diagnostic_naverage

contains

subroutine diagnostic_init

  use diagnos_dist, only : diagnos_dist_init

  call diagnostic_check_folder()
  call diagnostic_check_folder('dist_ntime')
  call diagnostic_check_folder('dist_fft')

  call diagnos_dist_init

end subroutine diagnostic_init

subroutine diagnostic_initial_output

  use diagnos_dist, only : diagnos_dist_ntime,diagnos_dist_modes, &
     & diagnos_dist_sum_naverage, diagnos_dist_sum_calc,diagnos_dist_max_naverage
  use diagnos_geom, only : diagnos_geom_g_pfs

  call diagnos_geom_g_pfs

  call diagnos_dist_ntime
  ! call diagnos_dist_modes
  call diagnos_dist_sum_calc
  call diagnos_dist_sum_naverage
  call diagnos_dist_max_naverage

end subroutine diagnostic_initial_output

subroutine diagnostic_naverage

  use diagnos_dist, only : diagnos_dist_ntime, diagnos_dist_modes, &
     & diagnos_dist_sum_naverage,diagnos_dist_sum_calc, diagnos_dist_max_naverage
  use control,      only : open_status

  open_status = 'old'

  call diagnos_dist_ntime
  ! call diagnos_dist_modes
  call diagnos_dist_sum_calc
  call diagnos_dist_sum_naverage
  call diagnos_dist_max_naverage

end subroutine diagnostic_naverage

subroutine diagnostic_check_folder(folder_name)

  character (len = *), optional, intent(in) :: folder_name
  character (len = 128) :: total_path

  logical :: dir_diagnostic
  logical :: dir_total_path

  character (len = 20) :: folder_diagnostic
  folder_diagnostic = './diagnostic'

  inquire(file=folder_diagnostic, exist=dir_diagnostic)

  if ( dir_diagnostic ) then
  else
    call system('mkdir diagnostic')
    write(*,*) "folder diagnostic created!"
  end if

  if(present(folder_name))then
    total_path = trim(folder_diagnostic)//'/'//trim(folder_name)
    inquire(file=total_path,exist=dir_total_path)
    if (dir_total_path) then
    else
      call system('mkdir '// total_path)
      write(*,*) 'folder '//trim(total_path)//' created!'
    end if
  end if

end subroutine diagnostic_check_folder

end module diagnostic
