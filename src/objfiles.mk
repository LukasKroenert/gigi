#############################
### Gigi object files.
############################

F90OBJ = general.o constants.o global.o control.o index_function.o fft.o\
         mpiinterface.o geom.o \
         dist.o structures.o \
         matdat.o linear_terms.o interpolation.o \
         diagnos_dist.o diagnos_geom.o diagnostic.o exp_integration.o\
         init.o\
         main.o

## objects required for the main program
OBJLIST = $(F90OBJ)
