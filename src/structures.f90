!-------------------------------------------------------------------------------
!> Module that defines and stores structures used in GKW.
!-------------------------------------------------------------------------------
module structures

  implicit none

  private

  public :: matrix_element

  !> Element datatype
  type :: matrix_element
    integer :: iphi     !< the toroidal grid index
    integer :: ith      !< the poloidal grid index
    integer :: ipsi     !< the radial grid index
    integer :: ipp      !< Index for the penetration point. 0 = acual grid,
                        !< 1 = alpha_1, 2 = alpha_2, -1 = beta_1, -2 = beta_2
    complex :: val      !< The value of the element
    integer :: iphiloc  !< the desired location of the toroidal direction
    integer :: ithloc   !< the desired location of the poloidal direction
    integer :: ipsiloc  !< the desired location of the radial direction
    integer :: ipploc   !< the desired location of the penetration point
    !> the name of the term that put the element
    character(len=64) :: term =  ''
    !> Set true once term is registered with matdat
    logical :: registered = .false.
  end type matrix_element

end module structures
