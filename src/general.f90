!*****************************************************************************
!> General purpose routines, having no dependencies other than things
!> in mpiinterface, are (or should be) in here. Perhaps we could put
!> root_and_verbose into a block. Filename generators, abort and warning messages.
!*****************************************************************************

module general

  implicit none

  private

  public :: general_init
  public :: gkw_abort

  contains

!-----------------------------------------------------------------------------
!> initialize anything for general
!-----------------------------------------------------------------------------
subroutine general_init()

end subroutine general_init

!-----------------------------------------------------------------------------
!> Abort the code safely (mpiabort) with an optional error message.
!> This routine can be used to abort from a module higher in the hierarchy
!> than IO.
!-----------------------------------------------------------------------------
subroutine gkw_abort(abort_message)
  !use mpiinterface, only : processor_number
  !use io, only : finalize_and_abort
  
  character (len=*),intent(in), optional :: abort_message

  if (present(abort_message)) then
    write (*,'(A,A,A,I4)') ' GKW_ABORT -- ', abort_message,                  &
                         & '  '!, processor_number
  else
    write (*,'(A,A,A,I4)') ' GKW_ABORT -- ', '(no message given!)',          &
                         & '  '!, processor_number
  end if
  stop
!  call finalize_and_abort

end subroutine gkw_abort

end module general
