module diagnos_geom

  implicit none

  private

  public :: diagnos_geom_g_pfs

contains

subroutine diagnos_geom_g_pfs

  use control,        only : open_status
  use geom,           only : gx, gy
  use index_function, only : indx_gp
  use dist,           only : ntot_geom

  integer :: i
  character(len = 128) :: file_name

  file_name = './diagnostic/geom_g_true.dat'

  open(unit= 20,file=file_name,status='replace', position='append')
  do i = 1, ntot_geom
    if (indx_gp(i)) then
      write(20,*) gx(i),gy(i)
    end if
  end do
  close(unit=20)

  file_name = './diagnostic/geom_g_false.dat'

  open(unit= 20,file=file_name,status='replace', position='append')
  do i = 1, ntot_geom
    if (indx_gp(i) .eqv. .false.) then
      write(20,*) gx(i),gy(i)
    end if
  end do
  close(unit=20)

end subroutine diagnos_geom_g_pfs

end module diagnos_geom
