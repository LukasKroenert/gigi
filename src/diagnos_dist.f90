module diagnos_dist

  implicit none

  private

  public :: diagnos_dist_init, diagnos_dist_ntime, diagnos_dist_pp, &
     & diagnos_dist_modes, diagnos_dist_sum_calc, diagnos_dist_sum_naverage, &
     & diagnos_dist_max_naverage

  ! Weights of each true grid point for calculation of the total dist
  real, public, allocatable, save :: dist_weight(:)

  ! Sum over the weighted distribution function
  real, public, allocatable, save :: sum_weighted_dist

  ! Sum over the weighted distribution function at t = 0
  real, public, allocatable, save :: sum_weighted_dist_init

  ! True if the weighted and normalized sum of the distribution function is
  ! larger than 1/exp(0)
  logical, public, save :: sum_weighted_exponential

  ! Maximum of the distribution normalized to the initial maximum
  real, public, save :: dist_max

  ! Initial maximum of the distribution
  real, public, save :: dist_max_init

  ! True if normalized maximum of the distribution is larger than 1/exp(0)
  logical, public, save :: dist_max_exponential

contains

subroutine diagnos_dist_init

  call diagnos_dist_sum_init

end subroutine diagnos_dist_init

subroutine diagnos_dist_ntime

  use control, only : itime, open_status
  use dist,    only : fdisi, ntot_geom
  use geom,    only : gx,gy
  use index_function, only : indx_gp

  character(len = 4) :: itime_char
  character(len = 128) :: file_name
  integer :: i

  write(itime_char,'(I3)')itime

  file_name = './diagnostic/dist_ntime/dist_time_'//&
     & trim(adjustl(itime_char))//'.dat'

  open(unit= 20,file=file_name,status='replace', position='append')
  do i = 1, ntot_geom
    if (indx_gp(i)) then
      write(20,*) gx(i),gy(i), real(fdisi(i))
    end if
  end do
  close(unit=20)

end subroutine diagnos_dist_ntime

subroutine diagnos_dist_pp(fdis)

  use control,         only : itime, open_status
  use dist,            only : ntot_geom
  use geom,            only : gx, gy, gth, gpsi
  use index_function,  only : indx

  complex, intent(in) :: fdis(ntot_geom*5)

  character(len = 4) :: itime_char
  character(len = 128) :: file_name
  integer :: i

  write(itime_char,'(I3)')itime

  file_name = './diagnostic/dist_ntime/dist_pp_time_'//&
     & trim(adjustl(itime_char))//'.dat'

  ! write(*,*) 
  ! write(*,*) fdis(1), gth(1)

  open(unit= 20,file=file_name,status='replace', position='append')

  do i = 1, ntot_geom
        write(20,*) gx(i), gy(i), gth(i), gpsi(i), real(fdis(i)), &
         & gx(ntot_geom+(i-1)*4+1), gy(ntot_geom+(i-1)*4+1), &
         & gth(ntot_geom+(i-1)*4+1), gpsi(ntot_geom+(i-1)*4+1), real(fdis(ntot_geom+(i-1)*4+1)), &
         & gx(ntot_geom+(i-1)*4+2), gy(ntot_geom+(i-1)*4+2), &
         & gth(ntot_geom+(i-1)*4+2), gpsi(ntot_geom+(i-1)*4+2), real(fdis(ntot_geom+(i-1)*4+2)), &
         & gx(ntot_geom+(i-1)*4+3), gy(ntot_geom+(i-1)*4+3), &
         & gth(ntot_geom+(i-1)*4+3), gpsi(ntot_geom+(i-1)*4+3), real(fdis(ntot_geom+(i-1)*4+3)), &
         & gx(ntot_geom+(i-1)*4+4), gy(ntot_geom+(i-1)*4+4), &
         & gth(ntot_geom+(i-1)*4+4), gpsi(ntot_geom+(i-1)*4+4), real(fdis(ntot_geom+(i-1)*4+4))
  end do

  close(unit=20)

end subroutine diagnos_dist_pp

! Do FFT of the distribution function and write it down
subroutine diagnos_dist_modes

  use control, only : itime, open_status
  use dist,    only : ntot_geom, fdisi_fft
  use geom,    only : gx,gy
  use index_function, only : indx_gp
  use index_function, only : indx
  use global,         only : nth_global, npsi_tot_global

  character(len = 4) :: itime_char
  character(len = 128) :: file_name
  integer :: i

  call diagnos_dist_calc_fft

  write(itime_char,'(I3)')itime

  file_name = './diagnostic/dist_fft/dist_fft_time_'//&
     & trim(adjustl(itime_char))//'.dat'

  open(unit= 20,file=file_name,status='replace', position='append')
  do i = 1, ntot_geom
    if (indx_gp(i)) then
      write(20,*) real(fdisi_fft(i)),aimag(fdisi_fft(i))
    end if
  end do
  close(unit=20)

end subroutine diagnos_dist_modes

subroutine diagnos_dist_calc_fft

  use fft,            only : four1D_real,FFT_FORWARD
  use dist,           only : fdisi,fdisi_fft,ntot_geom

  real :: fdisi_real(ntot_geom)

  integer :: i

  do i = 1,ntot_geom
    fdisi_real(i) = real(fdisi(i))
  end do

  call four1D_real(fdisi_real(1:ntot_geom),fdisi_fft(1:ntot_geom),FFT_FORWARD)

end subroutine diagnos_dist_calc_fft

subroutine diagnos_dist_sum_init

  use index_function, only : indx_gp,indx
  use geom,           only : nth,npsi_tot,flux_function_axial_diverted,&
     & gpsi,gy, eq_type,ff
  use general,        only : gkw_abort

  real :: sum_weight
  integer :: iphi, ith, ipsi, ipp, indx_tmp

  integer :: allocate_status = 0

  iphi = 1
  ipp = 0

  sum_weight = 0.

  allocate(dist_weight(nth*npsi_tot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate dist_weight'

  select case(eq_type)
  case('axial_circular')
    sum_weight = gpsi(indx(iphi,1,npsi_tot,0))-gpsi(indx(iphi,1,1,0))
    do ith = 1, nth
      do ipsi = 1, npsi_tot
        indx_tmp = indx(iphi,ith,ipsi,ipp)
        if (indx_gp(indx_tmp)) then
          dist_weight(indx_tmp) = 0.
          if (ipsi == 1) then
            dist_weight(indx_tmp) = &
               & (gpsi(indx(iphi,ith,ipsi+1,ipp))-gpsi(indx_tmp))/2
          else if (ipsi == npsi_tot) then
            dist_weight(indx_tmp) = &
               & (gpsi(indx_tmp)-gpsi(indx(iphi,ith,ipsi-1,ipp)))/2
          else
            dist_weight(indx_tmp) = dist_weight(indx_tmp) + &
               & (gpsi(indx(iphi,ith,ipsi+1,ipp))-gpsi(indx_tmp))/2
            dist_weight(indx_tmp) = dist_weight(indx_tmp) + &
               & (gpsi(indx_tmp)-gpsi(indx(iphi,ith,ipsi-1,ipp)))/2
          end if
        end if
      end do
    end do

  ! Maybe this make just Sense as long the x-Point is not in the  grid
  case('axial_diverted')
    do ith = 1, nth
      do ipsi = 1, npsi_tot
        indx_tmp = indx(iphi,ith,ipsi,ipp)
        if (indx_gp(indx_tmp)) then
          dist_weight(indx_tmp) = 0.
          if (ipsi == 1) then
            dist_weight(indx_tmp) = &
               & (ff(indx(iphi,ith,ipsi+1,ipp))-ff(indx_tmp))/2
          else if (ipsi == npsi_tot) then
            dist_weight(indx_tmp) = &
               & (ff(indx_tmp)-ff(indx(iphi,ith,ipsi-1,ipp)))/2
          else
            dist_weight(indx_tmp) = dist_weight(indx_tmp) + &
               & (ff(indx(iphi,ith,ipsi+1,ipp))-ff(indx_tmp))/2
            dist_weight(indx_tmp) = dist_weight(indx_tmp) + &
               & (ff(indx_tmp)-ff(indx(iphi,ith,ipsi-1,ipp)))/2
          end if
          sum_weight = sum_weight + dist_weight(indx_tmp)
        end if
      end do
    end do

    case default
      call gkw_abort('no eq_type in diagnos_dist: diagnos_dist_sum_init')

  end select

  ! Normalize the weighting to the sum of all radial distances between the inner
  ! and outer limiting flux surface, or to the total difference of the flux
  ! function between the grid points in radial direction.
  do ith = 1, nth
    do ipsi = 1, npsi_tot
      indx_tmp = indx(iphi,ith,ipsi,ipp)
      if(indx_gp(indx_tmp)) then
        dist_weight(indx_tmp) = dist_weight(indx_tmp)/sum_weight
      else
        dist_weight(indx_tmp) = 0.
      end if
    end do
  end do

end subroutine diagnos_dist_sum_init

subroutine diagnos_dist_sum_calc

  use dist,    only : ntot_geom, fdisi
  use control, only : itime, naverage

  integer :: i
  character(len = 128) :: file_name

  file_name = './diagnostic/dist_sum_weighted_exponential.dat'

  sum_weighted_dist = 0.
  do i = 1, ntot_geom
    sum_weighted_dist = sum_weighted_dist + fdisi(i)*dist_weight(i)
  end do

  ! Save the sum at the initial time step
  if (itime == 0) then
    sum_weighted_dist_init = sum_weighted_dist
    sum_weighted_exponential = .true.
  end if

  ! Normalize the sum to the sum of the initial time step
  sum_weighted_dist = sum_weighted_dist/sum_weighted_dist_init

  if (sum_weighted_exponential) then
    if (sum_weighted_dist < 1./exp(1.)) then
      sum_weighted_exponential = .false.

      write(*,*) 'weighted, normalized sum under 1/exp(0), sum =',&
         & sum_weighted_dist, ',time =',itime*naverage

      open(unit= 20,file=file_name,status='replace', position='append')
      write(20,*) itime*naverage, sum_weighted_dist
      close(unit=20)

    end if
  end if

end subroutine diagnos_dist_sum_calc

subroutine diagnos_dist_sum_naverage

  use control, only : open_status, itime, naverage

  character(len = 128) :: file_name

  file_name = './diagnostic/dist_sum_weighted.dat'

  open(unit= 20,file=file_name,status=open_status, position='append')
  write(20,*) itime*naverage, sum_weighted_dist
  close(unit=20)

end subroutine diagnos_dist_sum_naverage

subroutine diagnos_dist_max_naverage

  use control, only : open_status, itime, naverage
  use dist, only : ntot_geom, fdisi

  character(len = 128) :: file_name
  character(len = 128) :: file_name_exp

  file_name = './diagnostic/dist_max.dat'
  file_name_exp = './diagnostic/dist_max_exponential.dat'

  dist_max = maxval(real(fdisi(1:ntot_geom)))

  if(itime == 0) then
    dist_max_init = dist_max
    dist_max_exponential = .true.
  end if

  dist_max = dist_max/dist_max_init

  if(dist_max_exponential) then
    if(dist_max < 1./exp(1.)) then
      dist_max_exponential = .false.

      write(*,*) 'dist_max under 1/exp(0), dist_max =', &
         & dist_max,',time =',itime*naverage

      open(unit= 20,file=file_name_exp,status='replace', position='append')
      write(20,*) itime*naverage, dist_max
      close(unit=20)

    end if

    if(dist_max > 1.+exp(1.)) then
      write(*,*) 'WARNING: dist_max over 1+1/exp(0), dist_max =', &
         & dist_max,',time =',itime*naverage
    end if
  end if

  open(unit= 20,file=file_name,status=open_status, position='append')
  write(20,*) itime*naverage, dist_max
  close(unit=20)


end subroutine diagnos_dist_max_naverage

end module diagnos_dist
