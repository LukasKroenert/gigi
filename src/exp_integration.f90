module exp_integration

  implicit none

!#ifdef real_precision_default
!  external :: ccopy,caxpy
!#else
  external :: zcopy,zaxpy
!#endif

  private

  public :: init_explicit, explicit_integration

  !> parameter used to set the implicitness in some of the schemes
  real, save :: delta

  !> the distribution function and rhs of the intermediate steps
  complex, allocatable, save, dimension(:,:) :: fdisk
  complex, allocatable, save, dimension(:,:), public :: rhsk

  !> integers corresponding to the number of temporary solution copies needed
  !> in explicit time integration
  integer, save :: isizef, isizer

contains

subroutine init_explicit()

  use control, only : meth
  use dist,    only : ntot, ntot_geom

  ! for error
  integer :: ierr

  ierr = 0

  ! set the implicitness parameter
  delta = 0.5E0

  select case(meth)
  case('RK2')
    isizef = 1
    isizer = 1
  case('RK4')
    isizef = 2
    isizer = 1
  case default
    stop 'exp_integration : Unknown explicit integration scheme'
  end select

  allocate(fdisk(ntot,isizef),stat=ierr)
  if (ierr.ne.0) write(*,*) ('Could not allocate fdisk in exp_integration')

  ! allocate the right hand side
  allocate(rhsk(ntot_geom,isizer),stat=ierr)
  if (ierr.ne.0) write(*,*) ('Could not allocate rhsk in exp_integration')

end subroutine init_explicit

subroutine explicit_integration(itime)

  use control,      only : naverage, meth
  use diagnos_dist, only : diagnos_dist_sum_calc

  integer, intent(in) :: itime
  integer :: iloop

  time_stepping : do iloop = 1, naverage

    method_of_integration : select case(meth)
    case('RK2')  ; call rk2
    case('RK4')  ; call rk4
    case default
      stop 'Not a proper selection of the numerical method [METH]'
    end select method_of_integration

    call diagnos_dist_sum_calc

  end do time_stepping

end subroutine explicit_integration

!****************************************************************************
!> A Runge Kutta fourth order timestep
!----------------------------------------------------------------------------
subroutine rk4

  use control,   only : dtim
  use constants, only : c1
  use dist,      only : ntot_geom, fdisi

  use index_function, only: indx

  use geom, only : nth, npsi_tot
  integer :: ipsi, ith

  complex :: cdum

  !BLAS version, double precision
!#if defined(blas)

  ! initialize to fdisk(:,1)
  !do i = 1, nsolc
  !  fdisk(i,1) = fdisi(i)
  !  fdisk(i,2) = fdisi(i)
  !end do

! #ifdef real_precision_default
!   call ccopy(ntot_geom, fdisi(1:ntot_geom), 1, fdisk(1:ntot_geom, 1),1)
!   call ccopy(ntot_geom, fdisi(1:ntot_geom), 1, fdisk(1:ntot_geom, 2),1)
! #else
  call zcopy(ntot_geom, fdisi(1:ntot_geom), 1, fdisk(1:ntot_geom, 1),1)
  call zcopy(ntot_geom, fdisi(1:ntot_geom), 1, fdisk(1:ntot_geom, 2),1)
! #endif
  ! advance a timestep delta*dtime, calculate delta f
  call calculate_rhs(fdisk(1,2),rhsk(1,1))

  ! first step into solution
  !   do i = 1, ntot_geom
  !     fdisi(i) = fdisi(i) + rhsk(i,1)/6.E0
  !   end do

  cdum=c1/6.0
! #ifdef real_precision_default
  ! call caxpy(ntot_geom, cdum, rhsk(1:ntot_geom,1), 1,fdisi(1:ntot_geom),1)
! #else
  call zaxpy(ntot_geom, cdum, rhsk(1:ntot_geom,1), 1,fdisi(1:ntot_geom),1)
! #endif
  ! second step initialization
!   do i = 1, ntot_geom
!     fdisk(i,2) = fdisk(i,1) + rhsk(i,1)/2.E0
!   end do
  cdum=c1/2.0
! #ifdef real_precision_default
!   call ccopy(ntot_geom, fdisk(1:ntot_geom,1), 1, fdisk(1:ntot_geom,2), 1)
!   call caxpy(ntot_geom, cdum, rhsk(1:ntot_geom,1), 1,fdisk(1:ntot_geom,2),1)
! #else
  call zcopy(ntot_geom, fdisk(1:ntot_geom,1), 1, fdisk(1:ntot_geom,2), 1)
  call zaxpy(ntot_geom, cdum, rhsk(1:ntot_geom,1), 1,fdisk(1:ntot_geom,2),1)
! #endif
  ! advance a timestep delta*dtime, calculate delta f
  call calculate_rhs(fdisk(1,2),rhsk(1,1),0.5*dtim)

  ! second step into solution
!   do i = 1, ntot_geom
!     fdisi(i) = fdisi(i) + rhsk(i,1)/3.E0
!   end do
  cdum=c1/3.0
! #ifdef real_precision_default
!   call caxpy(ntot_geom, cdum, rhsk(1:ntot_geom,1), 1,fdisi(1:ntot_geom),1)
! #else
  call zaxpy(ntot_geom, cdum, rhsk(1:ntot_geom,1), 1,fdisi(1:ntot_geom),1)
! #endif
  ! third step initialization
!   do i = 1, ntot_geom
!     fdisk(i,2) = fdisk(i,1) + rhsk(i,1)/2.E0
!   end do
  cdum=c1/2.0
! #ifdef real_precision_default
!   call ccopy(ntot_geom, fdisk(1:ntot_geom,1), 1, fdisk(1:ntot_geom,2),1)
!   call caxpy(ntot_geom, cdum, rhsk(1:ntot_geom,1), 1,fdisk(1:ntot_geom,2),1)
! #else
  call zcopy(ntot_geom, fdisk(1:ntot_geom,1), 1, fdisk(1:ntot_geom,2),1)
  call zaxpy(ntot_geom, cdum, rhsk(1:ntot_geom,1), 1,fdisk(1:ntot_geom,2),1)
! #endif

  ! advance a timestep delta*dtime, calculate delta f
  call calculate_rhs(fdisk(1,2),rhsk(1,1),0.5*dtim)

  ! third step into solution
!   do i = 1, ntot_geom
!     fdisi(i) = fdisi(i) + rhsk(i,1)/3.E0
!   end do
  cdum=c1/3.0
! #ifdef real_precision_default
!   call caxpy(ntot_geom, cdum, rhsk(1:ntot_geom,1), 1,fdisi(1:ntot_geom),1)
! #else
  call zaxpy(ntot_geom, cdum, rhsk(1:ntot_geom,1), 1,fdisi(1:ntot_geom),1)
! #endif
  ! fourth step initialization
!   do i = 1, ntot_geom
!     fdisk(i,2) = fdisk(i,1) + rhsk(i,1)
!   end do
  cdum=c1
! #ifdef real_precision_default
!   call ccopy(ntot_geom, fdisk(1:ntot_geom,1), 1, fdisk(1:ntot_geom,2),1)
!   call caxpy(ntot_geom, cdum, rhsk(1:ntot_geom,1), 1,fdisk(1:ntot_geom,2),1)
! #else
  call zcopy(ntot_geom, fdisk(1:ntot_geom,1), 1, fdisk(1:ntot_geom,2),1)
  call zaxpy(ntot_geom, cdum, rhsk(1:ntot_geom,1), 1,fdisk(1:ntot_geom,2),1)
! #endif


  ! advance a timestep delta*dtime, calculate delta f
  call calculate_rhs(fdisk(1,2),rhsk(1,1),dtim)

!   ! fourth step into solution
!   do i = 1, ntot_geom
!     fdisi(i) = fdisi(i) + rhsk(i,1)/6.E0
!   end do
  cdum=c1/6.0 !promote to complex
! #ifdef real_precision_default
!   call caxpy(ntot_geom, cdum, rhsk(1:ntot_geom,1), 1,fdisi(1:ntot_geom),1)
! #else
  call zaxpy(ntot_geom, cdum, rhsk(1:ntot_geom,1), 1,fdisi(1:ntot_geom),1)
! #endif

end subroutine rk4


!****************************************************************************
!> A Runge Kutta second order timestep
!----------------------------------------------------------------------------
subroutine rk2

  use constants, only : c1
  use dist,      only : fdisi
  use dist,      only : ntot_geom

  use geom, only : nth, npsi_tot
  integer :: ipsi, ith

  integer :: i
  complex :: cdum

  ! first 'half' time step
  ! calculate the rhs
  do i = 1, ntot_geom
    fdisk(i,1) = fdisi(i)
  end do
  call calculate_rhs(fdisk(1,1),rhsk(1,1))

  ! advance a timestep delta*dtime, calculate f
  ! and store in fdisk(:,2)
  cdum = c1*delta
  do i = 1, ntot_geom
    fdisk(i,1) = fdisi(i) + cdum*rhsk(i,1)
  end do

  !  second part full time step calculated from fdisk(:,2)
  ! calculate the rhs
  call calculate_rhs(fdisk(1,1),rhsk(1,1))

  ! add the rhs to fdisi
  do i = 1, ntot_geom
    fdisi(i) = fdisi(i) + rhsk(i,1)
  end do

end subroutine rk2

subroutine calculate_rhs(fdis,rhs,DPART_IN)

  use matdat,        only : irs1, ire1, irs,jj,mat
  use interpolation, only : interpolation_psi_coeff, interpolation_th_coeff, &
     & interpolation_psi_dist, interpolation_th_dist
  use diagnos_dist,  only : diagnos_dist_pp
  use dist,          only : ntot_geom, fdis_tmp
  use constants,     only : c1
  use control,       only : dtim,itime
  use mpiinterface,  only : mpiwtime

  use geom, only : nth, npsi_tot
  use index_function, only : indx

  double precision :: t_1, t_2, t_3, t_4
  integer :: ipsi,ith, iphi, ipp

  complex, intent(inout) :: fdis(ntot_geom*5)
  complex, intent(out) :: rhs(ntot_geom)
  real, intent(in), optional :: DPART_IN

  complex :: cdum, acc
  integer :: ir, i

  ! t_1 = mpiwtime()

  ! Do spline interpolation, note: Do not change this order
  ! Step 1: Interpolate the ghost points

  ! write(*,*) '1',fdis(1)
  ! write(*,*) '1', fdis(indx(1,1,12,0)),fdis(indx(1,1,13,0)),&
  !    & fdis(indx(1,1,14,0))

  call interpolation_psi_coeff(fdis)
  call interpolation_psi_dist(fdis)
  call interpolation_th_coeff(fdis)
  call interpolation_th_dist(fdis)
  ! write(*,*) '2',fdis(1)
  ! write(*,*) '2', fdis(indx(1,1,12,0)),fdis(indx(1,1,13,0)),&
  !    & fdis(indx(1,1,14,0))

  ! t_2 = mpiwtime()

  call diagnos_dist_pp(fdis)

  ! t_3 = mpiwtime()

  ! Copy fdis into fdis_tmp.
  do i = 1, ntot_geom*5
    fdis_tmp(i) = fdis(i)
  end do

  ! Intitialise the RHS to zero
  ! only the points of the real grid and not the pp are considered
  do i = 1, ntot_geom
    rhs(i) = (0.,0.)
  end do

  cdum = dtim*c1
  ! Calculate the linear terms
  !>   f_new(ii(i)) = f_new(ii(i)) + delta_time * mat(i)*f_old(jj(i))
  do ir = irs1,ire1 !loop over rows
    acc = (0.,0.)
    
    do i = irs(ir,1),irs(ir+1,1)-1 !loop over elements in row
      ! if (itime == 20) then
      !   write(*,*) ir,i,acc
      ! end if
      acc = acc + cdum*mat(i)*fdis_tmp(jj(i))
    end do
    rhs(ir) = rhs(ir)+acc
  end do

  ! iphi = 1
  ! ipp = 0
  ! do ith = 1, nth
  !    do ipsi = 1, npsi_tot
  !       if(ith == 15) then
  !          select case(ipsi)
  !          case(10,11,12)
  !             write(*,*) ith, ipsi, real(fdis(indx(iphi,ith,ipsi,ipp))), &
  !                  & real(rhs(indx(iphi,ith,ipsi,ipp)))
  !          end select
  !       end if
  !    end do
  ! end do

  ! write(*,*) '3', fdis(1), rhs(1)
  ! write(*,*) '3', fdis(indx(1,1,12,0)),fdis(indx(1,1,13,0)),&
  !    & fdis(indx(1,1,14,0)),rhs(indx(1,1,13,0))
  ! write(*,*)

! do ith = 1, 1 !nth
  !   write(*,*) ith, rhs(indx(1,ith,1,0))
  ! end do

  ! stop
  ! t_4 = mpiwtime()

  ! write(*,*) 'interpolation', t_2-t_1
  ! write(*,*) 'matrix', t_4-t_3

end subroutine calculate_rhs

end module exp_integration
