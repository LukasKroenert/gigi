module control

 implicit none

 private

 public :: control_init, control_read_nml

 !> Status for open statment to write data to files
 character(len=7), save, public :: open_status

 !> Time step size
 real, public, save :: dtim

 !> Number of iterations of naverage
 integer, public, save :: ntime

 !> Number of time steps of one data output
 integer, public, save :: naverage

 ! counters:

 !> Total time
  real, save, public :: time
 !> number of current large timestep, since the beginning of the
 !> current run
 integer, save, public :: itime

 !> Choice of integration algorithm
 character(len=20), public, save :: meth

 ! Choice of discretization of the parallel gradient
 integer, public, save :: grad_disc

 !> (Hyper) dissipation coefficient for parallel velocity space
 real, public, save :: disp_vp

 !> (Hyper) dissipation coefficient for nonparallel velocity space
 real, public, save :: disp_vnp

 !> True: Turn on parallel advective motion
 logical, save, public :: grad_par

  !> True: Turn on poloidal and radial gradient
 logical, save, public :: grad_npar

 !> If grad_npar = .true. then turn on gradient in z-direction.
 !> with grad_z = proportional(dy/r)
 logical, save, public :: grad_z

 !> if grad_npar = .true. then trun on poloidal gradient.
 !> with grad_th = proportional(dx/r)
 logical, save, public :: grad_th

contains

subroutine control_init

  open_status = 'replace'

  itime = 0

end subroutine control_init

subroutine control_read_nml(control_file_unit)

  integer, intent(in) :: control_file_unit
  namelist /control/             &
       & dtim, ntime, naverage, meth, grad_disc, disp_vp, disp_vnp, &
       & grad_par, grad_npar, grad_z, grad_th
  ! read namelist
  read(control_file_unit,NML=control)

end subroutine control_read_nml

end module control
