program main

  use init,            only : initialize
  use control,         only : ntime,itime,time
  use exp_integration, only : explicit_integration
  use diagnostic,      only : diagnostic_init, diagnostic_naverage, &
     & diagnostic_initial_output
  use mpiinterface,    only : mpiinit, mpiwtime

  implicit none

  ! Integer variable for loop over large time steps naverage
  integer :: i = 0

  !> time at begin of the run
  double precision :: t_begin
  !> time at the end of the run
  double precision :: t_end
  double precision :: t_tus, t_1, t_predict
  double precision :: t_begin_main, t_end_main
  double precision :: t_diag_b, t_diag, t_step_b, t_step
  real    :: t_1r

  ! initialize mpi
  call mpiinit()

  ! get the start time of the run
  t_begin = mpiwtime()

  i = 0
  ! Store initial time in control
  itime = i

  call initialize

  ! get the time at the start of the main loops
  t_begin_main = mpiwtime()

  call diagnostic_initial_output

  write(*,*)
  write(*,*) 'Start time stepping'
  write(*,*)

  ! loop over large time steps
  large_time_steps : do i = 1, ntime

    ! store loop value in control
    itime = i

    t_step_b = mpiwtime()

    call explicit_integration(i)

    t_step = mpiwtime() - t_step_b

    ! calculate and output diagnostics
    t_diag_b = mpiwtime()
    call diagnostic_naverage
    t_diag = mpiwtime() - t_diag_b

    !Predict total runtime after first iteration
    if (i == 1) then
      !Time for one iteration in seconds
      t_1 = mpiwtime()-t_begin_main
      t_1r = t_1
      !Total predicted runtime in seconds
      t_predict=t_1*ntime

      if (t_predict > 60.) then
        write(*,*)
        write(*,*) 'Iteration 1 completed successfully.'
        write(*,*) 'Predicted runtime: ', nint(t_predict/60.), ' minutes'
        write(*,*) 'current time:'
        call system('date')
        write(*,*)
      end if
    end if

  end do large_time_steps

  write(*,*)
  write(*,*)'Run successfully completed'
  write(*,*)

end program
