!-------------------------------------------------------------------------------
!>
!!
!!
!<------------------------------------------------------------------------------
module matdat

  implicit none

  private

  public :: finish_matrix_section
  public :: compress_piece

  public :: matdat_allocate, register_term, set_indx, add_element
  public :: jj, mat
  public :: irs1, ire1, irs

  public :: pos_rad_grid

  complex, save, allocatable :: mat(:)

  !> Integer that gives the total number of elements in the matrix (mat).
  integer, save, public :: nmat  = 0

  !> Integer that determines the size of the first section of the matrix
  !> mat(1:n1) contains all terms of the evolution equation for the
  !> distribution function that are proportional to the distribution
  !> function itself
  integer, save  :: n1 = 0


  !> Integers for starting rows of each section of the main matrix
  integer, save :: irs1

  !> Integers for final row in each section of the main matrix
  integer, save :: ire1

  !> Integer array for the starting element of each matrix row
  integer, save, allocatable :: irs(:,:)
  
  !> Integers for tracking matrix compressions
  integer, save :: last_comp = 0
  integer, save :: last_sec = 0

  !> Integer array that determines the column of the matrix element in mat
  integer, save, allocatable :: jj(:)
  integer, save, allocatable :: ii(:)

  interface compress_piece
      module procedure compress_piece_real
      module procedure compress_piece_complex
  end interface

contains

subroutine matdat_allocate()

  use dist,    only : ntot, ntot_geom
  use general, only : gkw_abort

  integer :: ierr

  ! initialize the error code
  ierr = 0

  allocate(ii(ntot),stat=ierr)
  if (ierr /= 0) write(*,*) ('matdat_allocate: cannot allocate ii')
  allocate(jj(ntot),stat=ierr)
  if (ierr /= 0) write(*,*) ('matdat_allocate: cannot allocate jj')
  allocate(mat(ntot),stat=ierr)
  if (ierr /= 0) write(*,*) ('matdat_allocate: cannot allocate mat')

  ! allocate the row indices ! column major order, first index faster
  allocate(irs(ntot_geom+1,1),stat=ierr)
  if (ierr /= 0) call gkw_abort('matdat_allocate: cannot allocate irs')

end subroutine matdat_allocate

subroutine register_term(elem)

  use structures, only : matrix_element
  type (matrix_element), intent(inout) :: elem
  type (matrix_element):: E

  E = elem
  E%val = (0.0)

  call set_indx(E,1,1,1,0)

  call add_element(E)

  elem%registered = .true.

end subroutine register_term

subroutine set_indx(E,iphi,ith,ipsi,ipp)

  use structures, only : matrix_element

  type (matrix_element), intent(inout) :: E
  integer, intent(in) :: iphi,ith,ipsi,ipp

  E%iphi = iphi
  E%ith  = ith
  E%ipsi = ipsi
  E%ipp  = ipp

  E%iphiloc = iphi
  E%ithloc = ith
  E%ipsiloc = ipsi
  E%ipploc = ipp

end subroutine set_indx

subroutine add_element(E)

  use structures,    only : matrix_element
  use dist,          only : ntot
  ! use index_function , only : indx

  type (matrix_element), intent(in) :: E

  ! Check if anything is wrong
  if (nmat + 1 > ntot) then      ! The array is simply not large enough
    stop 'add_element: too many compressions, increase ntot'
  end if

  nmat = nmat+1
  ii(nmat) = get_elem_ii(E)
  jj(nmat) = get_elem_jj(E)
  mat(nmat)   = E%val

  ! if (E%ith == 30 .and. E%ipsi == 10) then
  !   write(*,*)
  !   write(*,*) 'add elem'
  !   write(*,*) nmat, ii(nmat), jj(nmat), mat(nmat), E%ith, E%ipsi, E%ipp, &
  !      & E%ipploc

  ! end if

  ! if (nmat == 5906 .or. nmat == 5907 .or. nmat ==5905) then
  !   write(*,*)
  !   write(*,*) 'add elem'
  !   write(*,*) nmat, ii(nmat), jj(nmat), mat(nmat), E%ith, E%ipsi, E%ipp, &
  !      & E%ipploc
  ! end if

  ! if (jj(nmat) < 0 ) then

  !   write(*,*) jj(nmat), indx(E%iphiloc,E%ithloc,E%ipsiloc,E%ipploc), &
  !      & E%iphiloc, E%ithloc, E%ipsiloc, E%ipploc
  ! end if
    
end subroutine add_element

function get_elem_ii(E)

  use structures,    only : matrix_element
  use index_function, only : indx

  type (matrix_element), intent(in) :: E

  integer :: get_elem_ii

  get_elem_ii = indx(E%iphi,E%ith,E%ipsi,E%ipp)

end function get_elem_ii

function  get_elem_jj(E)

  use structures,    only : matrix_element
  use index_function, only : indx

  type (matrix_element), intent(in) :: E

  integer :: get_elem_jj

  get_elem_jj = indx(E%iphiloc,E%ithloc,E%ipsiloc,E%ipploc)

end function get_elem_jj

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!-----------------------------------------------------------------------------
!> Stores size of current matrix section 
!> and sorts and compress each section when complete
!> Must only be called once, sequentially, for each isel
!-----------------------------------------------------------------------------
subroutine finish_matrix_section(isel)

  use general, only : gkw_abort

  integer, intent(in) :: isel
  integer :: i
  integer, save :: isel_ch = 0

  if (isel_ch + 1 /= isel) call gkw_abort('finish_matrix_section call error')
  isel_ch = isel

  select case(isel)

  case(1)
    call compress_piece(1,nmat,ii,jj,mat)
    n1  = nmat
    last_comp = n1
    last_sec = n1
    call row_index(1,n1,irs(:,1),irs1,ire1)

  case default
    call gkw_abort('finish_matrix_selection: isel out of range!')

  end select

end subroutine finish_matrix_section

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!-----------------------------------------------------------------------------
!> Compress complex matrix piece between istart and iend.
!> Same as compress_piece_real, matched via interface to compress_piece.
!>
!> iend returns the updated compressed location of the segment end.
!> Ranges must always be compressed sequentially to avoid gaps and junk data.
!> All of the matrix below istart must already have been compressed
!> It is acceptable however to recompress a range with a new piece at the end.
!>
!> If sections are compressed together after the per-section compression 
!> this results in a re-sorting but not a size reduction because there is no 
!> overlap between the four matrix sections n1,n2,n3,n4
!-----------------------------------------------------------------------------
subroutine compress_piece_complex(istart,iend,iii,jjj,mmm)

!  use control, only : root_or_not_silent
  use general, only : gkw_abort

  integer, intent(in) :: istart  !< matrix position to start from
  integer, intent(inout) :: iend !< matrix position to end at, and return
  integer, intent(inout), dimension(:) :: iii !< row location data
  integer, intent(inout), dimension(:) :: jjj !< column location data
  complex, intent(inout), dimension(:) :: mmm !< matrix data

  integer :: i, ireduced, ncmp
  real :: dummatr(1:0)  !< Dummy zero sized array
  
  ! number of elements to compress          
  ncmp = iend - istart + 1

  if (ncmp < 0) call gkw_abort('Error in call to compress_piece')
  if (ncmp < 2) return ! can't sort or compress a scalar

  ! some safety checks
  if (size(iii)/=size(jjj)) call gkw_abort('compress_piece: size iii/= jjj')
  if (size(mmm)/=size(jjj)) call gkw_abort('compress_piece: size mmm/= jjj')
  if (istart > size(mmm)) call gkw_abort('compress_piece: istart fail')
  if (iend > size(mmm)) call gkw_abort('compress_piece: iend fail') 
   
  !first sort the matrix (use 6th argument to sort complex matrix)  
  !call sort_matrix(ncmp,istart,iii,jjj,dummatr,mmm) 
  !Above line has bug (see issue 126), workaround below
  call sort_matrix(ncmp,1,iii(istart:iend),jjj(istart:iend),dummatr,mmm(istart:iend)) 
     
  ireduced = istart

  ! compress the matrix (not robust if the sort was incorrect)
  do i = 1+istart, iend
    if (iii(i) == iii(ireduced) .and. jjj(i) == jjj(ireduced)) then
      mmm(ireduced) = mmm(ireduced) + mmm(i)
    else
      ireduced = ireduced + 1
      iii(ireduced) = iii(i)
      jjj(ireduced) = jjj(i)
      mmm(ireduced) = mmm(i)
    end if
  end do  

  ! Reportage
  ! if (root_or_not_silent) then
  !       write(*,*) '++++++++++++++++++++++++++++++++++++++++++++++++++++++'
  !       write(*,*) 'Partial matrix compression successfully completed'
  !       write(*,223) iend,ireduced
  ! 223  format(' Original ',I8,' elements. New ',I8,' elements')
  !       write(*,*) '++++++++++++++++++++++++++++++++++++++++++++++++++++++'
  !       write(*,*)
  ! end if
  
  iend=ireduced

end subroutine compress_piece_complex

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!-----------------------------------------------------------------------------
!> Compress real matrix piece between istart and iend
!> Same as compress_piece_complex, matched via interface to compress_piece
!-----------------------------------------------------------------------------
subroutine compress_piece_real(istart,iend,iii,jjj,mmm)

  !use control, only : root_or_not_silent
  use general, only : gkw_abort
  
  integer, intent(in) :: istart !< matrix position to start from
  integer, intent(inout) :: iend !< matrix posistion to end at
  integer, intent(inout), dimension(:) :: iii !< row location data
  integer, intent(inout), dimension(:) :: jjj !< column location data
  real, intent(inout), dimension(:) :: mmm !< matrix data

  integer :: i, ireduced, ncmp
  complex :: dummatc(1:0)  !< Dummy zero sized array
  
  ! number of elements to compress
  ncmp = iend - istart + 1

  if (ncmp < 0) call gkw_abort('Error in call to compress_piece')
  if (ncmp < 2) return ! can't sort or compress a scalar
  
  ! some safety checks
  if (size(iii)/=size(jjj)) call gkw_abort('compress_piece: iii/= jjj')
  if (size(mmm)/=size(jjj)) call gkw_abort('compress_piece: mmm/= jjj')
  if (istart > size(mmm)) call gkw_abort('compress_piece: istart fail')
  if (iend > size(mmm)) call gkw_abort('compress_piece: iend fail')   
    
  !first sort the matrix (use 5th argument to sort real matrix)
  !call sort_matrix(ncmp,istart,iii,jjj,mmm,dummatc) 
  !Above line has bug (see issue 126), workaround below
  call sort_matrix(ncmp,1,iii(istart:iend),jjj(istart:iend),mmm(istart:iend),dummatc) 
     
  ireduced = istart

  ! compress the matrix
  do i = 1+istart, iend
    if (iii(i) == iii(ireduced) .and. jjj(i) == jjj(ireduced)) then
      mmm(ireduced) = mmm(ireduced) + mmm(i)
    else
      ireduced = ireduced + 1
      iii(ireduced) = iii(i)
      jjj(ireduced) = jjj(i)
      mmm(ireduced) = mmm(i)
    end if
  end do

  ! ! Reportage
  ! if (root_or_not_silent) then
  !       write(*,*) '------------------------------------------------------'
  !       write(*,*) 'Partial real matrix compression successfully completed'
  !       write(*,229) iend,ireduced
  ! 229  format(' Original ',I8,' elements. New ',I8,' elements')
  !       write(*,*) '------------------------------------------------------'
  !       write(*,*)
  ! end if

  iend=ireduced

end subroutine compress_piece_real

!-----------------------------------------------------------------------------
!> Find the elements in the matrix where each row begins, and store it in irs
!> Must operate on matrix AFTER it is fully sorted and compressed
!> Used for faster matrix-vector multiply, only needed for explicit scheme
!-----------------------------------------------------------------------------
subroutine row_index(ns,ne,irs,rs,re)

  use general, only : gkw_abort

  integer, intent(in) :: ns, ne             !< start element, end element
  integer, intent(out) :: rs, re            !< first row, last row
  integer, dimension(:), intent(out) :: irs !< indices of first elem in each row

  integer :: i, irow    

  irs(:)=0
 
  if (ne - (ns + 1) < 1) then  ! deal with the case of zero size
    rs = 1 
    re = 0    
  else
  
    re = maxval(ii(ns:ne))
    rs = minval(ii(ns:ne))  
    
    irow = rs -1
    
    ! do i = ns, ne
      ! if (ii(i) /= irow) then
        ! irow = irow + 1
        ! irs(irow) = i
      ! end if
    ! end do
    
    ! index the row starts
    do i = ns, ne
        if (irow == ii(i)) then
          ! Do nothing
        else if (ii(i) > irow ) then
          do
            irow = irow + 1
            if (ii(i) == irow) then
               irs(irow) = i
               exit
            else  
               if (irow > re ) call gkw_abort('Matdat: severe irow error 1')
               irs(irow) = i ! produces an empty row
               ! this is needed in some cases (e.g. n_s_grid = 1, k=0 mode)
            end if
          end do
        else if (ii(i) < irow ) then
          call gkw_abort('Matdat: severe irow error 2')
        end if
    end do    
    
    ! Extra line marking row beyond end of the matrix for convenience
    irs(irow+1) = ne+1
    
    ! write(222,*) irs(rs:re)
    ! write(333,*) ii(ns:ne)
    ! write(*,*) ns, ne, rs, re
    
    if(maxval(irs(rs:re))>ne) then
      call gkw_abort('Matdat: Severe error in row_index high')
     end if
    if(minval(irs(rs:re))<ns) then
      call gkw_abort('Matdat: Severe error in row_index low')
    end if
    
  end if

end subroutine row_index

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!-----------------------------------------------------------------------------
!> Sort the matrix on ii and then jj. Use in-place recursive quicksort, then
!> switch to insertion sort when the partitions are of size min_size or less.
!> (min_size = 9 was found to be optimal in most cases tested)
!>
!> To use the same routine to sort both complex and real matrices requires
!> either pointers, interfaces, or redundant arguments (the choice used here) 
!> No solution is ideal
!>
!> With this choice, a dummy array argument must be passed.  
!> For a real matrix, the 5th argument is the matrix and the 6th is dummy
!> For a complex matrix, the 6th argument is the matrix and the 5th is dummy
!-----------------------------------------------------------------------------
subroutine sort_matrix(n_elem,i_start,iii,jjj,mtr,mtc)

use general, only : gkw_abort

  integer, intent(in) :: n_elem, i_start
  integer :: istart, iend
  integer, intent(inout) :: iii(:), jjj(:) !< Arrays to sort on
  logical :: lcmplx, lreal  !< switches for type of matrix to sort 
  real, dimension(:), intent(inout) :: mtr    !< real matrix to sort
  complex, dimension(:), intent(inout) :: mtc !< complex matrix to sort

  !> The smallest sized partition on which to perform quicksort
  integer, parameter :: min_size = 9

  ! return if nothing to sort
  if (n_elem < 2) return

  lcmplx = size(mtc)>0
  lreal = size(mtr)>0

  if (lcmplx.and.lreal) then
     call gkw_abort('Sort matrix called with two matrices')
  else if ( .not. (lcmplx .or.lreal)) then
     call gkw_abort('Sort matrix called with no matrices')
  end if

  if (lcmplx .and. size(mtc) < n_elem) then
     call gkw_abort('sort matrix: mtc too small')
  end if
  if (lreal .and. size(mtr) < n_elem) then
    call gkw_abort('sort matrix: mtr too small')
  end if

  istart = i_start
  iend   = i_start + n_elem - 1

  call qsort(istart,iend)

  ! internal subroutines
  contains

  !---------------------------------------------------------------------------
  recursive subroutine qsort(b,e)

    integer, intent(in) :: b, e
    integer :: ind

    if (e - b > min_size) then
      ind = median_of_3_ind(b,e)
      call partition(b,e,ind)
      call qsort(b,ind-1)
      call qsort(ind+1,e)
    else
      call isort(b,e)
    end if

  end subroutine qsort
  !---------------------------------------------------------------------------
  subroutine partition(b,e,ind)

    integer, intent(in) :: b, e
    integer, intent(inout) :: ind
    integer :: ival, jval, i

    ! the pivot values
    ival = iii(ind)
    jval = jjj(ind)

    ! swap the pivot point with the end point
    call swap_elements(ind,e)

    ! put elements to the right or left of the pivot value
    ind = b
    do i = b, e-1
      if (lesseq(iii(i),jjj(i),ival,jval)) then
        call swap_elements(i,ind)
        ind = ind + 1
      end if
    end do

    ! put the pivot value back at new ind
    call swap_elements(ind,e)

  end subroutine partition
  !---------------------------------------------------------------------------
  subroutine swap_elements(i,j)

     integer, intent(in) :: i, j
     integer :: itmp
     complex :: ctmp
     real :: rtmp

     itmp = iii(i) ; iii(i) = iii(j) ; iii(j) = itmp
     itmp = jjj(i) ; jjj(i) = jjj(j) ; jjj(j) = itmp
     if (lcmplx) then 
       ctmp = mtc(i) ; mtc(i) = mtc(j) ; mtc(j) = ctmp
     end if
     if (lreal) then 
       rtmp = mtr(i) ; mtr(i) = mtr(j) ; mtr(j) = rtmp
     end if

  end subroutine swap_elements
  !---------------------------------------------------------------------------
  function median_of_3_ind(a,c)

    integer, intent(in) :: a, c
    integer :: b, median_of_3_ind

    b = (a + c) / 2
    if (larger(iii(a),jjj(a),iii(b),jjj(b))) then
      if (larger(iii(c),jjj(c),iii(a),jjj(a))) then
        median_of_3_ind = a
      else if (larger(iii(c),jjj(c),iii(b),jjj(b))) then
        median_of_3_ind = c
      else
        median_of_3_ind = b
      end if
    else if (larger(iii(b),jjj(b),iii(c),jjj(c))) then
      if (larger(iii(c),jjj(c),iii(a),jjj(a))) then
        median_of_3_ind = c
      else
        median_of_3_ind = a
      end if
    else
      median_of_3_ind = b
    end if

  end function median_of_3_ind
  !---------------------------------------------------------------------------
  subroutine isort(b,e)

    integer, intent(in) :: b, e
    integer :: ival, jval, i, j
    complex :: val
    real :: rval

    do i = b+1, e
      ival = iii(i)
      jval = jjj(i)
      if (lcmplx) val  = mtc(i)
      if (lreal) rval = mtr(i)
      ajgtval : do j = i-1, 1, -1
        if (less(ival,jval,iii(j),jjj(j))) then
          if (lcmplx) mtc(j+1) = mtc(j)
          if (lreal) mtr(j+1) = mtr(j) 
          iii(j+1)  = iii(j)
          jjj(j+1)  = jjj(j)
        else
          exit ajgtval
        end if
      end do ajgtval
      if (lcmplx) mtc(j+1) = val
      if (lreal) mtr(j+1) = rval  
      iii(j+1)  = ival
      jjj(j+1)  = jval
    end do

  end subroutine isort
  !---------------------------------------------------------------------------
  function larger(ii1,jj1,ii2,jj2)

    integer, intent(in) :: ii1, ii2, jj1, jj2
    logical :: larger

    larger = (ii1 > ii2) .or. (ii1 == ii2 .and. jj1 > jj2)

  end function larger
  !---------------------------------------------------------------------------
  function lesseq(ii1,jj1,ii2,jj2)

    integer, intent(in) :: ii1, ii2, jj1, jj2
    logical :: lesseq

    lesseq =  (ii1 < ii2) .or. (ii1 == ii2 .and. jj1 <= jj2)

  end function lesseq
  !---------------------------------------------------------------------------
  function less(ii1,jj1,ii2,jj2)

    integer, intent(in) :: ii1, ii2, jj1, jj2
    logical :: less

    less = (ii1 < ii2) .or. (ii1 == ii2 .and. jj1 < jj2)

  end function less
  !---------------------------------------------------------------------------

end subroutine sort_matrix

function pos_rad_grid(iphi,ith,ipsi,ipp)

  use geom,           only : npsi_tot
  !use global, only : npsi_tot_global
  use general,        only : gkw_abort
  use index_function, only : indx_gp, indx

  integer, intent(in) :: iphi,ith,ipsi,ipp

  real :: pos_rad_grid
  integer :: kplus,kminus, ipsi_tmp

  kplus = 0
  kminus = 0

  ipsi_tmp = ipsi
  do while (ipsi_tmp < npsi_tot)
    ! if (ith == 3) then
    !   if (ipsi == npsi_tot-1) then
    !     write(*,*)
    !     write(*,*) ipsi, ipsi_tmp, kplus
    !     write(*,*)
    !   end if
    ! end if

    ipsi_tmp = ipsi_tmp + 1
    if(indx_gp(indx(iphi,ith,ipsi_tmp,ipp))) then
      kplus = kplus+1
    end if
    if (kplus == 2) exit
  end do

  ipsi_tmp = ipsi
  do while(ipsi_tmp > 1)
    ipsi_tmp = ipsi_tmp - 1
    if(indx_gp(indx(iphi,ith,ipsi_tmp,ipp))) then
      kminus = kminus-1
    end if
    if (kminus == -2) exit
  end do

  select case(kminus)
  case(0)
    if (kplus == 2) then
      pos_rad_grid = -2
    else
      call gkw_abort('error of pos_rad_grid in matdat')
    end if
  case(-1)
    if (kplus == 2) then
      pos_rad_grid = -1
    else
      call gkw_abort('error of pos_rad_grid in matdat')
    end if
  case(-2)
    pos_rad_grid = 0
    select case (kplus)
    case(2); pos_rad_grid = 0
    case(1); pos_rad_grid = 1
    case(0); pos_rad_grid = 2
    case default
      call gkw_abort('error of pos_rad_grid in matdat')
    end select
  case default
    call gkw_abort('error of pos_rad_grid in matdat')
  end select

  ! if (ith == 3) then
  !   write(*,*) ith,ipsi,kplus,kminus, pos_rad_grid
  ! end if

  ! if (ith == 3) then
  !   if (ipsi == npsi_tot-1 .or. ipsi == npsi_tot) then
  !     write(*,*) ith,ipsi,kplus,kminus, pos_rad_grid
  !     write(*,*) 
  !   end if
  ! end if

  ! if (ipsi == 1) then
  !   pos_rad_grid = -2
  ! else if (ipsi == 2) then
  !   pos_rad_grid = -1
  ! else if (ipsi == npsi_tot-1) then
  !   pos_rad_grid = 1
  ! else if( ipsi == npsi_tot) then
  !   pos_rad_grid = 2
  ! else
  !   pos_rad_grid = 0
  ! end if

end function pos_rad_grid

end module matdat
