module dist

  implicit none

  private

  public :: dist_init, dist_read_nml

  !> Distribution function for both, pfs and cfs
  complex, public, allocatable, save :: fdisi(:)

  !> fft of the distribution function for both, pfs and cfs
  complex, public, allocatable, save :: fdisi_fft(:)

  !> The distribution function including all penetrationpoints on the right side
  !> of the matrix. (Vector which is multiplied with the matrix)
  complex, public, allocatable, save :: fdis_tmp(:)

  !> The matrix has a maximum of ntot elements.
  integer, public, save :: ntot

  !> Amount of rows in fdisi (# 'normal' grid points = nth*npsi_tot)
  integer, public, save :: ntot_geom

  !> Form of the initialized distribution
  character (len=20), public, save :: initial_dist

  !> radial coordiante of the center of a gaussian blob
  real, public, save :: r_c

  !> z-coordinate of the center of a gaussian blob
  real, public, save :: z_c

  !> deviation of a gaussian blob for theta direction
  real, public, save :: blob_deviation_z

  !> devation of a gaussian blob in radial direction (x at initialzation)
  real, public, save :: blob_deviation_psi

  !> Lower Ratio between the maximum of the distribution function after time
  !> step i and at time step 0, at which the program should be canceled
  real, public, save :: dist_decay_min

  !> Upper Ratio between the maximum of the distribution function after time
  !> step i and at time step 0, at which the program should be canceled
  real, public, save :: dist_decay_max

contains

subroutine dist_init

  use geom,         only : gtot, nth, npsi_tot
  use control,      only : grad_npar

  ! Just 'normal' grid points (=cfs,pfs+gp) without penetration points
  ntot_geom = nth*npsi_tot

  !5 point stencil
  !gtot: 'normal' grid points with penetration points
  ntot = gtot + 4*gtot

  if (grad_npar) then
    ntot = ntot + 5*gtot
  end if

  call dist_allocate
  call dist_init_fdis

end subroutine dist_init

subroutine dist_allocate

  integer:: allocate_status

  allocate_status = 0

  allocate(fdisi(ntot),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate fdisi'

  allocate(fdisi_fft(ntot),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate fdisi_fft'

  allocate(fdis_tmp(ntot),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate fdis_tmp'

end subroutine dist_allocate

subroutine dist_init_fdis

  use geom, only : gtot

  integer :: i

  do i = 1, ntot_geom
    fdisi(i) = 0.
  end do
  do i = 1, gtot
    fdis_tmp(i) = 0.
  end do

  form_of_distribution : select case(initial_dist)
    case('gaussian_blob') ; call dist_gaussian_blob
    ! Test scenario with 4 gaussian blobs
    case('gaussian_blob_x4') ;  call dist_gaussian_blob_x4
    case('plane') ; call dist_plane ! plane with slope = radius
    case('const') ; call dist_const
    case default
      stop 'Not a proper selection of the form of the initialized distribution'
  end select form_of_distribution

end subroutine dist_init_fdis

subroutine dist_gaussian_blob

  use geom,      only : gx, gy
  use constants, only : c1

  integer :: i

  write(*,*) 'A gaussian blob is initialized'

  do i = 1, ntot_geom
    fdisi(i) = c1*exp(-1*((gx(i)-r_c)**2 / (2*blob_deviation_psi) &
       & + (gy(i)-z_c)**2 / (2*blob_deviation_z)))
  end do

end subroutine dist_gaussian_blob

subroutine dist_gaussian_blob_x4

  use geom,      only : gx, gy, lim_in_psi, eps
  use constants, only : c1

  integer :: i
  real :: max

  write(*,*) 'Four gaussian blob is initialized'

  ! 0 degree
  do i = 1, ntot_geom
    fdisi(i) = c1*exp(-1*((gx(i)-r_c)**2 / (2*blob_deviation_psi) &
       & + (gy(i)-z_c)**2 / (2*blob_deviation_z)))
  end do

  do i = 1, ntot_geom
    fdisi(i) = fdisi(i) + c1*exp(-1*((gx(i)-(eps-(r_c-lim_in_psi*eps)))**2 / &
       & (2*blob_deviation_psi) &
       & + (gy(i)-z_c)**2 / (2*blob_deviation_z)))
  end do

  ! 180 degree
  do i = 1, ntot_geom
    fdisi(i) = fdisi(i) + c1*exp(-1*((gx(i)+eps-(r_c-lim_in_psi*eps))**2 &
       & / (2*blob_deviation_psi) &
       & + (gy(i)-z_c)**2 / (2*blob_deviation_z)))
  end do

  do i = 1, ntot_geom
    fdisi(i) = fdisi(i) + c1*exp(-1*((gx(i)+r_c)**2 / (2*blob_deviation_psi) &
       & + (gy(i)-z_c)**2 / (2*blob_deviation_z)))
  end do

  ! 90 degree
  do i = 1, ntot_geom
    fdisi(i) = fdisi(i) + c1*exp(-1*((gx(i)-z_c)**2 / &
       & (2*blob_deviation_psi) &
       & + (gy(i)-(lim_in_psi*eps+0.5*eps*(1-lim_in_psi)))**2 &
       & / (2*blob_deviation_z)))
  end do

  max = maxval(real(fdisi))

  do i = 1, ntot_geom
    fdisi(i) = c1*fdisi(i)/max
  end do

end subroutine dist_gaussian_blob_x4


!> plane with slope = radius
subroutine dist_plane

  use geom,      only : gpsi
  use constants, only : c1

  integer :: i

  write(*,*) 'A plane with slope = radius is initialized'

  do i = 1,ntot_geom
    fdisi(i) = c1*gpsi(i)
  end do

end subroutine dist_plane

subroutine dist_const

  use constants, only : c1

  integer :: i

  write(*,*) 'fdisi(i) = const = 1 is initialized'

  do i = 1,ntot_geom
    fdisi(i) = c1*1.
  end do

end subroutine dist_const

subroutine dist_read_nml(control_file_unit)

  integer, intent(in) :: control_file_unit

  namelist /dist/      &
       & initial_dist, r_c, z_c, blob_deviation_z, blob_deviation_psi, &
       & dist_decay_min, dist_decay_max

  read(control_file_unit,NML=dist)

end subroutine dist_read_nml

end module dist
