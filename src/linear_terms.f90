module linear_terms

  implicit none

  private

  public :: calc_linear_terms

contains

subroutine calc_linear_terms

  use control,      only : grad_par, grad_npar, grad_th, grad_z
  use matdat,       only : finish_matrix_section

  write(*,*)
  if (grad_par) then
    write(*,*) 'Calculate the matrix elements of grad_par'
    call term_grad_par
    write(*,*) 'Calculate the matrix elements of disp_par'
    call term_disp_par
  end if

  if(grad_npar) then
    if(grad_th) then
      write(*,*) 'Calculate the matrix elements of grad_th'
      call term_grad_th
      write(*,*) 'Calculate the matrix elements of disp_th'
      call term_disp_th
    end if
    if(grad_z) then
      write(*,*) 'Calculate the matrix elements of grad_psi'
      call term_grad_psi
      write(*,*) 'Calculate the matrix elements of disp_psi'
      call term_disp_psi
    end if
  end if

  write(*,*) 'Finish matrix section'

  ! store the value of nmat (all left hand side terms)
  call finish_matrix_section(1)

  ! stop 'at end of calc_linear_terms'

end subroutine calc_linear_terms

subroutine term_grad_par

  use structures,   only : matrix_element
  use matdat,       only : register_term, set_indx, add_element
  use geom,         only : nth, npsi_tot, dspar, vpmax
  use index_function , only : indx_gp, indx

  !> integer for the loop over all grid points
  integer :: iphi, ith, ipsi, ipp,m

  !Variable to deal with the radial boundary
  integer :: ist

  !Temporary index
  integer :: indx_tmp

  !> the element to attempt
  type (matrix_element) :: elem

  !> dummy variables
  real :: dum, w(5)
  integer :: id,ipw

  !> real array to pass the different path length
  real :: s(4)!

  call register_term(elem)

  elem%term = 'I: vpar_grad_par'

  iphi = 1
  ipp = 0
  !No boundary problems in parallel direction
  ist = 0
  id = +1 !1st derivative / gradient
  ipw = -1 !Use central differences, no boundary

  do ith = 1, nth; do ipsi = 1, npsi_tot
    indx_tmp = indx(iphi,ith,ipsi,ipp)

    dum = - vpmax

    ! indx_tmp = ((ipsi-1)*nth+ith)
    if (indx_gp(indx_tmp)) then
      call set_indx(elem,iphi,ith,ipsi,ipp)
      s(4) = dspar((indx_tmp-1)*4+2)
      s(3) = dspar((indx_tmp-1)*4+1)
      s(2) = dspar((indx_tmp-1)*4+3)
      s(1) = dspar((indx_tmp-1)*4+4)

      call differential_scheme(ist,ipw,id,w,s)

      ! if(ith == 1 .and. (ipsi == 5 .or. ipsi == 1 .or. ipsi == 2)) then
      !   write(*,*) ipsi,'grad_par', w(1),w(2),w(3),w(4),w(5)
      ! end if

      do m = 1, 5
        if (w(m) .ne. 0) then
          call set_indx(elem,iphi,ith,ipsi,ipp)
          elem%ipploc = m-3
          elem%val = w(m)*dum
          call add_element(elem)
        end if
      end do

    end if

  end do;end do

end subroutine term_grad_par

subroutine term_disp_par

  use structures,   only : matrix_element
  use matdat,       only : register_term, set_indx, add_element
  use control,      only : disp_vp
  use geom,         only : nth, npsi_tot, dspar, vpmax
  use index_function , only : indx_gp, indx

  !> integer for the loop over all grid points
  integer :: iphi, ith, ipsi, ipp,m

  !Variable to deal with the radial boundary
  integer :: ist

  !Temporary index
  integer :: indx_tmp

  !> the element to attempt
  type (matrix_element) :: elem

  !> dummy variables
  real :: dum, w(5)
  integer :: id, ipw

  !> real array to pass the different path length
  real :: s(4)!

  call register_term(elem)

  elem%term = 'I: vpar_disp_df'

  iphi = 1
  ipp = 0
  !No boundary problems in parallel direction
  ist = 0
  id = +2
  ipw = +1 !Use central differences, no boundary

  do ith = 1, nth; do ipsi = 1, npsi_tot
    indx_tmp = indx(iphi,ith,ipsi,ipp)
    if (indx_gp((ipsi-1)*nth+ith)) then
      call set_indx(elem,iphi,ith,ipsi,ipp)

      dum = - disp_vp*vpmax

      s(4) = dspar((indx_tmp-1)*4+2)
      s(3) = dspar((indx_tmp-1)*4+1)
      s(2) = dspar((indx_tmp-1)*4+3)
      s(1) = dspar((indx_tmp-1)*4+4)

      call differential_scheme(ist,ipw,id,w,s)

      ! if(ith == 1 .and. (ipsi == 5 .or. ipsi == 1 .or. ipsi == 2)) then
      !   write(*,*) ipsi,'disp_par',w(1),w(2),w(3),w(4),w(5)
      ! end if

      do m = 1, 5
        if (w(m) .ne. 0) then
          call set_indx(elem,iphi,ith,ipsi,ipp)
          elem%ipploc = m-3
          elem%val = w(m)*dum
          call add_element(elem)
        end if
      end do
      !stopx
    end if
  end do; end do
end subroutine term_disp_par

subroutine term_grad_th

  use structures,   only : matrix_element
  use matdat,       only : register_term, set_indx, add_element
  use geom,         only : nth, npsi_tot, dsth, vpmax, vpdz, gx, gpsi
  use index_function , only : indx_gp

  !> integer for the loop over all grid points
  integer :: iphi, ith, ipsi, ipp,m

  !Variable to deal with the radial boundary
  integer :: ist

  !Temporary index
  integer :: indx_temp

  !> the element to attempt
  type (matrix_element) :: elem

  !> dummy variables
  real :: dum, w(5)
  integer :: id,ipw

  !> real array to pass the different path length
  real :: s(4)!

  call register_term(elem)

  elem%term = 'I: vpar_grad_th'

  iphi = 1
  ipp = 0
  !No boundary problems in poloidal direction
  ist = 0
  id = +1
  ipw = +1 ! Use central differences, no boundary

  do ith = 1, nth; do ipsi = 1, npsi_tot
    indx_temp = ((ipsi-1)*nth+ith)
    if (indx_gp((ipsi-1)*nth+ith)) then
      call set_indx(elem,iphi,ith,ipsi,ipp)

      dum = - vpmax*vpdz* gx(indx_temp)/gpsi(indx_temp)

      if (ith == 1) then
        s(1) = dsth((ipsi-1)*nth+nth-1)
        s(2) = dsth((ipsi-1)*nth+nth)
        s(3) = dsth((ipsi-1)*nth+ith)
        s(4) = dsth((ipsi-1)*nth+ith+1)
      else if(ith == 2) then
        s(1) = dsth((ipsi-1)*nth+nth)
        s(2) = dsth((ipsi-1)*nth+ith-1)
        s(3) = dsth((ipsi-1)*nth+ith)
        s(4) = dsth((ipsi-1)*nth+ith+1)
      else if (ith == nth) then
        s(1) = dsth((ipsi-1)*nth+ith-2)
        s(2) = dsth((ipsi-1)*nth+ith-1)
        s(3) = dsth((ipsi-1)*nth+ith)
        s(4) = dsth((ipsi-1)*nth+1)
      else
        s(1) = dsth((ipsi-1)*nth+ith-2)
        s(2) = dsth((ipsi-1)*nth+ith-1)
        s(3) = dsth((ipsi-1)*nth+ith)
        s(4) = dsth((ipsi-1)*nth+ith+1)
      end if
      s(1) = s(1)+s(2)
      s(4) = s(4)+s(3)
      s(1) = -s(1)
      s(2) = -s(2)

      ! if ((ipsi == 1) .and.  ith == 1) then
      !   ! if ((ipsi == 1 .or. ipsi == 2 .or. ipsi == 3) .and.  ith == 1) then
      ! end if


      call differential_scheme(ist,ipw,id,w,s)

      ! if(ith == 1 .and. (ipsi == 5 .or. ipsi == 1 .or. ipsi == 2)) then
      !   write(*,*) ipsi,'grad_th',w(1),w(2),w(3),w(4),w(5)
      ! end if


      ! if (ith == 30 .and. ipsi == 10) then

      !   write(*,*) 'th'
      !   write(*,*) s(1), s(2), s(3), s(4)
      !   write(*,*) w(1), w(2), w(3), w(4), w(5)
      !   write(*,*)
      ! end if

      do m = 1, 5
        if (w(m) .ne. 0) then
          call set_indx(elem,iphi,ith,ipsi,ipp)
            elem%ithloc = ith + m-3

            if(elem%ithloc == 0) then
              elem%ithloc = nth
            else if (elem%ithloc == -1) then
              elem%ithloc = nth-1
            elseif (elem%ithloc == nth+1) then
              elem%ithloc = 1
            else if (elem%ithloc == nth+2) then
              elem%ithloc = 2
            end if

            ! if (ith == 30 .and. ipsi == 10) then
            !   write(*,*) m, w(m), elem%val, elem%ithloc
            ! end if

            elem%val = w(m) * dum
            call add_element(elem)

            ! if (ith == 30 .and. ipsi == 10) then
            !   write(*,*) m, w(m), elem%val, elem%ithloc
            ! end if

          ! if (ith ==1) then
          !   if (ipsi == 5) then
          !     write(*,*) m, elem%val, elem%ithloc
          !     if (m == 5) then
          !       stop
          !     end if
          !   end if
          ! end if

        end if
      end do
    end if
  end do; end do

end subroutine term_grad_th

subroutine term_disp_th

  use structures,   only : matrix_element
  use matdat,       only : register_term, set_indx, add_element
  use control,      only : disp_vnp
  use geom,         only : nth, npsi_tot, dsth, vpmax, vpdz, gx, gpsi
  use index_function , only : indx_gp

  !> integer for the loop over all grid points
  integer :: iphi, ith, ipsi, ipp,m

  !Variable to deal with the radial boundary
  integer :: ist

  !Temporary index
  integer :: indx_temp

  !> the element to attempt
  type (matrix_element) :: elem

  !> dummy variables
  real :: dum, w(5)
  integer :: id,ipw

  !> real array to pass the different path length
  real :: s(4)!

  call register_term(elem)

  elem%term = 'I: vpar_disp_th'

  iphi = 1
  ipp = 0
  !No boundary problems in poloidal direction
  ist = 0
  id = +2
  ipw = +1 !Use central differences, no boundary

  do ith = 1, nth; do ipsi = 1, npsi_tot
    indx_temp = ((ipsi-1)*nth+ith)
    if (indx_gp((ipsi-1)*nth+ith)) then
      call set_indx(elem,iphi,ith,ipsi,ipp)

      dum = -disp_vnp*vpmax*vpdz* gx(indx_temp)/gpsi(indx_temp)

      if (ith == 1) then
        s(1) = dsth((ipsi-1)*nth+nth-1)
        s(2) = dsth((ipsi-1)*nth+nth)
        s(3) = dsth((ipsi-1)*nth+ith)
        s(4) = dsth((ipsi-1)*nth+ith+1)
      else if(ith == 2) then
        s(1) = dsth((ipsi-1)*nth+nth)
        s(2) = dsth((ipsi-1)*nth+ith-1)
        s(3) = dsth((ipsi-1)*nth+ith)
        s(4) = dsth((ipsi-1)*nth+ith+1)
      else if (ith == nth) then
        s(1) = dsth((ipsi-1)*nth+ith-2)
        s(2) = dsth((ipsi-1)*nth+ith-1)
        s(3) = dsth((ipsi-1)*nth+ith)
        s(4) = dsth((ipsi-1)*nth+1)
      else
        s(1) = dsth((ipsi-1)*nth+ith-2)
        s(2) = dsth((ipsi-1)*nth+ith-1)
        s(3) = dsth((ipsi-1)*nth+ith)
        s(4) = dsth((ipsi-1)*nth+ith+1)
      end if
      s(1) = s(1)+s(2)
      s(4) = s(4)+s(3)
      s(1) = -s(1)
      s(2) = -s(2)

      call differential_scheme(ist,ipw,id,w,s)

      ! if(ith == 1 .and. (ipsi == 5 .or. ipsi == 1 .or. ipsi == 2)) then
      !   write(*,*) ipsi,'disp_th',w(1),w(2),w(3),w(4),w(5)
      ! end if

      do m = 1, 5
        if (w(m) .ne. 0) then
          call set_indx(elem,iphi,ith,ipsi,ipp)
            elem%ithloc = ith + m-3

            if(elem%ithloc == 0) then
              elem%ithloc = nth
            else if (elem%ithloc == -1) then
              elem%ithloc = nth-1
            elseif (elem%ithloc == nth+1) then
              elem%ithloc = 1
            else if (elem%ithloc == nth+2) then
              elem%ithloc = 2
            end if
          elem%val = w(m)*dum
          !write(*,*) elem%val, w(m)
          call add_element(elem)
        end if
      end do
      !stop
    end if
  end do; end do

end subroutine term_disp_th

subroutine term_grad_psi

  use structures,      only : matrix_element
  use matdat,          only : register_term, set_indx, add_element, pos_rad_grid
  use geom,            only : nth, npsi_tot, dspsi_true, npsi_tot, vpmax, vpdz, &
     & gy, gpsi
  use index_function , only : indx_gp, indx
  use general,         only : gkw_abort

  !> integer for the loop over all grid points
  integer :: iphi, ith, ipsi, ipp,m

  !Variable to deal with the radial boundary
  integer :: ist, indxpsi(5),bpsi,id,ipw

  !Temporary index
  integer :: indx_temp

  !> the element to attempt
  type (matrix_element) :: elem

  !> dummy variables
  real :: dum, w(5)
  integer :: kpsi,k

  !> real array to pass the different path length
  real :: s(4)!

  integer :: arr(2)

  call register_term(elem)

  elem%term = 'I: vpar_grad_psi'

  iphi = 1
  ipp = 0
  id = +1

  do ith = 1, nth; do ipsi = 1, npsi_tot
    indx_temp = ((ipsi-1)*nth+ith)

    ! Do not calculate linear terms for the ghost points
    if (indx_gp((ipsi-1)*nth+ith)) then
      call set_indx(elem,iphi,ith,ipsi,ipp)

      do k = 1,4
        s(k) = 0
      end do

      ! Velocity
      dum = - vpmax*vpdz *gy(indx_temp)/gpsi(indx_temp)

      ! the direction of the radial motion
      if (dum > 0) then
        ! Motion towards inner limiting flux surface
        ipw = 1
      else
        ! Motion towards outer limiting flux surface
        ipw = -1
      end if
      ist = pos_rad_grid(iphi,ith,ipsi,ipp)

      call linear_terms_indx_psi(iphi,ith,ipsi,ipp,ist,ipw,indxpsi,bpsi)
      call linear_terms_dspsi(iphi,ith,ipsi,ipp,indxpsi,bpsi,ipw,s)
      do k = 1,4
        if (bpsi > k) then
          s(k) = -s(k)
        end if
      end do

      ! write(*,*) ith, ipsi, ist, bpsi
      ! select case(ist)
      ! case(-2,-1,0,1,2)
      ! case default
      !   write(*,*) 'ist', ist
      ! end select

      ! select case(ipw)
      ! case(-1,1)
      ! case default
      !   write(*,*) 'ipw', ipw
      ! end select

      ! if(ith == 64) then !.and. (ipsi == 9 .or. ipsi == 10)) then
      !   write(*,*) ith, ipsi, ist, ipw,s(1),s(2),s(3),s(4)
      ! end if

      call differential_scheme(ist,ipw,id,w,s)

      ! if (ith == 1 .or. ith == 15) then
      !   write(*,*) ith, ipsi, ist, ipw,s(1),s(2),s(3),s(4)
      ! end if

      ! if(ith == 1 .and. (ipsi == 5 .or. ipsi == 1 .or. ipsi == 2 &
      !    & .or.ipsi == npsi_tot-1 .or. ipsi == npsi_tot)) then
        ! write(*,*) ith,ipsi, 'grad_psi',w(1),w(2),w(3),w(4),w(5)
      ! end if


      ! if (ith == 31) then
      !   select case(ipsi)
      !     case default
      !       write(*,*) ipsi, s(1), s(2), s(3), s(4)
      !   end select
      ! end if

      do m = 1, 5
        if (w(m) .ne. 0) then
          call set_indx(elem,iphi,ith,ipsi,ipp)
          elem%ipsiloc = indxpsi(m)
          if (indxpsi(m) == -1) then
            call gkw_abort('term_grad_psi: indxpsi = -1, gp value')
          end if
          elem%ipsiloc = indxpsi(m)
          elem%val = w(m)*dum
          call add_element(elem)

          ! if (ith == 3 .or. ith == 32) then
          !   select case(ipsi)
          !   case(14,15,16,17,18,19)
          !     write(*,*) m, elem%val, elem%ipsiloc
          !   end select
          ! end if

        end if
      end do

      ! if (ith == 1 .or. ith == 34) then
      ! if (ith == 15) then ! .or. ith == 34) then
      !   select case(ipsi)
      !     ! case(10,11,12)
      !   case(10,11,12)
      !     write(*,*) ith,ipsi,indxpsi(3),ist,ipw,'|',w(1),w(2),w(3),w(4),w(5),&
      !        & '|',s(1),s(2),s(3),s(4)
      !   end select
      ! end if

      ! if (ith == 34) then
      !   select case(ipsi)
      !   case(1,2,3)
      !     write(*,*) ith,ipsi,indxpsi(3),ist,ipw,'|',w(1),w(2),w(3),w(4),w(5),&
      !        & '|',s(1),s(2),s(3),s(4)
      !   end select
      ! end if

      ! if(ith == 15) then
      !   select case(ipsi)
      !   case(10,11,12)
      !     !       write(*,*) ipsi, w(1)*dum, w(2)*dum, w(3)*dum, w(4)*dum, w(5)*dum
      !     write(*,*) ipsi, w(1),w(2),w(3),w(4),w(5), ist, ipw
      !   end select
      ! end if

      ! if (ith == 31) then
      !   select case(ipsi)
      !     case default
      !       write(*,*) ipsi, w(1)*dum, w(2)*dum, w(3)*dum, w(4)*dum, w(5)*dum
      !     end select
      ! end if


    end if
  end do; end do
  ! stop 'at end of term_grad_psi'

end subroutine term_grad_psi

subroutine term_disp_psi
  use structures,   only : matrix_element
  use matdat,       only : register_term, set_indx, add_element, pos_rad_grid
  use geom,         only : nth, npsi_tot, dspsi, npsi_tot, vpmax, vpdz, gy, gpsi
  use index_function , only : indx_gp, indx
  use control, only      : disp_vnp

  !> integer for the loop over all grid points
  integer :: iphi, ith, ipsi, ipp,m

  !Variable to deal with the radial boundary
  integer :: ist, indxpsi(5),bpsi,id,ipw

  !Temporary index
  integer :: indx_temp

  !> the element to attempt
  type (matrix_element) :: elem

  !> dummy variables
  real :: dum, w(5)
  integer :: kpsi,k

  !> real array to pass the different path length
  real :: s(4)!

  integer :: arr(2)

  call register_term(elem)

  elem%term = 'I: vpar_disp_psi'

  iphi = 1
  ipp = 0
  !No boundary problems in parallel direction

  id = +2

  do ith = 1, nth; do ipsi = 1, npsi_tot
    indx_temp = ((ipsi-1)*nth+ith)
    if (indx_gp((ipsi-1)*nth+ith)) then
      call set_indx(elem,iphi,ith,ipsi,ipp)

      do k = 1,4
        s(k) = 0
      end do

      ! Velocity
      dum = - disp_vnp*vpmax*vpdz *gy(indx_temp)/gpsi(indx_temp)

      ! the direction of the radial motion
      if (dum > 0) then
        ipw = 1
      else
        ipw = -1
      end if
      ist = pos_rad_grid(iphi,ith,ipsi,ipp)

      ! Central differences on all points.
      ! One not jused ghost point at boundary cells and one zero ghost point
      ! at boundary cells and adjacent cells, if cells lie in backwind direction
      ! Otherwise all w(i) = 0 in upwind direction
      ! if (ist == -2) then
      !   bpsi = 3
      !   s(1) = dspsi(indx(iphi,ith,ipsi+1,ipp)) ! Not used because of 2nd order
      !   s(2) = dspsi(indx(iphi,ith,ipsi,ipp)) ! zero ghost point
      !   s(3) = dspsi(indx(iphi,ith,ipsi,ipp))
      !   s(4) = dspsi(indx(iphi,ith,ipsi+1,ipp))
      !   s(1) = s(1)+s(2)
      !   s(4) = s(4)+s(3)
      ! else if (ist == -1) then
      !   bpsi = 3
      !   s(1) = dspsi(indx(iphi,ith,ipsi-1,ipp)) ! zero ghost point
      !   s(2) = dspsi(indx(iphi,ith,ipsi-1,ipp))
      !   s(3) = dspsi(indx(iphi,ith,ipsi,ipp))
      !   s(4) = dspsi(indx(iphi,ith,ipsi+1,ipp))
      !   s(1) = s(1)+s(2)
      !   s(4) = s(4)+s(3)
      ! else if (ist == 1) then
      !   bpsi = 3
      !   s(1) = dspsi(indx(iphi,ith,ipsi-2,ipp))
      !   s(2) = dspsi(indx(iphi,ith,ipsi-1,ipp))
      !   s(3) = dspsi(indx(iphi,ith,ipsi,ipp))
      !   s(4) = dspsi(indx(iphi,ith,ipsi,ipp))
      !   s(1) = s(1)+s(2)
      !   s(4) = s(4)+s(3) ! zero ghost point
      ! else if (ist == 2) then
      !   bpsi = 3
      !   s(1) = dspsi(indx(iphi,ith,ipsi-2,ipp))
      !   s(2) = dspsi(indx(iphi,ith,ipsi-1,ipp))
      !   s(3) = dspsi(indx(iphi,ith,ipsi-1,ipp))
      !   s(4) = dspsi(indx(iphi,ith,ipsi-1,ipp))
      !   s(1) = s(1)+s(2) ! zero ghost point
      !   s(4) = s(4)+s(3) ! Not used because of 2nd order

      ! else
      !   ! Normal case
      !   call linear_terms_indx_psi(iphi,ith,ipsi,ipp,ist,ipw,indxpsi,bpsi)
      !   ! The distance between the index point and the stencil point is the
      !   ! total distance betwenn the two
      !   do k = 1,4
      !     s(k) = 0
      !     arr(1) = indxpsi(k)
      !     arr(2) = indxpsi(bpsi)

      !     if (indxpsi(bpsi) == minval(arr)) then
      !       do kpsi = minval(arr), maxval(arr)
      !         s(k) = s(k) + dspsi(kpsi)
      !       end do

      !     else
      !       do kpsi = minval(arr), maxval(arr)-1
      !         s(k) = s(k) + dspsi(kpsi)
      !       end do
      !     end if
      !   end do

      ! end if

      call linear_terms_indx_psi(iphi,ith,ipsi,ipp,ist,ipw,indxpsi,bpsi)

      call linear_terms_dspsi(iphi,ith,ipsi,ipp,indxpsi,bpsi,ipw,s)


      ! Invert the sign for the distances to stencil points which lie on a
      ! smaller radius
      do k = 1,4
        if (bpsi > k) then
          s(k) = -s(k)
        end if
      end do

      call differential_scheme(ist,ipw,id,w,s)

      ! if(ith == 1 .and. (ipsi == 5 .or. ipsi == 1 .or. ipsi == 2 &
      !    & .or.ipsi == npsi_tot-1 .or. ipsi == npsi_tot)) then
      !   write(*,*) ipsi,'disp_psi',w(1),w(2),w(3),w(4),w(5) !, &
      !      ! & s(1),s(2),s(3),s(4)
      ! end if


      do m = 1, 5
        if (w(m) .ne. 0) then
          call set_indx(elem,iphi,ith,ipsi,ipp)
          elem%ipsiloc = ipsi+m-bpsi
          elem%val = w(m) * dum
          call add_element(elem)
        end if
      end do

      ! if(ith ==15) then

      !   select case(ipsi)
      !   case(1,2,3,10,11,12)
      !     write(*,*) ipsi, w(1),w(2),w(3),w(4),w(5), ist, ipw
      !   end select

      ! end if

    end if
  end do; end do

end subroutine term_disp_psi

subroutine differential_scheme(ist,ipw,id,w,s)

  use control, only : grad_disc
  use general, only : gkw_abort

  integer, intent(in) :: ist  ! position on the field line
  integer,  intent(in) :: ipw  ! +1 / -1 up / dow wind
  ! Special consideratrion on boundary.
  ! +1: upwind on right boundary, central differences with zero ghost zell on
  ! left boundary
  ! -1:
  integer, intent(in) :: id   ! Gradient(1st deriv) or diffusion
  real, intent(out) :: w(5) ! stencil
  ! Distances between grid points necessary for ununiform spacing
  real, optional, intent(in) :: s(4)

  !uniform spacing
  real :: vfocm2pd(5), dfocm2pd(5), vfocm2nd(5), dfocm2nd(5)
  real :: vfocm1pd(5), dfocm1pd(5), vfocm1nd(5), dfocm1nd(5)
  real :: vfoc00(5), dfoc00(5)
  real :: vfocp1pd(5), dfocp1pd(5), vfocp1nd(5), dfocp1nd(5)
  real :: vfocp2pd(5), dfocp2pd(5), vfocp2nd(5), dfocp2nd(5)
  real :: vsocm2pd(5), dsocm2pd(5), vsocm2nd(5), dsocm2nd(5)
  real :: vsoc00(5), dsoc00(5)
  real :: vsocp2pd(5), dsocp2pd(5), vsocp2nd(5), dsocp2nd(5)
  real :: d2foc00(5)


  !###########################################
  !### uniform spacing - use schemes from gkw
  !###########################################

  ! Second order backwinded difference scheme
  data vfocm2pd / 0.E0,   0.E0, -18.E0,  24.E0,  -6.E0 /
  data dfocm2pd / 0.E0,   0.E0,   0.E0,   0.E0,   0.E0 /
  ! Centre differenced second order scheme with a zero ghost cell
  data vfocm2nd / 0.E0,   0.E0,   0.E0,   6.E0,   0.E0 /
  data dfocm2nd / 0.E0,   0.E0, -24.E0,  12.E0,   0.E0 /
  ! Third order backwinded difference scheme
  data vfocm1pd / 0.E0,  -4.E0,  -6.E0,  12.E0,  -2.E0 /
  data dfocm1pd / 0.E0,   0.E0,   0.E0,   0.E0,   0.E0 /
  ! Fourth order central difference with a ghost cell
  data vfocm1nd / 0.E0,  -8.E0,   0.E0,   8.E0,  -1.E0 /
  data dfocm1nd / 0.E0,   4.E0,  -6.E0,   4.E0,  -1.E0 /
  ! Fourth order 5 point stencil
  data vfoc00   / 1.E0,  -8.E0,   0.E0,   8.E0,  -1.E0 /   ! 4th order 1st deriv
  data dfoc00   /-1.E0,   4.E0,  -6.E0,   4.E0,  -1.E0 /   ! 2nd order 4th deriv (* -1)
  data d2foc00  /-1.E0,  16.E0, -30.E0,  16.E0,  -1.E0 /   ! 4th order 2nd deriv

  ! Fourth order central difference with a ghost cell
  data vfocp1pd / 1.E0,  -8.E0,   0.E0,   8.E0,   0.E0 /
  data dfocp1pd /-1.E0,   4.E0,  -6.E0,   4.E0,   0.E0 /
  ! Third order backwinded scheme
  data vfocp1nd / 2.E0, -12.E0,   6.E0,   4.E0,   0.E0 /
  data dfocp1nd / 0.E0,   0.E0,   0.E0,   0.E0,   0.E0 /
  ! Second order central difference with a zero ghost cell
  data vfocp2pd / 0.E0,  -6.E0,   0.E0,   0.E0,   0.E0 /
  data dfocp2pd / 0.E0,  12.E0, -24.E0,   0.E0,   0.E0 /
  ! Second order backwinded scheme
  data vfocp2nd / 6.E0, -24.E0,  18.E0,   0.E0,   0.E0 /
  data dfocp2nd / 0.E0,   0.E0,   0.E0,   0.E0,   0.E0 /

!---- Second order scheme (with treatment of end points) ------
  ! Second order 3 point stencil
  data vsoc00   / 0.E0,  -6.E0,   0.E0,   6.E0,   0.E0 /
  data dsoc00   / 0.E0,  12.E0, -24.E0,  12.E0,   0.E0 /  ! 2nd order 2nd deriv
  data vsocm2pd / 0.E0,   0.E0, -12.E0,  12.E0,   0.E0 /  ! check sign
  data dsocm2pd / 0.E0,   0.E0,   0.E0,   0.E0,   0.E0 /
  data vsocm2nd / 0.E0,   0.E0,   0.E0,   6.E0,   0.E0 /
  data dsocm2nd / 0.E0,   0.E0,   0.E0,   0.E0,   0.E0 /
  data vsocp2pd / 0.E0,  -6.E0,   0.E0,   0.E0,   0.E0 /
  data dsocp2pd / 0.E0,   0.E0,   0.E0,   0.E0,   0.E0 /
  data vsocp2nd / 0.E0, -12.E0,  12.E0,   0.E0,   0.E0 / ! check sign
  data dsocp2nd / 0.E0,   0.E0,   0.E0,   0.E0,   0.E0 /

  !###########################################
  !### nonuniform/irregular spacing
  !### Note: Here normalization not allowed.
  !###########################################

  ! 1st derivative/gradient
  if (id.eq.1) then

    if( .not. present(s)) then
      !Do the proper normalization.
      w = w / 12.E0
    else

      select case(grad_disc)

      case(1)
        select case(ist)
        case(-2)
        case(-1,0,1)
        case(2)
        end select

      case(2)

        ! call differential_scheme_cd_grad(s,w)

        select case(ist)

        case(-2)
          !To change
          if (ipw .eq. -1) then
            call differential_scheme_cd2_grad(s,w)
            w(1) = 0.
            w(2) = 0.
          else if(ipw .eq. +1) then
            call differential_scheme_bw2_grad_gc_l(s,w)
          end if


        case(-1)
          !To change
          if (ipw .eq. -1) then
            call differential_scheme_cd_grad(s,w)
            w(1) = 0.
          else if(ipw .eq. +1) then
            call differential_scheme_bw3_grad_ac_l(s,w)
          end if


        case(0)
          !To change
          if (ipw .eq. 1 .or. ipw .eq. -1) then
            call differential_scheme_cd_grad(s,w)
          end if

        case(1)
          !To change
          if (ipw .eq. 1) then
            call differential_scheme_cd_grad(s,w)
            w(5) = 0.
          else if (ipw .eq. -1) then
            call differential_scheme_bw3_grad_ac_r(s,w)
          end if

        case(2)
          !To change
          if (ipw .eq. 1) then
            call differential_scheme_cd2_grad(s,w)
            w(4) = 0.
            w(5) = 0.
          else if (ipw .eq. -1) then
            call differential_scheme_bw2_grad_gc_r(s,w)
          end if

        case default
          call gkw_abort('No proper ist in linear_terms - differential_scheme')

        end select

      end select

    end if

  ! (Hyper-) diffusive dissipation
  else if(id.eq.2) then

    if( .not. present(s)) then
      !Do the proper normalization.
      w = w / 12.E0
    else
      select case(grad_disc)

      case(1)
        select case(ist)
        case(-2)
        case(-1,0,1)
        case(2)
        end select

      case(2)

        select case(ist)
        case(-2)
          if(ipw .eq. -1) then
            call differential_scheme_cd2_diss(s,w)
            w(1) = 0.
            w(2) = 0.
          else if (ipw.eq. +1) then
            w = dfocm2pd ! upwind direction 0,0,0,0,0
          end if
        case(-1)
          if(ipw .eq. - 1) then
            call differential_scheme_cd4_diss(s,w)
            w(1) = 0.
          else if (ipw.eq.+1) then
            w = dfocm1pd ! upwind direction 0,0,0,0,0
          end if
        case(0)
          if (ipw .eq. 1 .or. ipw .eq. -1) then
            call differential_scheme_cd4_diss(s,w)
          end if
        case(1)
          if (ipw .eq. +1) then
            call differential_scheme_cd4_diss(s,w)
            w(5) = 0.
          else if (ipw.eq.-1) then
            w = dfocp1nd ! upwind direction 0,0,0,0,0
          end if
        case(2)
          if (ipw .eq. +1) then
            call differential_scheme_cd2_diss(s,w)
            w(4) = 0.
            w(5) = 0.
          else if (ipw.eq.-1) then
            w = dfocp2nd ! upwind direction 0,0,0,0,0
          end if
        end select
      end select

    end if


  else

    stop 'linear_terms: unknown differential derivative'

  end if


end subroutine differential_scheme

subroutine linear_terms_indx_psi(iphi,ith,ipsi,ipp,ist,ipw,indxpsi,bpsi)

  use geom,           only : npsi_tot
  use index_function, only : indx_gp, indx

  integer, intent(in) :: iphi, ith, ipsi, ipp
  integer, intent(in) :: ist,ipw
  integer, intent(out) :: indxpsi(5)
  !indxpsi(bpsi)=ipsi
  integer, intent(out) :: bpsi

  integer :: k

  if (ist == -2 .and. ipw == -1) then
    bpsi = 3
    indxpsi(1) = -1
    indxpsi(2) = -1
    indxpsi(3) = ipsi
    k = ipsi
    k = linear_terms_psi_next(iphi,ith,k,ipp,+1)
    indxpsi(4) = k
    k = linear_terms_psi_next(iphi,ith,k,ipp,+1)
    indxpsi(5) = k

  else if(ist == -1 .and. ipw == -1) then
    bpsi = 3
    indxpsi(1) = -1
    indxpsi(2) = 1
    indxpsi(3) = ipsi
    k = ipsi
    k = linear_terms_psi_next(iphi,ith,k,ipp,+1)
    indxpsi(4) = k
    k = linear_terms_psi_next(iphi,ith,k,ipp,+1)
    indxpsi(5) = k

  else if(ist == 1 .and. ipw == 1) then
    bpsi = 3
    indxpsi(3) = ipsi
    k = ipsi
    k = linear_terms_psi_next(iphi,ith,k,ipp,-1)
    indxpsi(2) = k
    k = linear_terms_psi_next(iphi,ith,k,ipp,-1)
    indxpsi(1) = k
    indxpsi(4) = npsi_tot
    indxpsi(5) = -1

  else if (ist == 2 .and. ipw == 1) then
    bpsi = 3
    indxpsi(3) = ipsi ! npsi_tot
    k = ipsi
    k = linear_terms_psi_next(iphi,ith,k,ipp,-1)
    indxpsi(2) = k
    k = linear_terms_psi_next(iphi,ith,k,ipp,-1)
    indxpsi(1) = k
    indxpsi(4) = -1
    indxpsi(5) = -1
  else

    if (ipsi == 1) then
      bpsi = 1
      k = ipsi
      indxpsi(bpsi) = k ! indxpsi(1)
      k = linear_terms_psi_next(iphi,ith,ipsi,ipp,+1)
      indxpsi(bpsi+1) = k ! indxpsi(2)
      k = linear_terms_psi_next(iphi,ith,k,ipp,+1)
      indxpsi(bpsi+2) = k ! indxpsi(3)
      k = linear_terms_psi_next(iphi,ith,k,ipp,+1)
      indxpsi(bpsi+3) = k ! indxpsi(4)
      k = linear_terms_psi_next(iphi,ith,k,ipp,+1)
      indxpsi(bpsi+4) = k ! indxpsi(5)

    else if(ipsi == 2) then
      bpsi=2
      k = ipsi
      indxpsi(bpsi) = k ! indxpsi(2)
      k = linear_terms_psi_next(iphi,ith,ipsi,ipp,-1)
      indxpsi(bpsi-1) = k ! indxpsi(1)
      k = linear_terms_psi_next(iphi,ith,ipsi,ipp,+1)
      indxpsi(bpsi+1) = k ! indxpsi(3)
      k = linear_terms_psi_next(iphi,ith,k,ipp,+1)
      indxpsi(bpsi+2) = k ! indxpsi(4)
      k = linear_terms_psi_next(iphi,ith,k,ipp,+1)
      indxpsi(bpsi+3) = k ! indxpsi(5)

    else if(ipsi == npsi_tot) then
      bpsi = 5
      k = ipsi
      indxpsi(bpsi) = k ! indxpsi(5)
      k = linear_terms_psi_next(iphi,ith,ipsi,ipp,-1)
      indxpsi(bpsi-1) = k ! indxpsi(4)
      k = linear_terms_psi_next(iphi,ith,k,ipp,-1)
      indxpsi(bpsi-2) = k ! indxpsi(3)
      k = linear_terms_psi_next(iphi,ith,k,ipp,-1)
      indxpsi(bpsi-3) = k ! indxpsi(2)
      k = linear_terms_psi_next(iphi,ith,k,ipp,-1)
      indxpsi(bpsi-4) = k ! indxpsi(1)

    else if(ipsi == npsi_tot-1) then
      bpsi=4
      k = ipsi
      indxpsi(bpsi) = k ! indxpsi(4)
      k = linear_terms_psi_next(iphi,ith,ipsi,ipp,+1)
      indxpsi(bpsi+1) = k ! indxpsi(5)
      k = linear_terms_psi_next(iphi,ith,ipsi,ipp,-1)
      indxpsi(bpsi-1) = k ! indxpsi(3)
      k = linear_terms_psi_next(iphi,ith,k,ipp,-1)
      indxpsi(bpsi-2) = k ! indxpsi(2)
      k = linear_terms_psi_next(iphi,ith,k,ipp,-1)
      indxpsi(bpsi-3) = k ! indxpsi(1)

    else
      if (linear_terms_psi_next(iphi,ith,ipsi,ipp,-1)==1) then
        bpsi=2
        k = ipsi
        indxpsi(bpsi) = k ! indxpsi(2)
        k = linear_terms_psi_next(iphi,ith,ipsi,ipp,-1)
        indxpsi(bpsi-1) = k ! indxpsi(1)
        k = linear_terms_psi_next(iphi,ith,ipsi,ipp,+1)
        indxpsi(bpsi+1) = k ! indxpsi(3)
        k = linear_terms_psi_next(iphi,ith,k,ipp,+1)
        indxpsi(bpsi+2) = k ! indxpsi(4)
        k = linear_terms_psi_next(iphi,ith,k,ipp,+1)
        indxpsi(bpsi+3) = k ! indxpsi(5)

      else if(linear_terms_psi_next(iphi,ith,ipsi,ipp,+1)==npsi_tot) then
        bpsi=4
        k = ipsi
        indxpsi(bpsi) = k ! indxpsi(4)
        k = linear_terms_psi_next(iphi,ith,ipsi,ipp,+1)
        indxpsi(bpsi+1) = k ! indxpsi(5)
        k = linear_terms_psi_next(iphi,ith,ipsi,ipp,-1)
        indxpsi(bpsi-1) = k ! indxpsi(3)
        k = linear_terms_psi_next(iphi,ith,k,ipp,-1)
        indxpsi(bpsi-2) = k ! indxpsi(2)
        k = linear_terms_psi_next(iphi,ith,k,ipp,-1)
        indxpsi(bpsi-3) = k ! indxpsi(1)

      else
        bpsi = 3
        k = ipsi
        indxpsi(bpsi) = k ! indxpsi(3)
        k = linear_terms_psi_next(iphi,ith,ipsi,ipp,-1)
        indxpsi(bpsi-1) = k ! indxpsi(2)
        k = linear_terms_psi_next(iphi,ith,k,ipp,-1)
        indxpsi(bpsi-2) = k ! indxpsi(1)
        k = linear_terms_psi_next(iphi,ith,ipsi,ipp,+1)
        indxpsi(bpsi+1) = k ! indxpsi(4)
        k = linear_terms_psi_next(iphi,ith,k,ipp,+1)
        indxpsi(bpsi+2) = k ! indxpsi(5)
      end if
    end if
  end if

  ! Test
  ! write(*,*) 'ipsi: ',ipsi, 'indxpsi: ',indxpsi,',bpsi: ', bpsi

  if(any(indxpsi==0)) then
    write(*,*) 'ipsi: ', ipsi, ',indxpsi: ', indxpsi, ',bpsi : ', bpsi
    write(*,*) 'gigi aborted, because indxpsi with value 0'
    stop
  end if

end subroutine linear_terms_indx_psi

subroutine linear_terms_dspsi(iphi,ith,ipsi,ipp,indxpsi,bpsi,ipw,s)

  use geom,           only : dspsi_true,npsi_tot
  use general,        only : gkw_abort
  use index_function, only : indx

  integer, intent(in) :: iphi,ith,ipsi,ipp
  integer, intent(in) :: indxpsi(5),bpsi,ipw
  real, intent(out) :: s(4)

  integer :: k,kpsi
  integer :: arr(2)

  if (ipsi ==1 .and. ipw == -1) then
    if (ipsi /= indxpsi(3)) then
      call gkw_abort('linear_terms_dspsi: ipsi =/ indxpsi(3)')
    end if

    s(3) = dspsi_true(indx(iphi,ith,ipsi,ipp))
    s(4) = dspsi_true(indx(iphi,ith,indxpsi(4),ipp))

    s(1) = s(3)
    s(2) = s(4)

    s(1) = s(1) + s(2)
    s(4) = s(4) + s(3)

    ! if(ith == 40 .and. (ipsi == 9 .or. ipsi == 10)) then
    !     write(*,*) '1',ith, ipsi,ipw,s(1),s(2),s(3),s(4)
    !   end if


  else if (ipsi == linear_terms_psi_next(iphi,ith,1,ipp,+1) &
     & .and. ipw == -1) then
    if (ipsi /= indxpsi(3)) then
      call gkw_abort('linear_terms_dspsi: ipsi =/ indxpsi(3)')
    end if


    s(2) = dspsi_true(indx(iphi,ith,indxpsi(2),ipp))
    s(3) = dspsi_true(indx(iphi,ith,ipsi,ipp))
    s(4) = dspsi_true(indx(iphi,ith,indxpsi(4),ipp))

    s(1) = s(2)

    s(1) = s(1) + s(2)
    s(4) = s(4) + s(3)

    ! if(ith == 40 .and. (ipsi == 9 .or. ipsi == 10)) then
    !     write(*,*) '2',ith, ipsi,ipw,s(1),s(2),s(3),s(4)
    !   end if


  else if (ipsi == linear_terms_psi_next(iphi,ith,npsi_tot,ipp,-1) &
     & .and. ipw == 1) then
    if (ipsi /= indxpsi(3)) then
      call gkw_abort('linear_terms_dspsi: ipsi =/ indxpsi(3)')
    end if

    s(1) = dspsi_true(indx(iphi,ith,indxpsi(1),ipp))
    s(2) = dspsi_true(indx(iphi,ith,indxpsi(2),ipp))
    s(3) = dspsi_true(indx(iphi,ith,ipsi,ipp))

    s(4) = s(3)

    s(1) = s(1) + s(2)
    s(4) = s(4) + s(3)

    ! if(ith == 40 .and. (ipsi == 9 .or. ipsi == 10)) then
    !     write(*,*) '3',ith, ipsi, ipw,s(1),s(2),s(3),s(4)
    !   end if


  else if (ipsi == npsi_tot .and. ipw == 1) then
    if (ipsi /= indxpsi(3)) then
      call gkw_abort('linear_terms_dspsi: ipsi =/ indxpsi(3)')
    end if
    s(1) = dspsi_true(indx(iphi,ith,indxpsi(1),ipp))
    s(2) = dspsi_true(indx(iphi,ith,indxpsi(2),ipp))

    s(3) = s(2)
    s(4) = s(1)

    s(1) = s(1) + s(2)
    s(4) = s(4) + s(3)

    ! if(ith == 40 .and. (ipsi == 9 .or. ipsi == 10)) then
    !     write(*,*) '4',ith, ipsi, ipw,s(1),s(2),s(3),s(4)
    !   end if


    ! if (ith == 66) then
    !   if (ipsi == 25) then
    !     write(*,*) s(1), s(2), s(3), s(4)
    !   end if
    ! end if

  else

    do k = 1, 4
      s(k) = 0
      arr(1) = indxpsi(k)
      arr(2) = indxpsi(bpsi)

      if (indxpsi(bpsi) == minval(arr)) then
        do kpsi = minval(arr), maxval(arr)
          s(k) = s(k) + dspsi_true(indx(iphi,ith,kpsi,ipp))
        end do

        ! if(ith == 40 .and. (ipsi == 9 .or. ipsi == 10)) then
        !   write(*,*) '5.1', k, s(k)
        ! end if

      else
        do kpsi = minval(arr), maxval(arr)-1
          s(k) = s(k) + dspsi_true(indx(iphi,ith,kpsi,ipp))

          ! if(ith == 40 .and. (ipsi == 9 .or. ipsi == 10)) then
          !   write(*,*) '6..', k,minval(arr),maxval(arr)-1,kpsi,s(k), &
          !      & dspsi_true(indx(iphi,ith,kpsi,ipp)), indx(iphi,ith,kpsi,ipp)
          ! end if

      end do

        ! if(ith == 40 .and. (ipsi == 9 .or. ipsi == 10)) then
        !   write(*,*) '5.2', k, s(k)
        ! end if

      end if
    end do

    ! if(ith == 40 .and. (ipsi == 9 .or. ipsi == 10)) then
    !     write(*,*) '5',ith, ipsi, ipw,s(1),s(2),s(3),s(4)
    !   end if

  end if

end subroutine linear_terms_dspsi

function linear_terms_psi_next(iphi,ith,ipsi,ipp,sig)

  use index_function, only : indx_gp, indx
  use geom,           only : gtot, npsi_tot
  use general,        only : gkw_abort

  integer, intent(in) :: iphi,ith,ipsi, ipp
  integer, intent(in) :: sig !-1/+1, -> -psi/+psi direction

  integer :: linear_terms_psi_next
  integer :: k

  k = ipsi
  if (sig == +1) then
    k = k+1
    do while(indx_gp(indx(iphi,ith,k,ipp)).eqv. .false.)
      k = k+1
      if (k<1 .or. k>npsi_tot) then
        call gkw_abort('k<1 or > npsi_tot in linear_terms_psi_next')
        exit
      end if
    end do

  else if (sig == -1) then
    k = k-1
    do while(indx_gp(indx(iphi,ith,k,ipp)).eqv. .false.)
      k = k-1
      if (k<1 .or. k>npsi_tot) then
        call gkw_abort('k<1 or > npsi_tot in linear_terms_psi_next')
        exit
      end if
    end do
  end if

  linear_terms_psi_next = k
end function linear_terms_psi_next

! 4th order central differences gradient
subroutine differential_scheme_cd_grad(s,w)

  real, intent(in) :: s(4)
  real, intent(out) :: w(5)

  integer :: i

  real :: dum1, dum2, dum

  dum1=(s(1)-s(2))*(s(2)-s(3))*(s(3)-s(4))
  dum2=s(1)*s(2)*s(3)*s(4)
  dum = dum1/dum2*(s(1)-s(3))*(s(2)-s(4))*(s(1)-s(4))

  w(1) =   1/s(1)**2 * (s(2)-s(3))*(s(3)-s(4))*(s(2)-s(4))
  w(2) = - 1/s(2)**2 * (s(3)-s(4))*(s(1)-s(3))*(s(1)-s(4))
  w(3) = - dum1/dum2**2 * &
     & (((s(2)+s(3))*s(1)**2-(s(1)+s(2))*s(3)**2)*(s(2)-s(4))*s(4)**2 - &
     &  ((s(3)+s(4))*s(2)**2-(s(2)+s(3))*s(4)**2)*(s(1)-s(3))*s(1)**2)
  w(4) =   1/s(3)**2 * (s(1)-s(2))*(s(1)-s(4))*(s(2)-s(4))
  w(5) = - 1/s(4)**2 * (s(1)-s(2))*(s(2)-s(3))*(s(1)-s(3))
  do i =1,5
    w(i) = - w(i)/dum
  end do

end subroutine differential_scheme_cd_grad

! 2nd order central differences gradient
subroutine differential_scheme_cd2_grad(s,w)

  real, intent(in) :: s(4)
  real, intent(out) :: w(5)

  real :: dum
  dum = 1/(s(2)*s(3)*(s(3)-s(2)))

  w(2) = s(3)**2 *dum
  w(3) = -(s(3)**2-s(2)**2) *dum
  w(4) = -s(2)**2 *dum

  w(1) = 0.
  w(5) = 0.

end subroutine differential_scheme_cd2_grad

! 3rd order backwinded scheme for the adjacent cell at the left boundary,
! Including the boundary cell at the left
subroutine differential_scheme_bw3_grad_ac_l(s,w)
  real, intent(in) :: s(4)
  real, intent(out) :: w(5)

  real :: dum

  ! w(5) = 0.

  ! dum = (1/s(1)-1/s(2))*(s(2)-s(3)) -(1/s(2)-1/s(3))*(s(1)-s(2))

  ! w(1) =   1/s(1)**2 *(s(2)-s(3)) / dum
  ! w(2) =- ((1/s(1)**2-1/s(2)**2)*(s(2)-s(3)) - &
  !    &     (1/s(2)**2-1/s(3)**2)*(s(1)-s(2))) / dum
  ! w(3) = - 1/s(2)**2 *(s(1)-s(3)) / dum
  ! w(4) =   1/s(3)**2 *(s(1)-s(2)) / dum

  w(5) = 0.
  dum = (s(1)-s(2))*(s(1)-s(3))*(s(2)-s(3))/(s(1)*s(2)*s(3))

  w(1) =   1/s(1)**2 *(s(2)-s(3)) / dum
  w(2) = -((s(1)-s(2))*(s(2)-s(3)))/(s(1)**2 * s(2)**2 * s(3)**2) * &
     & (- s(3)**2 *(s(1)+s(2)) + s(1)**2 *(s(2)+s(3))) / dum
  w(3) = - 1/s(2)**2 *(s(1)-s(3)) / dum
  w(4) =   1/s(3)**2 *(s(1)-s(2)) / dum


end subroutine differential_scheme_bw3_grad_ac_l

! 3rd order backwinded scheme for the adjacent cell at the right boundary,
! Including the boundary cell at the right
subroutine differential_scheme_bw3_grad_ac_r(s,w)

  real, intent(in) :: s(4)
  real, intent(out) :: w(5)

  real :: dum

  w(1) = 0.

  dum = (1/s(2)-1/s(3))*(s(3)-s(4)) - (1/s(3)-1/s(4))*(s(2)*s(3))

  ! w(2) =   1/s(2)**2 *(s(3)-s(4)) / dum
  ! w(3) = - 1/s(3)**2 *(s(2)-s(4)) / dum
  ! w(4) = -((1/s(2)**2-1/s(3)**2)*(s(3)-s(4))- &
  !    &     (1/s(3)**2-1/s(4)**2)*(s(2)-s(3))) / dum
  ! w(5) =   1/s(4)**2 *(s(2)-s(3)) / dum

  ! dum = (1/s(4)-1/s(3))*(s(3)-s(2))-(1/s(3)-1/s(2))*(s(4)-s(3))

  ! w(1) = 0.
  ! dum = (s(2)-s(3))*(s(2)-s(4))*(s(3)-s(4)) / (s(2)*s(3)*s(4))
  
  dum = (s(4)-s(3))*(s(4)-s(2))*(s(3)-s(2)) / (s(4)*s(3)*s(2))
  w(5) =   1/s(4)**2 *(s(3)-s(2)) / dum
  w(4) = - (s(4)-s(3))*(s(3)-s(2)) / (s(4)**2 * s(3)**2 * s(2)**2) *  &
     & (-s(2)**2 *(s(4)+s(3)) + s(4)**2 *(s(3)+s(2))) / dum
  w(3) = - 1/s(3)**2 *(s(4)-s(2)) / dum
  w(2) =   1/s(2)**2 *(s(4)-s(3)) / dum

  ! w(2) =   1/s(2)**2 *(s(4)-s(3)) / dum
  ! w(3) = - 1/s(3)**2 *(s(4)-s(2)) / dum
  ! w(5) =   1/s(4)**2 *(s(3)-s(2)) / dum

end subroutine differential_scheme_bw3_grad_ac_r

! 2nd order backwinded scheme for the boundary cell at the left boundary
subroutine differential_scheme_bw2_grad_gc_l(s,w)

  real, intent(in) :: s(4)
  real, intent(out) :: w(5)

  real :: dum

  w(4) = 0.
  w(5) = 0.

  dum = 1/(s(2)*s(1)*(s(1)-s(2)))

  w(1) = -(s(1)**2-s(2)**2) * dum
  w(2) = - s(2)**2 * dum
  w(3) =   s(1)**2 * dum

end subroutine differential_scheme_bw2_grad_gc_l

! 2nd order backwinded scheme for the boundary cell at the right boundary
subroutine differential_scheme_bw2_grad_gc_r(s,w)

  real, intent(in) :: s(4)
  real, intent(out) :: w(5)

  real :: dum

  w(1) = 0.
  w(2) = 0.

  dum = 1/(s(3)*s(4)*(s(4)-s(3)))

  w(3) =  s(4)**2 *dum
  w(4) = -s(3)**2 *dum
  w(5) = -(s(4)**2-s(3)**2) *dum

end subroutine differential_scheme_bw2_grad_gc_r

! 4th order central differences hyper dissipation
subroutine differential_scheme_cd4_diss(s,w)

  real, intent(in) :: s(4)
  real, intent(out) :: w(5)

  integer :: i

  real :: dum, dum_norm

  dum = 1*2*3*4
  dum_norm = (s(1)*s(2)*s(3)*s(4)) / ((abs(s(2))+abs(s(3)))/2)/dum

  w(1) =   1/s(1) / ((s(1)-s(2))*(s(1)-s(3))*(s(1)-s(4)))
  w(2) = - 1/s(2) / ((s(1)-s(2))*(s(2)-s(3))*(s(2)-s(4)))
  w(3) =   1/(s(1)*s(2)*s(3)*s(4))
  w(4) =   1/s(3) / ((s(1)-s(3))*(s(2)-s(3))*(s(3)-s(4)))
  w(5) = - 1/s(4) / ((s(1)-s(4))*(s(2)-s(4))*(s(3)-s(4)))

  do i =1,5
    w(i) = w(i)*dum*dum_norm
  end do

end subroutine differential_scheme_cd4_diss

subroutine differential_scheme_cd2_diss(s,w)

  real, intent(in) :: s(4)
  real, intent(out) :: w(5)

  integer :: i

  real :: dum, dum_norm

  dum = 2./(s(2)*s(3)*(s(2)-s(3)))
  dum_norm = ((abs(s(2))+abs(s(3)))/2)

  w(2) = s(3)
  w(3) = (s(2)-s(3))
  w(4) = - s(3)

  w(1) = 0.
  w(5) = 0.

  do i =1,5
    w(i) = w(i)*dum*dum_norm
  end do


end subroutine differential_scheme_cd2_diss

end module linear_terms
