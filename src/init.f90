module init

  implicit none

  private

  public :: initialize, read_params

contains

subroutine initialize()

  use control,         only : control_init
  use geom,            only : geom_init
  use dist,            only : dist_init
  use matdat,          only : matdat_allocate
  use linear_terms,    only : calc_linear_terms
  use exp_integration, only : init_explicit
  use interpolation,   only : interpolation_init
  use index_function,  only : indx_init_2
  use diagnostic,      only : diagnostic_init

  call read_params
  call check_diagnostic_folder

  call control_init

  ! Set up the geometry
  call geom_init

  call indx_init_2

  ! Set upt the initialized distribution function on the grid
  call dist_init

  ! Set up the matrix
  call matdat_allocate

  ! Calculate the linear term
  call calc_linear_terms

  ! initialise the explict time integration
  call init_explicit

  ! initialise the matrices for the spline interpolaiton
  call interpolation_init

  call diagnostic_init

  ! Set up the spline interpolation
  ! call interpolation_init

end subroutine initialize

subroutine read_params

  use control,     only : control_read_nml
  use geom,        only : geom_read_nml
  use dist,        only : dist_read_nml

  integer, parameter :: file_unit = 92

  open (file_unit,file='input.dat',FORM='formatted',STATUS='old', &
     POSITION='rewind',ACTION='read')
  call control_read_nml(file_unit)
  close(file_unit)

  open (file_unit,file='input.dat',FORM='formatted',STATUS='old', &
     POSITION='rewind',ACTION='read')
  call geom_read_nml(file_unit)
  close(file_unit)

  open (file_unit,file='input.dat',FORM='formatted',STATUS='old', &
     POSITION='rewind',ACTION='read')
  call dist_read_nml(file_unit)
  close(file_unit)

end subroutine read_params

subroutine check_diagnostic_folder


end subroutine check_diagnostic_folder

end module init
