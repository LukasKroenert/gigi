module interpolation

  implicit none

  private

  public :: interpolation_init, interpolation_psi_coeff, interpolation_th_coeff,&
     & interpolation_psi_dist, interpolation_th_dist

  !#########
  !### Variables for interpolation in radial direction
  !#########

  ! Tridiagonal matrix for initialization of the splines
  real, allocatable, save :: mtri_psi(:,:)

  ! lu-matrix of matri_psi
  real, allocatable, save :: mlu_psi(:,:,:)

  !lapack routines specific variables
  integer, allocatable, save :: ipiv_psi(:,:)
  integer, save :: info01_psi
  integer, save :: info02_psi

  !Coefficients for the cubic splines
  real, allocatable, save :: coeff_a_psi(:,:)
  real, allocatable, save :: coeff_b_psi(:,:)
  real, allocatable, save :: coeff_c_psi(:,:)
  real, allocatable, save :: coeff_d_psi(:,:)

  !#########
  !### Variables for interpolation in poloidal direction
  !#########

  ! Tridiagonal matrix for initialization of the splines
  real, allocatable, save :: mtri_th(:,:)

  ! lu-matrix of matri_th
  real, allocatable, save :: mlu_th(:,:,:)

  !lapack routines specific variables
  integer, allocatable, save :: ipiv_th(:,:)
  integer, save :: info01_th
  integer, save :: info02_th

  !Coefficients for the cubic splines
  real, allocatable, save :: coeff_a_th(:,:)
  real, allocatable, save :: coeff_b_th(:,:)
  real, allocatable, save :: coeff_c_th(:,:)
  real, allocatable, save :: coeff_d_th(:,:)

  real, allocatable, save :: dsth_interpolation(:,:)
  real, allocatable, save :: dspsi_interpolation(:,:)

  !> Path length in radial direction from each ghost point to the next 'real'
  !> grid point. (:,:) = (ipsi,ith). with ith = 1:#angles with gp,
  !> ipsi = 1:#gp
  real, public, allocatable, save :: dspsi_false(:,:)

contains

subroutine interpolation_init()

  call interpolation_psi_init()
  call interpolation_th_init()

end subroutine interpolation_init

subroutine interpolation_psi_init()

  use geom,           only : nth, npsi_tot, dspsi_true
  use index_function, only : indx_gp_th,indx_ntrue_psi, indx_false_th, indx_gp,&
     & indx_true_ith

  integer :: ith, ipsi1, ipsi2, ipsi,k, ipsi_t1, ipsi_t2

  ! Temporarily loop index
  integer :: indx_tmp, npsi_true,ith_tmp
  ! Temporary array length
  integer :: ll

  integer :: allocate_status
  allocate_status = 0

  ! Note (npsi_tot-2)*(npsi_tot-2) is the maximum of needed points
  ! If ghost point exists at a specific point the amount of initialized points
  ! can is smaller
  allocate(mtri_psi((npsi_tot-2)*(npsi_tot-2),indx_gp_th),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate mtri_psi'

  allocate(mlu_psi((npsi_tot-2),(npsi_tot-2),indx_gp_th),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate mlu_psi'

  allocate(ipiv_psi((npsi_tot-2),indx_gp_th),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate ipiv_psi'

  !Allocate the coefficients for the cubic splines
  allocate(coeff_a_psi((npsi_tot-1),indx_gp_th),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate coeff_a_psi'
  allocate(coeff_b_psi((npsi_tot-1),indx_gp_th),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate coeff_b_psi'
  allocate(coeff_c_psi(npsi_tot,indx_gp_th),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate coeff_c_psi'
  allocate(coeff_d_psi((npsi_tot-1),indx_gp_th),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate coeff_d_psi'

  allocate(dspsi_interpolation(npsi_tot,indx_gp_th),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate dspsi_interpolation'

  do ith = 1, indx_gp_th
    ith_tmp = indx_false_th(ith)
    do ipsi = 1, indx_ntrue_psi(ith_tmp)
      dspsi_interpolation(ipsi,ith) = dspsi_true(indx_true_ith(ipsi,ith))
    end do
  end do

  call interpolation_dspsi_false_calc()

  do ith = 1, indx_gp_th
    ll = indx_ntrue_psi(indx_false_th(ith))
    call interpolation_matrix_nat(&
       & ll,dspsi_interpolation(1:ll,ith),mlu_psi(1:ll-2,1:ll-2,ith))
  end do

  !####################
  !### Lapack LU factorization
  !####################

  do ith = 1, indx_gp_th
    ll = indx_ntrue_psi(indx_false_th(ith))
    ! write(*,*) 'mlu_psi before lu. ith =',ith
    ! do indx_tmp = 1, ll-2
    !   write(*,*) mlu_psi(indx_tmp,1:ll-2,ith)
    ! end do

    call DGETRF(ll-2,ll-2,mlu_psi(1:ll-2,1:ll-2,ith),ll-2,&
       & ipiv_psi(1:ll-2,ith), info01_psi)

    ! write(*,*)
    ! write(*,*) 'mlu_psi after lu. ith =',ith
    ! do indx_tmp = 1, ll-2
    !  write(*,*) mlu_psi(indx_tmp,1:ll-2,ith)
    ! end do
    ! stop 'in interpolation_psi_init'
  end do

end subroutine interpolation_psi_init

subroutine interpolation_psi_coeff(fdis)

   use dist,           only : ntot_geom
   use geom,           only : nth,npsi_tot,gth
   use index_function, only : indx_gp_th, indx_ntrue_psi, indx_true_ith, &
      & indx_false_th

  complex, intent(in) :: fdis(ntot_geom)

  integer :: ith, ipsi, indx_tmp
  integer :: ll ! Temporary array length
  integer :: ith_tmp ! Temporary poloidal indices of the real grid

  real :: fdis_tmp(npsi_tot,indx_gp_th)

  do ith = 1, indx_gp_th
    do ipsi = 1, indx_ntrue_psi(indx_false_th(ith))
      indx_tmp = indx_true_ith(ipsi,ith)
      fdis_tmp(ipsi,ith)=real(fdis(indx_tmp))
      !write(*,*) ith, ipsi, indx_tmp

      ! if( ith ==25) then
      !   write(*,*) ith, ipsi, fdis_tmp(ipsi,ith)
      ! end if

    end do
  end do
  ! stop

  ! write(*,*) fdis(1), gth(1)
  ! write(*,*) fdis(nth), gth(nth)
  ! write(*,*) fdis(nth+1), gth(nth+1)
  do ith = 1, indx_gp_th
    ith_tmp = indx_false_th(ith)
    ll = indx_ntrue_psi(ith_tmp)

    ! if (ith == 25) then
    !   ! write(*,*)
    !   ! write(*,*) 'ith = ', ith

    !   do ipsi = 1, ll
    !     ! write(*,*) ipsi, fdis_tmp(ipsi,ith)
    !   end do
    ! end if

    ! write(*,*)
    !write(*,*) 'ith = ', ith
    !write(*,*) fdis_tmp(1:ll,ith)
    ! if (ith == 25) then
    !   write(*,*) fdis_tmp(1:ll,ith)
    ! end if

    call interpolation_coeff_nat_calc(ll,fdis_tmp(1:ll,ith), &
       & mlu_psi(1:ll-2,1:ll-2,ith),ipiv_psi(1:ll-2,ith),info01_psi,&
       & dspsi_interpolation(1:ll,ith),&
       & coeff_a_psi(1:ll,ith),coeff_b_psi(1:ll,ith),&
       & coeff_c_psi(1:ll,ith),coeff_d_psi(1:ll,ith))
  end do
   ! stop 'at interpolation_psi_coefficients'

end subroutine interpolation_psi_coeff

subroutine interpolation_psi_dist(fdis)

  use dist,           only : ntot_geom
  use index_function, only : indx_gp_th, indx_false_th, indx_nfalse_psi, &
     & indx_false_ith, indx_false_psi, indx
  use constants,      only : c1

  complex, intent(inout) :: fdis(ntot_geom)

  integer :: ith, ith_tmp,ipsi,indx_tmp

  !indx_gp_th = amount of angles with gp
  do ith = 1, indx_gp_th
    ! indx_false_th. Get real th index of th index of the gp
    ith_tmp = indx_false_th(ith)
    ! indx_false_th(ith) amount of gp at constant th for all th
    do ipsi = 1, indx_nfalse_psi(ith_tmp)
      indx_tmp=indx_false_ith(ipsi,ith)
      fdis(indx_tmp) = c1*(&
         & coeff_a_psi(indx_false_psi(ipsi,ith),ith) + &
         & coeff_b_psi(indx_false_psi(ipsi,ith),ith)*dspsi_false(ith,ipsi) + &
         & coeff_c_psi(indx_false_psi(ipsi,ith),ith)*dspsi_false(ith,ipsi)**2 + &
         & coeff_d_psi(indx_false_psi(ipsi,ith),ith)*dspsi_false(ith,ipsi)**3)
    end do
  end do

  ! stop

end subroutine interpolation_psi_dist

subroutine interpolation_th_init()

  use geom,           only : nth, npsi_tot, dsth
  use index_function, only : indx

  ! Variables for loops
  integer :: ipsi, ith1,ith2, iphi, ipp, ith

  ! Temporarily loop index
  integer :: indx_tmp

  integer :: allocate_status
  allocate_status = 0

  !Allocate the cofficients for the cubic circulant splines
  allocate(mtri_th(nth*nth,npsi_tot),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate mtri_th'

  allocate(mlu_th(nth,nth,npsi_tot),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate mlu_th'

  allocate(ipiv_th(nth,npsi_tot),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate ipiv_th'

  !Allocate the coefficients for the cubic splines
  allocate(coeff_a_th(nth,npsi_tot),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate coeff_a_th'
  allocate(coeff_b_th(nth,npsi_tot),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate coeff_b_th'
  allocate(coeff_c_th(nth,npsi_tot),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate coeff_c_th'
  allocate(coeff_d_th(nth,npsi_tot),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate coeff_d_th'

  allocate(dsth_interpolation(nth,npsi_tot),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate dsth_interpolation'

  !Sort dsth into dsth_interpolation
  iphi = 1
  ipp = 0
  do ith = 1, nth
    do ipsi = 1, npsi_tot
      dsth_interpolation(ith,ipsi)=dsth(indx(iphi,ith,ipsi,ipp))
    end do
  end do

  do ipsi = 1, npsi_tot
    call interpolation_matrix_circ(nth,dsth_interpolation(1:nth,ipsi),&
       & mlu_th(1:nth,1:nth,ipsi))
  end do

  ! ! Initialize the circulant tridiagonal matrix
  ! do ipsi = 1,npsi_tot
  !   do ith1 = 1,nth
  !     do ith2 = 1,nth
  !       indx_tmp = (ith1-1)*nth+ith2
  !       if(ith2 ==  1 .and. ith1 == 1) then
  !         mtri_th(indx_tmp,ipsi) = &
  !            & 2 * (dsth((ipsi-1)*nth+nth) + dsth((ipsi-1)*nth+ith2))
  !       else if (ith2 /=1 .and. ith2 ==ith1) then
  !         mtri_th(indx_tmp,ipsi) = &
  !            & 2 *(dsth((ipsi-1)*nth+ith2-1)+dsth((ipsi-1)*nth+ith2))
  !       else if (ith2 == ith1+1) then
  !         mtri_th(indx_tmp,ipsi) = dsth((ipsi-1)*nth+ith2)
  !       else if (ith2 == ith1-1) then
  !         mtri_th(indx_tmp,ipsi) = dsth((ipsi-1)*nth+ith1)
  !       else if (ith2 == 1 .and. ith1 == nth) then
  !         mtri_th(indx_tmp,ipsi) = dsth((ipsi-1)*nth+ith1)
  !       else if (ith2 == nth .and. ith1 == 1) then
  !         mtri_th(indx_tmp,ipsi) = dsth((ipsi-1)*nth+ith2)
  !       else
  !         mtri_th(indx_tmp,ipsi) = 0
  !       end if
  !     end do
  !   end do
  ! end do

  !####################
  !### Lapack LU factorization
  !####################
  
  ! do ipsi = 1,npsi_tot
  !   do ith1 = 1,nth
  !     do ith2 = 1,nth
  !       indx_tmp = (ith1-1)*nth+ith2
  !       mlu_th(ith2,ith1,ipsi)=mtri_th(indx_tmp,ipsi)
  !     end do
  !   end do
  ! end do

  do ipsi = 1,npsi_tot

    ! write(*,*) 'mlu_th before lu, ipsi = ', ipsi,'nth =', nth
    ! open (unit=20,file='mlu_th_before.dat',status='replace', &
    ! & position = 'append')
    ! do ith=1, nth
    !   write(20,*) mlu_th(ith,1:nth,ipsi)
    ! end do
    ! close(unit = 20)
    ! stop

    call DGETRF(nth, nth, mlu_th(:,:,ipsi), nth, ipiv_th(:,ipsi), info01_th)

    ! write(*,*)
    ! write(*,*) 'mlu_th_after lu, ipsi = ', ipsi, 'nth =', nth
    ! open (unit=20,file='mlu_th_after.dat',status='replace', position = 'append')
    ! do ith=1, nth
    !   write(20,*) mlu_th(ith,1:nth,ipsi)
    ! end do
    ! close(unit = 20)
    ! stop
  end do

end subroutine interpolation_th_init

subroutine interpolation_th_coeff(fdis)

   use dist, only : ntot_geom
   use geom, only : nth, npsi_tot, dsth
   complex, intent(in) :: fdis(ntot_geom)

  ! integer :: ipsi, ith,indx_tmp
  ! real :: rhs_tmp(nth)

  real :: fdis_tmp(nth,npsi_tot)
  integer :: i,ith,ipsi

  do ipsi = 1, npsi_tot
    do ith = 1, nth
      fdis_tmp(ith,ipsi)=real(fdis((ipsi-1)*nth+ith))
    end do
  end do

  do ipsi = 1, npsi_tot
    call interpolation_coeff_circ_calc(nth,fdis_tmp(:,ipsi),mlu_th(:,:,ipsi), &
       & ipiv_th(:,ipsi),info02_th,dsth_interpolation(:,ipsi),&
       & coeff_a_th(:,ipsi),coeff_b_th(:,ipsi),&
       & coeff_c_th(:,ipsi),coeff_d_th(:,ipsi))
    ! write(*,*) dsth_interpolation(1,ipsi), dsth_interpolation(2,ipsi), &
    !    & dsth_interpolation(nth,ipsi)
  end do
  
end subroutine interpolation_th_coeff

subroutine interpolation_th_dist(fdis)

  use dist,           only : ntot_geom
  use geom,           only : npsi_tot, nth, dsthpp, geom_nearest
  use general,        only : gkw_abort
  use index_function, only : indx
  use constants,      only : c1

  complex, intent(inout) :: fdis(ntot_geom+ntot_geom*5)

  integer :: ipsi, ith, iphi, ipp, indx_tmp,k
  integer :: pp_val(4)

  iphi = 1

  ipsi = 1
  ipp=0

  ! do ith = 1, nth
  !   write(*,*) indx(iphi,ith,ipsi,ipp), real(fdis(indx(iphi,ith,ipsi,ipp)))
  ! end do

  do ipsi = 1, npsi_tot

    do ith = 1, nth
      do ipp = -2,2
        select case(ipp)
        case (-2)
          k = 4
        case(-1)
          k = 3
        case(0)
        case(1)
          k = 1
        case(2)
          k = 2
        case default
          call gkw_abort('interpolation_th_dist: no proper ipp')
        end select

        select case(ipp)
        case(-2,-1,1,2)
          indx_tmp = ((ipsi-1)*nth+ith-1)*4+k

          ! indx_tmp = (ipsi-1)*nth+geom_nearest(ith,ipsi,k)
          !write(*,*) ith, geom_nearest(ith,ipsi,k), indx_tmp
          ! write(*,*) iphi, ith, ipsi, ipp, ntot_geom
          ! write(*,*) indx(iphi,ith,ipsi,0), &
          !    & indx(iphi, ith, geom_nearest(ith,ipsi,1), 0)
          ! write(*,*)  geom_nearest(ith,ipsi,4), geom_nearest(ith,ipsi,3), &
          !    & geom_nearest(ith,ipsi,1), geom_nearest(ith,ipsi,2)
          ! stop

          fdis(indx(iphi,ith,ipsi,ipp))= c1*(&
             & coeff_a_th(geom_nearest(ith,ipsi,k),ipsi) + &
             & coeff_b_th(geom_nearest(ith,ipsi,k),ipsi) * dsthpp(indx_tmp) + &
             & coeff_c_th(geom_nearest(ith,ipsi,k),ipsi) * dsthpp(indx_tmp)**2 + &
             & coeff_d_th(geom_nearest(ith,ipsi,k),ipsi) * dsthpp(indx_tmp)**3)

          ! if (k==1) then

          !   if (ith == nth) then
          !     write(*,*) iphi, ipsi, ith, ipp, k, geom_nearest(ith,ipsi,k), &
          !        & indx_tmp,dsthpp(indx_tmp), &
          !        !& real(fdis(indx(iphi,ith,ipsi,0))), &
          !        & coeff_a_th(geom_nearest(ith,ipsi,k),ipsi), &
          !        & real(fdis(indx(iphi,ith,ipsi,ipp))), &
          !          & real(fdis(indx(iphi,1,ipsi,0)))

          !   else
          !       write(*,*) iphi, ipsi, ith, ipp, k, geom_nearest(ith,ipsi,k), &
          !          & indx_tmp,dsthpp(indx_tmp), &
          !          !& real(fdis(indx(iphi,ith,ipsi,0))), &
          !          & coeff_a_th(geom_nearest(ith,ipsi,k),ipsi), &
          !          & real(fdis(indx(iphi,ith,ipsi,ipp))), &
          !          & real(fdis(indx(iphi,ith+1,ipsi,0)))
          !       ! stop 'interpolation_th_dist'
          !     end if
          ! end if

          ! if (ipsi == 1) then

!           if (k==2) then
!             if(ipsi ==1) then
!               if (ith==nth) then
!                 write(*,*) iphi, ipsi, ith, ipp, k, geom_nearest(ith,ipsi,k), &
!                    & indx_tmp,&
!                                 & dsthpp(indx_tmp), &
!                                 ! & real(fdis(indx(iphi,ith+1,ipsi,0))), &
!                    & coeff_a_th(geom_nearest(ith,ipsi,k),ipsi), &
!                    & real(fdis(indx(iphi,ith,ipsi,ipp))), &
!                    & real(fdis(indx(iphi,2,ipsi,0))), indx(iphi,2,ipsi,0)
!                 ! stop 'interpolation_th_dist'


!               else if (ith == nth-1)then
!                 write(*,*) iphi, ipsi, ith, ipp, k, geom_nearest(ith,ipsi,k), &
!                    & indx_tmp,&
!                                 & dsthpp(indx_tmp), &
!                                 ! & real(fdis(indx(iphi,ith+1,ipsi,0))), &
!                    & coeff_a_th(geom_nearest(ith,ipsi,k),ipsi), &
!                    & real(fdis(indx(iphi,ith,ipsi,ipp))), &
!                    & real(fdis(indx(iphi,1,ipsi,0))), indx(iphi,1,ipsi,0)
!                 ! stop 'interpolation_th_dist'

!               else
!                 write(*,*) iphi, ipsi, ith, ipp, k, geom_nearest(ith,ipsi,k), &
!                    & indx_tmp,&
!                                 & dsthpp(indx_tmp), &
!                                 ! & real(fdis(indx(iphi,ith+1,ipsi,0))), &
!                    & coeff_a_th(geom_nearest(ith,ipsi,k),ipsi), &
!                    & real(fdis(indx(iphi,ith,ipsi,ipp))), &
!                    & real(fdis(indx(iphi,ith+2,ipsi,0))), indx(iphi,ith+2,ipsi,0)
!                 ! stop 'interpolation_th_dist'
!               end if

!            end if
!           end if

!           if (k==3) then
!             if (ipsi == 1) then
!             if (ith==1) then
!               write(*,*) iphi, ipsi, ith, ipp, k, geom_nearest(ith,ipsi,k), &
!                  & indx_tmp,&
!                  & dsthpp(indx_tmp), &
!                  !& real(fdis(indx(iphi,nth,ipsi,0))), &
!                  & coeff_a_th(geom_nearest(ith,ipsi,k),ipsi), &
!                  & real(fdis(indx(iphi,ith,ipsi,ipp))), &
!                  & real(fdis(indx(iphi,ith,ipsi,0)))
!               ! stop 'interpolation_th_dist'

!            else
!               write(*,*) iphi, ipsi, ith, ipp, k, geom_nearest(ith,ipsi,k), &
!                  & indx_tmp, &
!                   & dsthpp(indx_tmp), &
!                  !& real(fdis(indx(iphi,ith-1,ipsi,0))), &
!                  & coeff_a_th(geom_nearest(ith,ipsi,k),ipsi), &
!                  & real(fdis(indx(iphi,ith,ipsi,ipp))), &
!                  & real(fdis(indx(iphi,ith,ipsi,0)))
! !              stop 'interpolation_th_dist'

!            end if
!           end if
!         end if

!           if (k==4) then
!             if (ith == 1) then
!               write(*,*) iphi, ipsi, ith, ipp, k, geom_nearest(ith,ipsi,k), &
!                  & indx_tmp,dsthpp(indx_tmp), &
!                  & real(fdis(indx(iphi,geom_nearest(ith,ipsi,k),ipsi,0))), &
!                  & coeff_a_th(geom_nearest(ith,ipsi,k),ipsi), &
!                  & real(fdis(indx(iphi,ith,ipsi,ipp))), &
!                  & real(fdis(indx(iphi,nth,ipsi,0)))

!             else
!               write(*,*) iphi, ipsi, ith, ipp, k, geom_nearest(ith,ipsi,k), &
!                  & indx_tmp,dsthpp(indx_tmp), &
!                  & real(fdis(indx(iphi,geom_nearest(ith,ipsi,k),ipsi,0))), &
!                  & coeff_a_th(geom_nearest(ith,ipsi,k),ipsi), &
!                  & real(fdis(indx(iphi,ith,ipsi,ipp))), &
!                  & real(fdis(indx(iphi,ith-1,ipsi,0)))
!               ! stop 'interpolation_th_dist'
!             end if
!           end if

        case(0)
        case default
          call gkw_abort('interpolation_th_dist: no proper ipp')
        end select
      end do
      ! stop  'end of interpolation_th_dist'
    end do
    ! stop 'at end of interpolation_th_dist'
  end do
  ! stop

end subroutine interpolation_th_dist

subroutine interpolation_coeff_circ_calc(l,fdis,mlu,ipiv,info,ds,a,b,c,d)

  integer, intent(in) :: l
  real, intent(in) :: fdis(l)
  real, intent(in) :: ds(l)
  real, intent(in) :: mlu(l,l)
  integer, intent(in) :: ipiv(l)
  integer, intent(out) :: info
  real, intent(out) :: a(l),b(l),c(l),d(l)

  integer :: i

  do i = 1,l
    c(i) = fdis(i)
    ! write(*,*) c(i)
  end do

  ! write(*,*) 'c before ', c

  ! Calculate c
  call DGETRS('N',l,1,mlu,l,ipiv,c,l,info)

  ! write(*,*) 'c after', c
  ! write(*,*)

  !Calculate the coefficients a,b,d
  do i = 1,l
    a(i) = fdis(i)
    if (i==l) then
      b(i) = (fdis(1)-fdis(i))/ds(i) - (2*c(i)+c(1))*ds(i)/3
      d(i) = (c(1)-c(i))/(3*ds(i))
    else
      b(i) = (fdis(i+1)-fdis(i))/ds(i) - (2*c(i)+c(i+1))*ds(i)/3
      d(i) = (c(i+1)-c(i))/(3*ds(i))
    end if
  end do

  ! write(*,*) ds(2)

  ! do i = 1, l
  !   write(*,*) a(i), b(i), c(i), d(i), ds(i)
  ! end do

  ! write(*,*)

  ! do i = 1, l
  !   if (i==l) then
  !     write(*,*) i, a(1), a(i)+b(i)*ds(i)+c(i)*ds(i)**2+d(i)*ds(i)**3
  !   else
  !     write(*,*) i, a(i+1), a(i)+b(i)*ds(i)+c(i)*ds(i)**2+d(i)*ds(i)**3
  !   end if
  ! end do

  ! stop 'at interpolation_coeff_circ_calc'

end subroutine interpolation_coeff_circ_calc

subroutine interpolation_coeff_nat_calc(l,fdis,mlu,ipiv,info,ds,a,b,c,d)

  integer, intent(in) :: l
  real, intent(in) :: fdis(l)
  real, intent(in) :: ds(l)
  real, intent(in) :: mlu(l-2,l-2)
  integer, intent(in) :: ipiv(l)
  integer, intent(out) :: info
  real, intent(out) :: a(l-1),b(l-1),c(l),d(l-1)

  integer :: i

  !Determine a
  do i = 1,l-1
    a(i) = fdis(i)
!    write(*,*) i, a(i)
  end do

  !Calculate c
  do i = 1, l
    c(i) = fdis(i)
  end do

  ! do i = 1, l
  !   write(*,*) mlu(l,:)
  ! end do
  !write(*,*)
  ! write(*,*) 'c before', c
  ! write(*,*) l
  ! write(*,*) mlu
  ! write(*,*) ipiv
  call DGETRS('N',l-2,1,mlu,l-2,ipiv,c(2:l-1),l-2,info)
  ! write(*,*) 'after'
  ! stop
  
  !write(*,*) 'c after', c

  ! Natural boundary condition requires:
  c(1) = 0.
  c(l) = 0.

  !Calculate b,d
  do i = 1,l-1
    b(i) = (fdis(i+1)-fdis(i))/ds(i) - (2*c(i)+c(i+1))*ds(i)/3
    d(i) = (c(i+1)-c(i)) / (3*ds(i))
  end do

  ! Test coefficients
  ! do i = 1, l-1
  !   write(*,*) i, a(i), a(i)+b(i)*ds(i)+c(i)*ds(i)**2+d(i)*ds(i)**3, fdis(i+1)
  ! end do

end subroutine interpolation_coeff_nat_calc

subroutine interpolation_dspsi_false_calc()

  use index_function,  only : indx, indx_gp, indx_gp_th, indx_false_th
  use geom, only : nth, npsi_tot, gpsi

  integer :: ipsi_false, ipsi_true, iphi, ith,ipsi, ipp, ith_tmp

  integer :: allocate_status
  allocate_status = 0

  allocate(dspsi_false(indx_gp_th,npsi_tot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate dspsi_true'

  iphi = 1
  ipp = 0

  do ith = 1, indx_gp_th
    ith_tmp = indx_false_th(ith)
    ipsi_false = 0
    do ipsi = 1, npsi_tot
      if (indx_gp(indx(iphi,ith_tmp,ipsi,ipp)) .eqv. .true.) then
        ipsi_true = ipsi
      else if (indx_gp(indx(iphi,ith_tmp,ipsi,ipp)) .eqv. .false.) then
        ipsi_false = ipsi_false+1
        dspsi_false(ith,ipsi_false) = gpsi(indx(iphi,ith_tmp,ipsi,ipp)) &
           & - gpsi(indx(iphi,ith_tmp,ipsi_true,ipp))
      end if
    end do
  end do

end subroutine interpolation_dspsi_false_calc


subroutine interpolation_matrix_nat(ll,ds,mat)

  integer, intent(in) :: ll
  real, intent(in) :: ds(ll)
  real, intent(out) :: mat(ll-2,ll-2)

  !Row of the matrix
  integer :: i
  !Column of the matrix
  integer :: j

  do i = 2, ll-1
    do j = 2, ll-1
      if(i==j) then
        mat(i-1,j-1) = 2*(ds(i-1)+ds(i))
      else if(j==i+1) then
        mat(i-1,j-1) = ds(i)
      else if(j==i-1) then
        mat(i-1,j-1) = ds(j)
      else
        mat(i-1,j-1)=0
      end if
    end do
  end do

  ! do i = 1, ll-2
  !   write(*,*) mat(i,:)
  ! end do
  ! stop

end subroutine interpolation_matrix_nat

subroutine interpolation_matrix_circ(ll,ds,mat)

  integer, intent(in) :: ll
  real, intent(in)    :: ds(ll)
  real, intent(out)   :: mat(ll,ll)

  !Row of the matrix
  integer :: i
  !Column of the matrix
  integer :: j

  do i = 1, ll
    do j = 1, ll
      if (i==j .and. i==1) then
        mat(i,j) = 2*(ds(ll) + ds(i))
      else if(i==j .and. i/=1) then
        mat(i,j) = 2*(ds(i-1)+ds(i))
      else if(j==i+1) then
        mat(i,j) = ds(i)
      else if(j==i-1) then
        mat(i,j) = ds(j)
      else if (i==1 .and. j==ll) then
        mat(i,j) = ds(ll)
      else if (i==ll .and. j==1) then
        mat(i,j) = ds(ll)
      else
        mat(i,j) = 0
      end if
    end do
  end do

  !do i = 1, ll
!    write(*,*) mat(i,:)
  !end do
  !stop

end subroutine interpolation_matrix_circ

end module interpolation
