module index_function

  implicit none

  private

  public :: indx, indx_add_gp, indx_init, indx_init_2

  !> if .false. then grid point is a ghost point.
  !> else, then grid point is a regular grid point, either on the cfs, or pfs
  logical, save, allocatable, public :: indx_gp(:)

  ! Radial indices of the pfs with length npsi_pfs
  integer, save, allocatable, public :: indx_psi_pfs(:)

  !> Amount of true grid points without
  integer, save,  public :: indx_ntrue

  ! Amount of true points for each th
  integer, save, allocatable, public :: indx_ntrue_psi(:)

  ! Amount of false points for each th for all th
  integer, save, allocatable, public :: indx_nfalse_psi(:)

  !> Amount of angles th with ghost points
  integer, save, public :: indx_gp_th

  !> indices of angles theta with ghost points
  integer, save, allocatable, public :: indx_false_th(:)

  !> indices of true points at angles th with gp in a 2d array
  !> (indx_psi1, ith_1), (indx_psi2, ith_1), ...
  !> Note: Variable amount of points of each ith determined by indx_ntrue_psi
  !> Amount of indx_gp_th determined by indx_gp_th
  !> Indizess ith determined by indx_false_th
  integer, save, allocatable, public :: indx_true_ith(:,:)

  !> indices of false points at angles th with gp in a 2d array
  integer, save, allocatable, public :: indx_false_ith(:,:)

  !> index psi of a true point corresponding the the indx (ipsi,ith)
  !> of a gp with [ipsi,ith] =
  !> [1:#amount of gp at angle th, 1:#ith angle on whcih a gp exists]
  integer, save, allocatable, public :: indx_false_psi(:,:)

contains

subroutine indx_init(gtot,npsi_pfs)

  integer, intent(in) :: gtot
  integer, intent(in), optional :: npsi_pfs

  integer :: allocate_status = 0

  allocate(indx_gp(gtot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate indx_gp'

  if (present(npsi_pfs)) then
    allocate(indx_psi_pfs(npsi_pfs),stat=allocate_status)
    if(allocate_status /=0) write (*,*) 'Could not allocate indx_psi_pfs'
  endif

end subroutine indx_init

subroutine indx_init_2

  call indx_ntrue_calc

  call indx_ntrue_psi_calc
  call indx_nfalse_psi_calc

  ! Count the amount of angles th with ghost points
  call indx_gp_th_calc
  ! Determine the indices of angles theta with ghost points
  call indx_gp_th_det

  ! Write the indices of true points at angles th with gp in a 2d array
  call indx_true_ith_calc
  ! Write the indices of false points at angles th with gp in a 2d array
  call indx_false_ith_calc

end subroutine indx_init_2

function indx(I1,I2,I3,I4)

  use global, only : nth_global, npsi_tot_global

  !I1 = iphi, I2 = ith, I3 = ipsi, I4 = ipp
  integer, intent(in) :: I1, I2, I3, I4
  integer             :: iphi, ith, ipsi, ipp

  integer             :: dum
  integer             :: indx


  iphi = I1
  ith  = I2
  ipsi = I3
  ipp  = I4

  dum = (ipsi-1)*nth_global+ith

  select case (ipp)
  case(-2)
    indx = nth_global*npsi_tot_global+(dum-1)*4+4
  case(-1)
    indx = nth_global*npsi_tot_global+(dum-1)*4+3
  case(0)
    indx = dum
  case(1)
    indx = nth_global*npsi_tot_global+(dum-1)*4+1
  case(2)
    indx = nth_global*npsi_tot_global+(dum-1)*4+2
  end select

end function indx

subroutine indx_add_gp(grid_indx,gp_logic)

  integer, intent(in) :: grid_indx
  logical, intent(in) :: gp_logic

  indx_gp(grid_indx) = gp_logic

end subroutine indx_add_gp

!> Calculate the amount of true grid points
subroutine indx_ntrue_calc

  use global, only : nth_global, npsi_tot_global

  integer :: i

  indx_ntrue = 0
  do i = 1, nth_global*npsi_tot_global
    if(indx_gp(i)) then
      indx_ntrue = indx_ntrue+1
    end if
  end do

end subroutine indx_ntrue_calc

!> Calculate the amount of true points in radial direction for each th
subroutine indx_ntrue_psi_calc

  use global, only : nth_global, npsi_tot_global

  integer :: ith,ipsi

  integer :: allocate_status = 0

  allocate(indx_ntrue_psi(nth_global),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate indx_ntrue_psi'
  
  do ith = 1, nth_global
    indx_ntrue_psi(ith) = 0
    do ipsi = 1, npsi_tot_global
      if(indx_gp((ipsi-1)*nth_global+ith)) then
        indx_ntrue_psi(ith) = indx_ntrue_psi(ith)+1
      end if
    end do
  end do

end subroutine indx_ntrue_psi_calc

subroutine indx_nfalse_psi_calc

  use global, only : nth_global, npsi_tot_global

  integer :: ith,ipsi

  integer :: allocate_status = 0

  allocate(indx_nfalse_psi(nth_global),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate indx_nfalse_psi'
  
  do ith = 1, nth_global
    indx_nfalse_psi(ith) = 0
    do ipsi = 1, npsi_tot_global
      if(indx_gp((ipsi-1)*nth_global+ith) .eqv. .false.) then
        indx_nfalse_psi(ith) = indx_nfalse_psi(ith)+1
      end if
    end do
  end do

end subroutine indx_nfalse_psi_calc


!> Count the amount of angles th with ghost points
subroutine indx_gp_th_calc

  use global, only : nth_global, npsi_tot_global

  integer :: ipsi,ith

  indx_gp_th = 0
  do ith = 1, nth_global
    do ipsi = 1, npsi_tot_global
      if(indx_gp((ipsi-1)*nth_global+ith) .eqv. .false.) then
        indx_gp_th = indx_gp_th + 1
        exit
      end if
    end do
  end do

end subroutine indx_gp_th_calc

!> Determine the indices of angles theta with ghost points
subroutine indx_gp_th_det

  use global, only : nth_global, npsi_tot_global

  integer :: k,ipsi,ith

  integer :: allocate_status = 0

  allocate(indx_false_th(indx_gp_th),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate indx_false_th'
  k = 0

  do ith = 1, nth_global
    do ipsi = 1, npsi_tot_global
      if(indx_gp((ipsi-1)*nth_global+ith) .eqv. .false.) then
        k = k+1
        indx_false_th(k)=ith
        exit
      end if
    end do
  end do

end subroutine indx_gp_th_det

!> Write the indices of true points at angles th with gp in a 2d array
subroutine indx_true_ith_calc

  use global, only : npsi_tot_global, nth_global

  integer :: ith, ipsi, ipsi_true

  integer :: allocate_status = 0

  allocate(indx_true_ith(npsi_tot_global,indx_gp_th),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate indx_true_ith'

  do ith = 1, indx_gp_th
    ipsi_true=0
    do ipsi = 1, npsi_tot_global
      if (indx_gp((ipsi-1)*nth_global+indx_false_th(ith))) then
        ipsi_true = ipsi_true+1
        indx_true_ith(ipsi_true,ith)=(ipsi-1)*nth_global+indx_false_th(ith)
      end if
    end do
  end do

end subroutine indx_true_ith_calc

!> Write the indices of false points at angles th with gp in a 2d array
subroutine indx_false_ith_calc

  use global, only : npsi_tot_global, nth_global

  integer :: ith, ipsi, ipsi_false, ipsi_true

  integer :: allocate_status = 0

  allocate(indx_false_ith(npsi_tot_global,indx_gp_th),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate indx_false_ith'
  allocate(indx_false_psi(npsi_tot_global,indx_gp_th),stat=allocate_status)
  if (allocate_status /=0) write(*,*) 'Could not allocate indx_false_ith'

  do ith = 1, indx_gp_th
    ipsi_false=0
    ipsi_true =0
    do ipsi = 1, npsi_tot_global
      if (indx_gp((ipsi-1)*nth_global+indx_false_th(ith)) .eqv. .true.) then
        ipsi_true = ipsi_true+1
      else if (indx_gp((ipsi-1)*nth_global+indx_false_th(ith)) .eqv. .false.) then
        ipsi_false = ipsi_false+1
        indx_false_ith(ipsi_false,ith)=(ipsi-1)*nth_global+indx_false_th(ith)
        indx_false_psi(ipsi_false,ith) = ipsi_true
      end if
    end do
  end do

end subroutine indx_false_ith_calc

end module index_function
