module geom

  implicit none

  private

  public :: geom_read_nml, geom_init, flux_function_axial_diverted

  !> Magnetic equilibrium type
  character (len = 20), save, public :: eq_type

  !True if partial flux surfaces are added
  logical, save, public :: pfs

  !> Ratio between minor radius and major radius
  real, public, save :: eps

  !> Ratio beween the radius of the limiting inner flux surface and the limiting
  !> outer flux surface
  real, public, save :: lim_in_psi

  !> Angular position of the reference point on the inner limiting flux surface
  real, public, save :: lim_in_th

  !> Coefficient for axial diverted geometry
  real, save :: psi0

  !> Coefficient for axial diverted geometry
  real, save :: qq0

  !> Coefficient for axial diverted geometry
  real, save :: alpha

  !> Coefficient for axial diverted geometry
  real, save :: c_wire

  !> Coefficient for axial diverted geometry
  real, save :: y_wire

  !> Factor of the radial distance at constant th which decides wether a pfs is
  !> created, or not
  real, save :: pfs_val

  !> Maximum number of iterations at biscection method to determine grid at non
  !> circular geometry
  integer, save :: nmax

  !> Function value of the output point has to differ less than toll from the
  !> input function value
  real, save :: tol

    !> 1 = All parallel operators are projected in the same poloidal plane with
  !> an imaginary distance delta_phi between the poloidal plane and the next
  integer, save :: delta_phi_mod

  !> ratio of the angle between the main poloidal plane and the alpha/beta
  !> planes to the angle between two grid points at constant radius,
  !> if delta_phi_mod = 1
  real, save :: delta_phi

  !>  Amount of integration steps to calculate the penetration points
  integer, public, save :: ndphi

  !> Length in phi direction of one rk4 step for the calculation of the pp
  real, public, save :: dphi

  !> Amount of given toroidal planes (# of angles phi)
  integer, public, save :: nphi

  !> Amount of given grid points on the closed flux surfaces in the poloidal
  !> plane (# angles theta)
  integer, public, save :: nth

  !> Amount of radial grid points of the cfs in the poloidal plane
  integer, public, save :: npsi

  !> Amount of grid points for each partial flux surface for eq_type = 'axial'
  integer, save, public :: nth_pfs

  !> Amount of partial flux surfaces for eq_type = 'axial'
  integer, save, public :: npsi_pfs

  !> Amount of radial coordinate points, npsi+npsi_pfs
  integer, save, public :: npsi_tot

  !> Amount of total points. gtot = #pfs+#cfs (#pfs+#cfs)*4;
  !> last terms corresponds to the penetration points
  integer, save, public :: gtot

  !> Analytic q-profile
  character (len = 20), save, public :: prof_type

  !> Coefficients for the analytic q-profile as a function of radius.
  !> Relevant for circular type
  real, public, save :: qprof_coef(3)

  ! Q-factor for ey_type='axial_circular' and prof_type='constant'
  real, public, save :: qq_const

  !> Maximum normalized value of the parallel velocity grid
  real, public, save :: vpmax

  !> Ratio between artificial drift in Z direction and vpmax
  real, public, save :: vpdz

  !> x-coordinate of the closed flux surfaces (cfs)
  real, public, allocatable, save :: gx_cfs(:)
  !> y-coordinate of the closed flux surfaces (cfs)
  real, public, allocatable, save :: gy_cfs(:)
  !> poloidal coordinate of the closed flux surfaces (cfs)
  real, public, allocatable, save :: gth_cfs(:)
  !> radial coordinate of the closed flux surfaces (cfs)
  real, public, allocatable, save :: gpsi_cfs(:)
  !> flux functdion of the closed surfaces (cfs)
  !> for same psi index ff_cfs should be constant
  real, public, allocatable, save :: ff_cfs(:)

  !> x-coordinate of the partial flux surfaces (pfs)
  real, public, allocatable, save :: gx_pfs(:)
  !> y-coordinate of the partial flux surfaces (pfs)
  real, public, allocatable, save :: gy_pfs(:)
  !> poloidal coordinate of the partial flux surfaces (pfs)
  real, public, allocatable, save :: gth_pfs(:)
  !> radial coordinate of the partial flux surfaces (pfs)
  real, public, allocatable, save :: gpsi_pfs(:)

  !> x-coordinate of the total geometry array with pfs, cfs and
  !> penetration points. Indexes due to index_function
  real, public, allocatable, save :: gx(:)
  !> y-coordinate of the total geometry array with pfs, cfs and
  !> penetration points. Indexes due to index_function
  real, public, allocatable, save :: gy(:)
  !> poloidal coordinate of the total geometry array with pfs, cfs and
  !> penetration points. Indexes due to index_function
  real, public, allocatable, save :: gth(:)
  !> radial coordinate of the total geometry array with pfs, cfs and
  !> penetration points. Indexes due to index_function
  real, public, allocatable, save :: gpsi(:)

  !> flux function. For same psi index, ff should be constant
  real, public, allocatable, save :: ff(:)

  !> x-coordinate to call geom_rk4
  real, public, allocatable, save :: gx_tmp(:)
  !> y-coordinate to call geom_rk4
  real, public, allocatable, save :: gy_tmp(:)
  !> Temporary path length in parallel direction
  real, public, allocatable, save :: dspar_tmp(:)
  !> Temporary path length in poloidal direction
  real, public, allocatable, save :: dsth_tmp(:)

  !> x-coordinate for intermediate step in rk4
  real, public, allocatable, save :: gxk(:,:)
  !> y-coordinate for intermediate step in rk4
  real, public, allocatable, save :: gyk(:,:)
  !> path length in parallel direction for intermediate step in rk4
  real, public, allocatable, save :: dspark(:,:)

  !> Intermediate x coordinate/right hand side of the integration of the pp
  real, public, allocatable, save :: rhsxk(:)
  !> Intermediate y coordinate/right hand side of the integration of the pp
  real, public, allocatable, save :: rhsyk(:)
  !> Intermediate rhs of calculation of the path length to each pp
  real, public, allocatable, save :: rhsdspark(:)

  !> Intermediate magnetic field in x direction of the integration of the pp
  real, public, allocatable, save :: bxk(:)
  !> Intermediate magnetic field in y direction of the integration of the pp
  real, public, allocatable, save :: byk(:)


  !> Location of the pfs regarding the cfs. Just for eq_type 'axial_circular'
  real, public, allocatable, save :: pfs_psi_location(:)

  !> Path length along the field line from each point to all four penetration
  !> points, ordered after s_p/m_1/2_gridindex
  !> Like s_a_1_1, s_a_2_1, s_b_1_1, s_b_2_1, s_a_1_2, ...
  real, public, allocatable, save :: dspar(:)

  !> Path length in poloidal direction from each point to the next
  !> contraclockwise
  real, public, allocatable, save :: dsth(:)

  !> Path length in radial direction from each point to the next, from a grid
  !> inner grid point to the next with larger radius
  real, public, allocatable, save :: dspsi(:)

  !> Path length in radial direction from each points to the next, with skipping
  !> all ghost, but not points on the pfs. HERE: For all nth
  real, public, allocatable, save :: dspsi_true(:)


  !> Clockwise nearest grid point in poloidal direction to all penetration points
  !> Ordered in the same way like dspar
  integer, public, allocatable, save :: geom_nearest(:,:,:)

  !> path length between each penetration point and its clockwise nearest
  !> poloidal neighbour
  real, public, allocatable, save :: dsthpp(:)

  !difference in the angle betwen two points at the inner limiting flux surfaces
  real, public,save :: delta_theta

contains

subroutine geom_read_nml(control_file_unit)

  integer, intent(in) :: control_file_unit

  namelist /geom/       &
     & eq_type, pfs, eps, lim_in_psi, lim_in_th, &
     & psi0,qq0,alpha,c_wire,y_wire, nmax, tol,pfs_val,&
     & delta_phi_mod, delta_phi, ndphi, nphi, nth, nth_pfs, npsi_pfs, &
     & prof_type, qprof_coef,qq_const, vpmax, vpdz

  read(control_file_unit,NML=geom)

end subroutine geom_read_nml

subroutine geom_init

  use general, only : gkw_abort

  integer :: ipsi, ith, k

  geometry_type : select case(eq_type)

  case('axial_circular')
    call geom_create_axial
  case('axial_diverted')
    call geom_axial_diverted
  case default
      call gkw_abort('No proper geometry type chosen')
  end select geometry_type

  ! stop 'in geom_init'

  write(*,*)
  write(*,*) 'Grid succsessfully created'
  write(*,*)

  !write(*,*) (gth(nth*npsi_tot+1)-gth(1))*gpsi(1), dsthpp(1)
!  write(*,*) dsthpp(:)

!   ith = 1
!   do k = 1, 2
!     do ipsi = 1, npsi_tot
!      ! write(*,*) ((ipsi-1)*nth+ith-1)*4+k, ith, k, ipsi, nth
!       write(*,*) k, ith, ipsi, dsthpp(((ipsi-1)*nth+ith-1)*4+k), &
!          & dsthpp(((ipsi-1)*nth+ith-1)*4+k+2), &
!           & gpsi((ipsi-1)*nth+ith), &
!          ! & gpsi(nth*npsi_tot+((ipsi-1)*nth+ith-1)*4+k), &
!          ! & gpsi(nth*npsi_tot+((ipsi-1)*nth+ith-1)*4+k+2), &
!          !& gth((ipsi-1)*nth+ith) &
!          & gth(nth*npsi_tot+((ipsi-1)*nth+ith-1)*4+k) - &
!          & gth((ipsi-1)*nth+ith), &
!          & gth(nth*npsi_tot+((ipsi-1)*nth+ith-1)*4+k+2) - &
!          & gth((ipsi-1)*nth+ith), &
!          & (gth(nth*npsi_tot+((ipsi-1)*nth+ith-1)*4+k) - &
!          & gth((ipsi-1)*nth+ith))*gpsi((ipsi-1)*nth+ith), &
!          & (gth(nth*npsi_tot+((ipsi-1)*nth+ith-1)*4+k+2) - &
!          & gth((ipsi-1)*nth+ith))*gpsi((ipsi-1)*nth+ith)

! !      do ith = 1, nth

!         !write(*,*) dsthpp(((ipsi-1)*nth+ith-1)*4+k)

!  !     end do
!     end do
!   end do
!   stop

end subroutine geom_init

subroutine geom_create_axial

  use control,          only : open_status
  use constants,        only : pi
  use index_function,   only : indx_add_gp, indx_init, indx, indx_gp
  use general,          only : gkw_abort
  use global,         only : nth_global, npsi_tot_global

  ! length of the inner limiting flux surface
  real :: length_in
  ! length between to grid points on the inner limiting flux surface
  real :: delta_length_in
  ! difference in the angle betwen two points at the inner limiting flux surfaces
  real :: delta_psi
  !Distance between two radii at constant theta
  real :: delta_npsi

  ! Variables for loops
  integer :: iphi, ith, ipsi, ipp, i, k, l

  ! Temporary loop index
  integer :: indx_tmp

  ! Temporary q value
  real :: qq
  ! Temporary phi value
  real :: delta_phi_abs

  ! Dummy variable
  real :: dum, dum1, dum2

  !Dummy arrrays
  real, allocatable :: dum_a1(:), dum_a2(:)

  integer :: allocate_status = 0

  ! Determine the amount of radial points
  length_in = 2 * pi * lim_in_psi * eps
  delta_length_in = length_in / nth
  delta_psi = eps * (1 - lim_in_psi)
  npsi = ceiling(delta_psi / delta_length_in) + 1

  delta_theta = 2*pi/ nth
  delta_npsi = delta_psi / (npsi - 1)

  write(*,*) 'nphi:    ', nphi
  write(*,*) 'nth:     ', nth
  write(*,*) 'npsi:    ', npsi

  if (npsi <= 5) then
    call gkw_abort('geom: npsi <= 5. Increase nth, or decrease lim_in_psi')
  end if


  ! Create the grid points on the arbitrary cfs
  allocate(gx_cfs(nth*npsi),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate gx_cfs'
  allocate(gy_cfs(nth*npsi),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate gy_cfs'
  allocate(gth_cfs(nth*npsi),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate gth_cfs'
  allocate(gpsi_cfs(nth*npsi),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate gpsi_cfs'

  do ipsi = 1, npsi
    do ith = 1, nth
      gth_cfs((ipsi-1)*nth+ith) = (ith-1)*delta_theta
      gpsi_cfs((ipsi-1)*nth+ith) = lim_in_psi*eps + (ipsi-1)*delta_npsi
      call geom_cartesian(gth_cfs((ipsi-1)*nth+ith),gpsi_cfs((ipsi-1)*nth+ith),&
         & gx_cfs((ipsi-1)*nth+ith),gy_cfs((ipsi-1)*nth+ith))
    end do
  end do

  open (unit=20,file='geom_g_cfs.dat',status=open_status, position = 'append')
  do ipsi = 1, npsi
    do ith = 1, nth
      write(20,*) ((ipsi-1)*nth+ith), &
         & gx_cfs((ipsi-1)*nth+ith), gy_cfs((ipsi-1)*nth+ith)
    end do
  end do
  close(unit=20)

  ! Create the partial flux surfaces and the additional ghost points
  if (pfs) then

    write(*,*) 'nth_pfs: ', nth_pfs
    write(*,*) 'npsi_pfs:', npsi_pfs

    !Check if the artificial pfs fit in the existing grid
    if (npsi == 1) then
      write(*,*) 'STOP: Partial flux surfaces not possible for npsi = 1'
      stop
    else if (npsi <= npsi_pfs) then
      write(*,*) 'STOP: npsi smaller than npsi_pfs'
      stop
    end if

    !Locate the pfs in the radial coordinate regarding the cfs
    allocate(pfs_psi_location(npsi_pfs),stat=allocate_status)
    if(allocate_status /=0) write (*,*) 'Could not allocate pfs_psi_location'
    do i = 1, npsi_pfs
      pfs_psi_location(i) = npsi-npsi_pfs+i-1
      !write(*,*) i, pfs_psi_location(i)
    end do

    npsi_tot = npsi + npsi_pfs
  else

    npsi_tot = npsi

  end if
  write(*,*) 'npsi_tot:', npsi_tot

  gtot = nth*npsi_tot + nth*npsi_tot*4
  nth_global = nth
  npsi_tot_global = npsi_tot



  ! Write the grid points of the cfs and pfs together in an array for each
  ! coordinate and locate the with .false. or .true. in indx_add_gp, wheter
  ! they are a ghost point (possible for pfs grid points), or not.
  allocate(gx(gtot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate gx'
  allocate(gy(gtot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate gy'
  allocate(gth(gtot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate gth'
  allocate(gpsi(gtot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate gpsi'

  call indx_init(gtot)
  l = 0

  if (pfs) then
    do ipsi = 1, npsi
      !cfs
      do ith = 1, nth

        gth((ipsi-1+l)*nth+ith)  = gth_cfs((ipsi-1)*nth+ith)
        gpsi((ipsi-1+l)*nth+ith) = gpsi_cfs((ipsi-1)*nth+ith)
        call geom_cartesian(&
           & gth((ipsi-1+l)*nth+ith),gpsi((ipsi-1+l)*nth+ith),&
           & gx((ipsi-1+l)*nth+ith),gy((ipsi-1+l)*nth+ith))
        call indx_add_gp((ipsi-1+l)*nth+ith,.true.)
        !write(*,*) (ipsi-1+l)*nth+ith,.true.
      end do
      !pfs
      if (l+1 <= npsi_pfs) then
        if ((pfs_psi_location(l+1) == ipsi)) then
          l = l+1
          k = 1
          do ith = 1, nth
            gth((ipsi-1+l)*nth+ith) = gth((ipsi-1+l-1)*nth+ith)
            gpsi((ipsi-1+l)*nth+ith) = gpsi((ipsi-1+l-1)*nth+ith) + delta_npsi/2
            call geom_cartesian(&
               & gth((ipsi-1+l)*nth+ith),gpsi((ipsi-1+l)*nth+ith),&
               & gx((ipsi-1+l)*nth+ith),gy((ipsi-1+l)*nth+ith))

            if (k <= nth_pfs) then
              call indx_add_gp((ipsi-1+l)*nth+ith,.true.)
              k = k+1
              !write(*,*) (ipsi-1+l)*nth+ith,.true.
            else
              call indx_add_gp((ipsi-1+l)*nth+ith,.false.)
              !write(*,*) (ipsi-1+l)*nth+ith,.false.
            end if
          end do
        end if
      end if

    end do
  else

    !No pfs
    do ipsi = 1, npsi
      !cfs
      do ith = 1, nth
        gth((ipsi-1+l)*nth+ith)  = gth_cfs((ipsi-1)*nth+ith)
        gpsi((ipsi-1+l)*nth+ith) = gpsi_cfs((ipsi-1)*nth+ith)
        call geom_cartesian(&
           & gth((ipsi-1+l)*nth+ith),gpsi((ipsi-1+l)*nth+ith),&
           & gx((ipsi-1+l)*nth+ith),gy((ipsi-1+l)*nth+ith))
        call indx_add_gp((ipsi-1+l)*nth+ith,.true.)
        !write(*,*) (ipsi-1+l)*nth+ith,.true.
      end do
    end do
  end if

  iphi = 1
  ipp = 0
  do ipsi = 1, npsi_tot
    do ith = 1, nth
      if (ipsi == 1) then
        !write(*,*) indx(iphi,ith,ipsi,ipp), gth(indx(iphi,ith,ipsi,ipp)), &
        !  & gpsi(indx(iphi,ith,ipsi,ipp))
      end if
    end do
    !stop
  end do

  ! Set the periodicity of theta from [0,2*pi] to [-pi,pi]
  ! do i = 1, nth*npsi_tot
  !   if (gth(i) > pi) then
  !     gth(i) = gth(i)-2*pi
  !   end if
  ! end do

  open (unit=20,file='geom_g.dat',status=open_status, position = 'append')
  do ipsi = 1, npsi_tot
    do ith = 1, nth
      indx_tmp = ((ipsi-1)*nth+ith)
      write(20,*) indx_tmp, gx(indx_tmp), gy(indx_tmp), &
         & gth(indx_tmp), gpsi(indx_tmp)
    end do
  end do
  close(unit=20)

  select case(delta_phi_mod)
  case(1)
    call geom_q_axial(gx(1),gy(1),qq)
    delta_phi_abs = 2*pi/nth*delta_phi*qq
  case(2)
    delta_phi_abs = 2*pi / nphi
  case default
    write(*,*) 'No proper choice of modus for delta_phi_mod'
  end select

  ! Calculate the penetration points
  iphi = 1
  ipp = 0
  do ith = 1, nth
    do ipsi = 1, npsi_tot
      indx_tmp = indx(iphi,ith,ipsi,0)
      call geom_q_axial(gx(indx_tmp), gy(indx_tmp),qq)

      gth(indx(iphi,ith,ipsi,2)) = gth(indx_tmp) + 2*delta_phi_abs/qq
      gth(indx(iphi,ith,ipsi,1)) = gth(indx_tmp) +   delta_phi_abs/qq
      gth(indx(iphi,ith,ipsi,-1))= gth(indx_tmp) -   delta_phi_abs/qq
      gth(indx(iphi,ith,ipsi,-2))= gth(indx_tmp) - 2*delta_phi_abs/qq
      gpsi(indx(iphi,ith,ipsi,2))  = gpsi(indx_tmp)
      gpsi(indx(iphi,ith,ipsi,1))  = gpsi(indx_tmp)
      gpsi(indx(iphi,ith,ipsi,-1)) = gpsi(indx_tmp)
      gpsi(indx(iphi,ith,ipsi,-2)) = gpsi(indx_tmp)
    end do
  end do

  do i = nth*npsi_tot+1, gtot
    call geom_cartesian(gth(i),gpsi(i),gx(i),gy(i))
  end do

  ! Set the periodicity of theta from [0,2*pi] to [-pi,pi]
  do i = 1, gtot
    if (gth(i) > pi) then
      gth(i) = gth(i)-2*pi
    else if(gth(i) <= -pi ) then
      gth(i) = gth(i)+2*pi
    end if
  end do

  open (unit=20,file='geom_g_test.dat',status=open_status, position = 'append')
   ! do ipsi = 1, npsi_tot
   do ipsi = 1, 1
     do ith = 1, nth
       do ipp = -2,2,1
         write(20,*) gx(indx(iphi,ith,ipsi,0)),gy(indx(iphi,ith,ipsi,0)),&
            & gx(indx(iphi,ith,ipsi,1)),gy(indx(iphi,ith,ipsi,1)),&
            & gx(indx(iphi,ith,ipsi,2)),gy(indx(iphi,ith,ipsi,2)),&
            & gx(indx(iphi,ith,ipsi,-1)),gy(indx(iphi,ith,ipsi,-1)),&
            & gx(indx(iphi,ith,ipsi,-2)),gy(indx(iphi,ith,ipsi,-2))
       end do
     end do
   end do
   close(unit=20)

  !####
  !### Calculate the distances in each direction: s, psi,th
  !###

  ! Calculate the parallel path length
  allocate(dspar(nth*npsi_tot*4),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate dspar'

  do i = 1, nth*npsi_tot
    call geom_q_axial(gx(i), gy(i),qq)
    do k = 1,4
      dum = delta_phi_abs
      select case(k)
      case(1)
        dum = dum*1
      case(2)
        dum = dum*2
      case(3)
        dum = dum*(-1)
      case(4)
        dum = dum*(-2)
      end select

      dspar((i-1)*4+k)=dum*sqrt(1+(gpsi(i)/qq)**2)

    end do
  end do

  ! Calculate the path length between two points in poloidal direction
  allocate(dsth(nth*npsi_tot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate dsth'

  do ipsi = 1, npsi_tot
    do ith = 1, nth
      indx_tmp = (ipsi-1)*nth+ith
      if(ith == nth) then
        dsth(indx_tmp) = &
           & geom_d_theta(gth(indx_tmp),gth((ipsi-1)*nth+1))*gpsi(indx_tmp)
      else
        dsth(indx_tmp) = &
           & geom_d_theta(gth(indx_tmp),gth(indx_tmp+1))*gpsi(indx_tmp)
      end if
      !dsth(i) = 2*pi*gpsi(i)/nth
    end do
  end do

  ! Calculate the path length between two points in radial direction
  allocate(dspsi(nth*npsi_tot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate dspsi'
  do ith = 1,nth
    do ipsi = 1,npsi_tot
      if (ipsi == npsi_tot) then
        dspsi((ipsi-1)*nth+ith) = 0.
      else
        dspsi((ipsi-1)*nth+ith) = gpsi((ipsi)*nth+ith)-gpsi((ipsi-1)*nth+ith)
      end if
    end do
  end do

  ! Test
  ! do ith = 1, 1
  !   do ipsi = 1, npsi_tot
  !     write(*,*) dspsi((ipsi-1)*nth+ith)
  !   end do
  ! end do


  call geom_dspsi_true_calc

  ! Test
  ! do ith = 1, nth
  !   do ipsi = 1, npsi_tot
  !     if (ith == 1 .or. ith == 32) then
  !       write(*,*) ith, ipsi, dspsi_true((ipsi-1)*nth+ith),&
  !          & indx_gp((ipsi-1)*nth+ith)
  !     end if
  !   end do
  ! end do
  ! stop


  ! Determine the clockwise nearest grid points in poloidal direction to each
  ! penetration point
  allocate(geom_nearest(nth,npsi_tot,4),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate geom_nearest'

  allocate(dum_a1(nth),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate dum_a1'
  allocate(dum_a2(nth),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate dum_a2'

  iphi =1
  do ipsi = 1, npsi_tot
    do ith = 1, nth
      dum_a1(ith) = gx((ipsi-1)*nth+ith)
      dum_a2(ith) = gy((ipsi-1)*nth+ith)
    end do

    do ith = 1, nth
      do k = 1, 4
        indx_tmp = ((ipsi-1)*nth+ith-1)*4+k

        geom_nearest(ith,ipsi,k) = &
           & geom_nearest_point(gx(nth*npsi_tot+indx_tmp),&
           & gy(nth*npsi_tot+indx_tmp),dum_a1,dum_a2)
      end do
    end do
  end do

  deallocate(dum_a1)
  deallocate(dum_a2)

  ! Calculate the path length between each penetration point and its
  ! clockwise nearest poloidal neighbour

  allocate(dsthpp(nth*npsi_tot*4),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate dsthpp'

  iphi = 1
  ipp = 0

  do ipsi = 1, npsi_tot; do ith = 1, nth

    do ipp = -2,2
      select case(ipp)
      case (-2)
        k = 4
      case(-1)
        k = 3
      case(0)
      case(1)
        k = 1
      case(2)
        k = 2
      case default
        call gkw_abort('geom: no proper ipp')
      end select

      select case(ipp)
      case(-2,-1,1,2)
        indx_tmp = indx(iphi,ith,ipsi,ipp)-nth*npsi_tot

        dsthpp(indx_tmp) = &
           & geom_d_theta(gth(nth*npsi_tot + indx_tmp),&
           & gth(indx(iphi,geom_nearest(ith,ipsi,k),ipsi,0))) * &
           & gpsi(nth*npsi_tot + indx_tmp)

      end select

    end do
    ! stop
  end do; end do

end subroutine geom_create_axial

!> Main subroutine to create the axial diverted equilibrium described by
!> Stegmeier, PHD-Thesis, Grillix ...
subroutine geom_axial_diverted

  use constants,      only : pi
  use index_function, only : indx, indx_init,indx_psi_pfs, indx_gp
  use control,        only : open_status
  use general,        only : gkw_abort
  use global,         only : nth_global, npsi_tot_global

  real :: gpsi0,gth0,gx0, gy0, gpsi1,gth1,gx1,gy1
  real :: ff0,ff1
  
  ! difference in the angle betwen both limiting flux surfaces at angle gth0
  real :: delta_psi
  !Distance between two point at angle gth0
  real :: delta_npsi
  ! chord length between to grid points on the inner limiting flux surface
  real :: delta_length_in, delta_length_in_tmp

  real :: gx_tmp(nth), gy_tmp(nth), gth_tmp(nth), gpsi_tmp(nth)

  integer :: allocate_status = 0

  integer :: iphi, ith, ipsi, ipp, indx_tmp, ipsi_tmp, jpsi

  real :: psi_min, psi_max
  real :: x0, y0, x1,y1,psi1

  !difference between two grid points in radial direction of the cfs
  real :: dpsi_tmp

  integer :: npsi_pfs, npsi_pfs_tmp

  integer, allocatable :: npsi_pfs_indx(:)
  real, allocatable :: npsi_pfs_dpsi(:), npsi_pfs_ff(:)

  real:: x_tmp, y_tmp, psi_tmp

  integer :: i
  character(len = 128) :: file_name

  delta_psi = eps * (1 - lim_in_psi)
  delta_theta = 2*pi/ nth
  delta_npsi = delta_psi / (npsi - 1)

  x0 = 0.
  y0 = -0.021

  gth_tmp(1)  = lim_in_th
  gpsi_tmp(1) = lim_in_psi*eps
  call geom_cartesian(gth_tmp(1),gpsi_tmp(1),gx_tmp(1),gy_tmp(1))
  ff0 = flux_function_axial_diverted(gpsi_tmp(1),gy_tmp(1))

  ! psi_min = .022

  do ith = 2, nth
    psi_min = 0.8 * gpsi_tmp(ith-1)
    psi_max = 1.2 * gpsi_tmp(ith-1)
     gth_tmp(ith) = gth_tmp(1) + (ith-1)*delta_theta

    gth_tmp(ith) = gth_tmp(1) +(ith-1)*delta_theta
    gpsi_tmp(ith) = bisection(gth_tmp(ith),psi_min,psi_max,ff0)

    call geom_cartesian(gth_tmp(ith),gpsi_tmp(ith),gx_tmp(ith),gy_tmp(ith))
  end do

  open (unit=20,file='geom_g_tmp.dat',status=open_status, position = 'append')
    do ith = 1, nth
      write(20,*) ith, gx_tmp(ith), gy_tmp(ith), &
         & gth_tmp(ith), gpsi_tmp(ith)
  end do
  close(unit=20)

  ! Determine the minimum distance on the field line between two point with the
  ! chord length

  delta_length_in = sqrt((gx_tmp(nth)-gx_tmp(1))**2+(gy_tmp(nth)-gy_tmp(1))**2)
  do ith = 1, nth-1
    delta_length_in_tmp = &
       & sqrt((gx_tmp(ith)-gx_tmp(ith+1))**2+(gy_tmp(ith)-gy_tmp(ith+1)**2))
    if(delta_length_in_tmp<delta_length_in) then
      delta_length_in = delta_length_in_tmp
    end if
  end do
  npsi = ceiling(delta_psi / delta_length_in) + 1
  delta_npsi = delta_psi / (npsi - 1)

  if (npsi <= 5) then
    call gkw_abort('geom: npsi <= 5. Increase nth, or decrease r_lim_in')
  end if

  write(*,*) 'nphi: ', nphi
  write(*,*) 'nth:  ', nth
  write(*,*) 'npsi: ', npsi

  ! Create the grid points on the arbitrary cfs
  allocate(gx_cfs(nth*npsi),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate gx_cfs'
  allocate(gy_cfs(nth*npsi),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate gy_cfs'
  allocate(gth_cfs(nth*npsi),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate gth_cfs'
  allocate(gpsi_cfs(nth*npsi),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate gpsi_cfs'
  allocate(ff_cfs(nth*npsi),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate ff_cfs'

  iphi = 1
  ipp = 0
  do ipsi = 1, npsi
    do ith = 1, nth
      indx_tmp = (ipsi-1)*nth+ith
      if (ipsi == 1) then
        gx_cfs(indx_tmp) = gx_tmp(ith)
        gy_cfs(indx_tmp) = gy_tmp(ith)
        gth_cfs(indx_tmp) = gth_tmp(ith)
        gpsi_cfs(indx_tmp) = gpsi_tmp(ith)
        ff_cfs(indx_tmp) = &
           & flux_function_axial_diverted(gpsi_cfs(indx_tmp),gy_cfs(indx_tmp))
      else
        gth_cfs(indx_tmp) = gth_tmp(ith)
        if (ith == 1) then
          gpsi_cfs(indx_tmp) = gpsi_cfs((ipsi-2)*nth+ith) + delta_npsi
          call geom_cartesian(gth_cfs(indx_tmp),gpsi_cfs(indx_tmp), &
             & gx_cfs(indx_tmp),gy_cfs(indx_tmp))
          ff_cfs(indx_tmp) = &
             & flux_function_axial_diverted(gpsi_cfs(indx_tmp),gy_cfs(indx_tmp))
        else
          psi_min = 0.7 * gpsi_cfs((ipsi-1)*nth+ith-1)
          psi_max = 1.1 * gpsi_cfs((ipsi-1)*nth+ith-1)
          gpsi_cfs(indx_tmp) = bisection(gth_cfs(indx_tmp),psi_min,psi_max,&
             & ff_cfs((ipsi-1)*nth+1))
          call geom_cartesian(gth_cfs(indx_tmp),gpsi_cfs(indx_tmp), &
             & gx_cfs(indx_tmp),gy_cfs(indx_tmp))
          ff_cfs(indx_tmp) = &
             & flux_function_axial_diverted(gpsi_cfs(indx_tmp),gy_cfs(indx_tmp))
        end if
      end if
      ! write(*,*) ipsi, ith, gth_cfs(indx_tmp), gpsi_cfs(indx_tmp), &
      !    & gx_cfs(indx_tmp), gy_cfs(indx_tmp), ff_cfs(indx_tmp)
    end do
  end do

  open (unit=20,file='geom_g_cfs.dat',status=open_status, position = 'append')
  do ipsi = 1, npsi
    do ith = 1, nth
      write(20,*) ((ipsi-1)*nth+ith), &
         & gx_cfs((ipsi-1)*nth+ith), gy_cfs((ipsi-1)*nth+ith),&
         & ff_cfs((ipsi-1)*nth+ith)
    end do
  end do
  close(unit=20)

  npsi_tot = npsi
  npsi_pfs = 0
  ipsi_tmp = 0
  if (pfs) then
    ! Determine the amount of pfs flux surfaces
    do ipsi = 1, npsi-1
      do ith = 1, nth
        indx_tmp = (ipsi-1)*nth+ith
        dpsi_tmp = abs(gpsi_cfs((ipsi)*nth+ith)-gpsi_cfs((ipsi-1)*nth+ith))
        if(dpsi_tmp &
           & > pfs_val*delta_npsi) then
          ! write(*,*) ith, ipsi, dpsi_tmp/delta_npsi
          if(ipsi_tmp /= ipsi) then
            ipsi_tmp = ipsi
            npsi_tot = npsi_tot +1
            npsi_pfs = npsi_pfs +1
          end if
        end if
      end do
    end do

    write(*,*) 'npsi_pfs: ', npsi_pfs
    write(*,*) 'npsi_tot: ', npsi_tot

    gtot = nth*npsi_tot + nth*npsi_tot*4
    call indx_init(gtot,npsi_pfs)

    nth_global = nth
    npsi_tot_global = npsi_tot

    allocate(npsi_pfs_ff(npsi_pfs),stat=allocate_status)
    if(allocate_status /=0) write (*,*) 'Could not allocate npsi_pfs_ff'

    npsi_pfs_tmp = 0
    ipsi_tmp = 0
    jpsi = 0
    do ipsi = 1, npsi-1
      jpsi = jpsi+1
      do ith = 1, nth
        indx_tmp = (ipsi-1)*nth+ith
        dpsi_tmp = abs(gpsi_cfs((ipsi)*nth+ith)-gpsi_cfs((ipsi-1)*nth+ith))
        if(dpsi_tmp &
           & > pfs_val*delta_npsi) then
          if(ipsi_tmp /= ipsi) then
            ipsi_tmp = ipsi
            npsi_pfs_tmp = npsi_pfs_tmp + 1
            jpsi = jpsi + 1
            indx_psi_pfs(npsi_pfs_tmp) = jpsi
            psi_tmp = gpsi_cfs(indx_tmp)+dpsi_tmp/2
            call geom_cartesian(gth_cfs(indx_tmp),psi_tmp,x_tmp, y_tmp)
            npsi_pfs_ff(npsi_pfs_tmp) = &
               & flux_function_axial_diverted(psi_tmp,y_tmp)
          end if
        end if
      end do
    end do
  end if

  ! Write the grid points of the cfs and pfs together in an array for each
  ! coordinate and locate the with .false. or .true. in indx_add_gp, wheter
  ! they are a ghost point (possible for pfs grid points), or not.
  allocate(gx(gtot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate gx'
  allocate(gy(gtot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate gy'
  allocate(gth(gtot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate gth'
  allocate(gpsi(gtot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate gpsi'
  allocate(ff(gtot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate ff'

  !Write the cfs in the g* arrays
  ipsi_tmp = 0
  do ipsi = 1, npsi_tot
    ! Case: cfs
    if (all(indx_psi_pfs /= ipsi)) then
      ipsi_tmp = ipsi_tmp+1
      do ith = 1, nth
        indx_tmp = (ipsi_tmp-1)*nth+ith
        gx(indx(iphi,ith,ipsi,ipp)) = gx_cfs(indx_tmp)
        gy(indx(iphi,ith,ipsi,ipp)) = gy_cfs(indx_tmp)
        gth(indx(iphi,ith,ipsi,ipp)) = gth_cfs(indx_tmp)
        gpsi(indx(iphi,ith,ipsi,ipp)) = gpsi_cfs(indx_tmp)
        ff(indx(iphi,ith,ipsi,ipp)) = ff_cfs(indx_tmp)
        if (ith == 1) then
        end if
      end do
    else
      gx(indx(iphi,ith,ipsi,ipp)) = 0.
      gy(indx(iphi,ith,ipsi,ipp)) = 0.
      gth(indx(iphi,ith,ipsi,ipp)) = 0.
      gpsi(indx(iphi,ith,ipsi,ipp)) = 0.
      ff(indx(iphi,ith,ipsi,ipp)) = 0.
    end if
  end do

  ! Write the pfs int the g* arrays
  jpsi = 0
  ipsi_tmp = 0
  do ipsi = 1, npsi_tot
    ! Case pfs
    if (any(indx_psi_pfs == ipsi)) then
      if(ipsi_tmp /= ipsi) then
        ipsi_tmp = ipsi
        jpsi = jpsi +1
      end if
      do ith = 1, nth
        indx_tmp = indx(iphi,ith,ipsi,ipp)
        gth(indx_tmp) = gth(indx(iphi,ith,ipsi-1,ipp))
        gpsi(indx_tmp) = &
           & bisection(gth(indx(iphi,ith,ipsi,ipp)), &
           & gpsi(indx(iphi,ith,ipsi-1,ipp)),gpsi(indx(iphi,ith,ipsi+1,ipp)), &
           & npsi_pfs_ff(jpsi))
        call geom_cartesian(gth(indx_tmp),gpsi(indx_tmp), &
           & gx(indx_tmp),gy(indx_tmp))
        ff(indx_tmp)=flux_function_axial_diverted(gpsi(indx_tmp),gy(indx_tmp))
      end do
    end if
  end do

  ! call geom_get_gp

  ! Differentiate between real grid points (cfs and pfs) and ghost points
  do ipsi = 1, npsi_tot
    do ith = 1, nth
      if (any(indx_psi_pfs == ipsi)) then
        dpsi_tmp = abs(gpsi(indx(iphi,ith,ipsi-1,ipp)) - &
           & gpsi(indx(iphi,ith,ipsi+1,ipp)))
        if(dpsi_tmp &
           & > pfs_val*delta_npsi) then
          indx_gp(indx(iphi,ith,ipsi,ipp)) = .true.
        else
          indx_gp(indx(iphi,ith,ipsi,ipp)) = .false.
        end if
      else
        indx_gp(indx(iphi,ith,ipsi,ipp)) = .true.
      end if
    end do
  end do


  ! Test

  ! do ith = 1, nth
  !   do ipsi = 1, npsi_tot
  !     indx_tmp = indx(iphi,ith,ipsi,ipp)
  !     select case (ith)
  !     case(1,96,97,98)
  !       if (ipsi /= 1 .and. ipsi /= npsi_tot) then
  !         dpsi_tmp = abs(gpsi(indx(iphi,ith,ipsi-1,ipp))- &
  !            & gpsi(indx(iphi,ith,ipsi+1,ipp)))
  !         write(*,*) ith, ipsi, indx_gp(indx_tmp), gx(indx_tmp), &
  !            & dpsi_tmp,dpsi_tmp/(pfs_val*delta_npsi)
  !       end if
  !     end select
  !   end do
  ! end do

  ! Set the periodicity of theta from [0,2*pi] to [-pi,pi]
  do i = 1, gtot
    if (gth(i) > pi) then
      gth(i) = gth(i)-2*pi
    else if(gth(i) <= -pi ) then
      gth(i) = gth(i)+2*pi
    end if
  end do



  open (unit=20,file='geom_g.dat',status=open_status, position = 'append')
  do ipsi = 1, npsi_tot
    do ith = 1, nth
      indx_tmp = ((ipsi-1)*nth+ith)
      write(20,*) indx_tmp, gx(indx_tmp), gy(indx_tmp), &
         & gth(indx_tmp), gpsi(indx_tmp)
    end do
  end do
  close(unit=20)

  file_name = 'geom_g_true.dat'

  open(unit= 20,file=file_name,status=open_status, position='append')
  do i = 1, npsi_tot*nth
    if (indx_gp(i)) then
      write(20,*) gx(i),gy(i)
    end if
  end do
  close(unit=20)

  file_name = 'geom_g_false.dat'

  open(unit= 20,file=file_name,status=open_status, position='append')
  do i = 1, npsi_tot*nth
    if (indx_gp(i) .eqv. .false.) then
      write(20,*) gx(i),gy(i)
    end if
  end do
  close(unit=20)

  ! Alloctate arrays for path length
  allocate(dspar(nth*npsi_tot*4),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate dspar'
  allocate(dsth(nth*npsi_tot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate dsth'
  allocate(dspsi(nth*npsi_tot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate dspsi'

  ! Calculate the penetration points and path length along field line
  call geom_diverted_pp

  ! Calculate the arc length between two grid points in poloidal direction
  call geom_diverted_dsth


  ! Calculate the path length between two points in radial direction
  do ith = 1, nth
    do ipsi = 1,npsi_tot
      if (ipsi == npsi_tot) then
        dspsi(indx(iphi,ith,ipsi,ipp)) = 0.
      else
        dspsi(indx(iphi,ith,ipsi,ipp)) = gpsi(indx(iphi,ith,ipsi+1,ipp)) - &
           & gpsi(indx(iphi,ith,ipsi,ipp))
      end if
    end do
  end do

  ! Test
  ! do ith = 1, 1
  !   do ipsi = 1, npsi_tot
  !     write(*,*) dspsi(indx(iphi,ith,ipsi,ipp))
  !   end do
  ! end do

  ! Calculate the path length in radial direction skipping ghost point
  call geom_dspsi_true_calc

  ! Test
  ! do ith = 1, 1
  !   do ipsi = 1, npsi_tot
  !     write(*,*) dspsi_true(indx(iphi,ith,ipsi,ipp))
  !   end do
  ! end do

  ! Determine the clockwise nearest grid points in poloidal direction to each
  ! penetration point
  call geom_nearest_neighbours

  call geom_diverted_dsthpp
  
  iphi = 1

  ! do ipsi = 1, 1 !npsi_tot
  !   do ith = 1, nth
  !     ! if (ipsi == 1) then
  !     write(*,*) ipsi, ith, dsth(indx(iphi,ith,ipsi,0)),&
  !        & dsthpp(indx(iphi,ith,ipsi,-2)), &
  !        & dsthpp(indx(iphi,ith,ipsi,-1)), &
  !        & dsthpp(indx(iphi,ith,ipsi,+1)), &
  !        & dsthpp(indx(iphi,ith,ipsi,2)), &
  !        & geom_nearest(ith,ipsi,1), geom_nearest(ith,ipsi,2), &
  !        & geom_nearest(ith,ipsi,3), geom_nearest(ith,ipsi,4)
  !   ! end if
  !   end do
  ! end do

  ! do ipsi = 1,1
  !   do ith = 1, nth

  !     ! if (ith == 126) then !.or. ith == 105 .or. ith == 107) then
  !       write(*,*) gth(indx(iphi,ith,ipsi,0)),gth(indx(iphi,ith,ipsi,2)), &
  !          & gth(indx(iphi,108,ipsi,0)),gth(indx(iphi,109,ipsi,0)), &
  !          & gth(indx(iphi,110,ipsi,0))
  !     ! end if

  !   end do
  ! end do

  ! stop 'at end of geom_axial_diverted'

end subroutine geom_axial_diverted

subroutine geom_cartesian(th,psi,x,y)

  real, intent(in) :: th, psi
  real, intent(out) :: x, y

  x = cos(th)*psi
  y = sin(th)*psi

end subroutine geom_cartesian

subroutine geom_q_axial(gxx,gyy,qq)

  real, intent(in)  :: gxx, gyy
  real, intent(out) :: qq

  real :: psi

  psi = sqrt(gxx ** 2 + gyy **2)

  select case(eq_type)

  case('axial_circular')
    select case(prof_type)
    case('parabolic')
      ! For the safety factor the parabolic q-profile of gkw, p.93 is used
      qq = qprof_coef(1) + qprof_coef(2) * psi ** 2
    case('constant')
      qq = qq_const
    case('default')
      write(*,*) 'No proper choice of modus for prof_type'
    end select

  case('axial_diverted')
    qq = 1/(1/qq0  - alpha *log(psi/psi0))
  end select

end subroutine geom_q_axial

function geom_d_theta(tha,thb)

  use constants, only : pi

  real, intent(in) :: tha, thb
  real :: geom_d_theta
  real :: d_theta

  d_theta = abs(tha - thb)
  if(d_theta > pi) then
    d_theta = 2*pi - d_theta
  end if
  geom_d_theta = abs(d_theta)

  ! if (geom_d_theta == 0.) then
  !   write(*,*) tha, thb
  ! end if

end function geom_d_theta

function geom_nearest_point(x,y,x_array,y_array)

  real, intent(in) :: x, y
  real, intent(in) :: x_array(:), y_array(:)

  integer :: geom_nearest_point

  ! Nearest point clockwise,counterclockwise
  integer :: i1, i2

  integer :: i, k1, k2
  integer :: array_length

  integer :: allocate_status

  real :: min1
  real :: min2

  real, allocatable :: distance(:)

  allocate_status = 0

  array_length = size(x_array)

  allocate(distance(array_length),stat=allocate_status)
  if (allocate_status /=0) write (*,*) 'Could not allocate distance'


  do i = 1, array_length
    distance(i) = sqrt((x_array(i)-x)**2+(y_array(i)-y)**2)
    !write(*,*) i, distance(i)
  end do

  min1 = distance(1)
  min2 = distance(2)
  k1 = 1
  k2 = 2

  if (min2 < min1) then
    min1 = distance(2)
    min2 = distance(1)
    k1 = 2
    k2 = 1
  end if

  do i=3,array_length
    if(distance(i) < min1) then
      min2 = min1
      min1 = distance(i)
      k2 = k1
      k1 = i
    else if(distance(i) < min2) then
      min2 = distance(i)
      k2 = i
    end if
  end do

  if (minval([k1,k2]) == 1 .and. maxval([k1,k2]) == array_length) then
    i1 = minval([k1,k2])
    i2 = maxval([k1,k2])
  else
    i1 = maxval([k1,k2])
    i2 = minval([k1,k2])
  end if

  if (i1==i2) then
    stop 'Two distance indices are the same'
  end if

  geom_nearest_point = i2

end function geom_nearest_point

!> Calculate the path length between two points in radial direction with
!> skipping of all ghost points. However the array size and the indices stay
!> the same. The length dspsi_true for all ghost points is set to 0
subroutine geom_dspsi_true_calc

  use index_function, only : indx_gp, indx

  integer :: iphi,ipsi, ith, ipp,ipsi_true
  integer :: indx_tmp

  integer :: allocate_status
  allocate_status = 0

  allocate(dspsi_true(nth*npsi_tot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate dspsi_true'

  iphi =  1
  ipp = 0

  ipsi_true = 0

  do ith = 1, nth
    do ipsi = 1, npsi_tot
      indx_tmp = indx(iphi,ith,ipsi,ipp)

      ! if(indx_gp((ipsi-1)*nth+ith)) then
      !   ipsi_true = ipsi
      !   dspsi_true((ipsi-1)*nth+ith) = dspsi((ipsi-1)*nth+ith)
      ! else
      !   dspsi_true((ipsi_true-1)*nth+ith) = &
      !      & dspsi_true((ipsi_true-1)*nth+ith) + dspsi((ipsi-1)*nth+ith)
      ! end if

      if(indx_gp(indx_tmp)) then
        ipsi_true = ipsi
        dspsi_true(indx_tmp) = dspsi(indx_tmp)

      else
        dspsi_true(indx(iphi,ith,ipsi_true,ipp)) = &
           & dspsi_true(indx(iphi,ith,ipsi_true,ipp)) + dspsi(indx_tmp)
        dspsi_true(indx_tmp) = 0.
      end if

      ! if(ith == 40) then !.and. (ipsi == 9 .or. ipsi == 10)) then
      !   write(*,*) ith,ipsi,indx_tmp,indx_gp(indx_tmp),dspsi(indx_tmp), &
      !      & dspsi_true(indx(iphi,ith,ipsi,ipp)), &
      !      & ipsi_true, dspsi_true(indx(iphi,ith,ipsi_true,ipp))
      ! end if

    end do
  end do

  ! stop

end subroutine geom_dspsi_true_calc

function flux_function_axial_diverted(psi,yy)

  real, intent(in) :: psi,yy

  real :: flux_function_axial_diverted

  flux_function_axial_diverted = &
     & (1/(2*qq0) + alpha/4)*psi**2 - alpha/2*psi**2 * log(psi/psi0) + &
     & c_wire/2 * log(1-2*yy/y_wire + psi**2/y_wire**2)

  ! write(*,*) psi,yy,q
  ! q0, alpha, psi0, c_wire, y_wire, flux_function_axial_diverted

end function flux_function_axial_diverted

! get y value
function bisection(th,psia,psib,ff)

  use general, only : gkw_abort

  ! ff value to evaluate
  ! th = angle at which function potential has to be evaluated
  ! a,b min/max radius.
  real, intent(in) :: th,psia,psib,ff

  ! Output radius
  real  :: bisection

  real  :: psiaa,psibb,psic,fa,fb,fc,xaa,yaa,xbb,ybb,xc,yc
  integer :: n

  ! write(*,*) 'bisection input :'
  ! write(*,*) th,psia,psib,ff

  ! nmax
  ! tol = toleranz

  n = 1

  psiaa = psia
  psibb = psib
  call geom_cartesian(th,psiaa,xaa,yaa)
  call geom_cartesian(th,psibb,xbb,ybb)

  fa =  flux_function_axial_diverted(psiaa,yaa)
  fb = flux_function_axial_diverted(psibb,ybb)
  if(sign(1.,fa-ff) == sign(1.,fb-ff)) then
    write(*,*) th, psiaa, psibb, fa, fb, ff
    call gkw_abort('geom/bisection: fa and fb on the same side to ff')
  end if
  do while(n <= nmax) ! limit iterations to prevent infinite loop
    psic = (psiaa+psibb)/2 ! new midpoint
    call geom_cartesian(th,psic,xc,yc)
    fc = flux_function_axial_diverted(psic,yc)
    ! write(*,*)
    ! write(*,*) th, psiaa, psic, psibb,fa,fc,fb, ff
    if (fc-ff == 0 .or. (psibb-psiaa)/2 < tol) then
      bisection = psic !solution found
      exit
    end if
    n = n+1
    fa =  flux_function_axial_diverted(psiaa,yaa)
    fb = flux_function_axial_diverted(psibb,ybb)
    if (sign(1.,(fc-ff)) == sign(1.,(fa-ff))) then
      psiaa = psic
      call geom_cartesian(th,psiaa,xaa,yaa)
    else
      psibb = psic
      call geom_cartesian(th,psibb,xbb,ybb)
    end if
  end do
  if (n > nmax) then
    call gkw_abort('geom/bisection: max number of steps exceeded')
  else
  end if

end function bisection

subroutine geom_diverted_pp

  use constants, only : pi
  use index_function, only : indx
  use control,        only : open_status
  use general,         only : gkw_abort

  real ::  sum_phi

  integer :: ith, ipsi, iphi,i,ipp

  integer :: allocate_status = 0

  real :: qq

  allocate(gx_tmp(nth*npsi_tot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate gx_tmp'
  allocate(gy_tmp(nth*npsi_tot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate gy_tmp'
  allocate(dspar_tmp(nth*npsi_tot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate dspar_tmp'

  allocate(gxk(nth*npsi_tot,2),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate gxk'
  allocate(gyk(nth*npsi_tot,2),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate gyk'
  allocate(dspark(nth*npsi_tot,2),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate dspark'

  allocate(rhsxk(nth*npsi_tot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate rhsxk'
  allocate(rhsyk(nth*npsi_tot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate rhsyk'
  allocate(rhsdspark(nth*npsi_tot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate rhsdspark'

  allocate(bxk(nth*npsi_tot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate bxk'
  allocate(byk(nth*npsi_tot),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate byk'



   delta_phi = 2*pi/nphi
   dphi = delta_phi/ndphi

   iphi = 1

   do i = 1, nth*npsi_tot
     dspar_tmp(i) = 0.
   end do

   !########################
   !### alpha direction
   !#####################
   call dcopy(nth*npsi_tot, gx(1:nth*npsi_tot), 1, gx_tmp,1)
   call dcopy(nth*npsi_tot, gy(1:nth*npsi_tot), 1, gy_tmp,1)

   sum_phi = 0
   i = 0
   do while(sum_phi < delta_phi)
     call geom_rk4
     sum_phi = sum_phi + dphi
     i = i+1
     ! write(*,*) abs(atan2(gy_tmp(1),gx_tmp(1))-gth(1))/i
   end do
   !copy gx_tmp,gy_tmp to alpha coordinates
   do ipsi = 1, npsi_tot
     do ith = 1, nth
       gx(indx(iphi,ith,ipsi,1)) = gx_tmp((ipsi-1)*nth+ith)
       gy(indx(iphi,ith,ipsi,1)) = gy_tmp((ipsi-1)*nth+ith)
       gth(indx(iphi,ith,ipsi,1)) = atan2(gy(indx(iphi,ith,ipsi,1)),&
          & gx(indx(iphi,ith,ipsi,1)))
       gpsi(indx(iphi,ith,ipsi,1)) = sqrt(gx(indx(iphi,ith,ipsi,1))**2+ &
          & gy(indx(iphi,ith,ipsi,1))**2)
       ff(indx(iphi,ith,ipsi,1)) = flux_function_axial_diverted( &
          & gpsi(indx(iphi,ith,ipsi,1)),gy(indx(iphi,ith,ipsi,1)))
       dspar(((ipsi-1)*nth+ith-1)*4+1) = dspar_tmp((ipsi-1)*nth+ith)

     end do
   end do

   ! do ipsi = 1, 1 !npsi_tot
   !   do ith = 1, nth-1
   !     call geom_q_axial(gx(indx(iphi,ith,ipsi,0)),gy(indx(iphi,ith,ipsi,0)),qq)
   !     write(*,*) ith, gth(indx(iphi,ith,ipsi,+1)),gth(indx(iphi,ith+1,ipsi,0)),qq, &
   !        & ff(indx(iphi,ith,ipsi,1)),ff(indx(iphi,ith,ipsi,0))
   !   end do
   ! end do
   ! stop

   ! do ipsi = 1, 1 !npsi_tot
   !   do ith = 1, 1
   !     write(*,*) ndphi,gx(indx(iphi,ith,ipsi,1)), gy(indx(iphi,ith,ipsi,1)), &
   !        & ff(indx(iphi,ith,ipsi,1))/ff(indx(iphi,ith,ipsi,0)), &
   !        & dspar(((ipsi-1)*nth+ith-1)*4+1)
   !   end do
   ! end do
   ! stop

   ! alpha_2 direction
   do while(sum_phi < delta_phi*2)
     call geom_rk4
     sum_phi = sum_phi + dphi
   end do
   do ipsi = 1, npsi_tot
     do ith = 1, nth
       gx(indx(iphi,ith,ipsi,2)) = gx_tmp((ipsi-1)*nth+ith)
       gy(indx(iphi,ith,ipsi,2)) = gy_tmp((ipsi-1)*nth+ith)
       gth(indx(iphi,ith,ipsi,2)) = atan2(gy(indx(iphi,ith,ipsi,2)),&
          & gx(indx(iphi,ith,ipsi,2)))
       gpsi(indx(iphi,ith,ipsi,2)) = sqrt(gx(indx(iphi,ith,ipsi,2))**2+ &
          & gy(indx(iphi,ith,ipsi,2))**2)
       ff(indx(iphi,ith,ipsi,2)) = flux_function_axial_diverted( &
          & gpsi(indx(iphi,ith,ipsi,2)),gy(indx(iphi,ith,ipsi,2)))
       dspar(((ipsi-1)*nth+ith-1)*4+2) = dspar_tmp((ipsi-1)*nth+ith)
     end do
   end do

   !!!!!!!!!!!!!!!!
   !!!  beta direction
   !!!!!!!!!!!!!!!!

   call dcopy(nth*npsi_tot, gx(1:nth*npsi_tot), 1, gx_tmp,1)
   call dcopy(nth*npsi_tot, gy(1:nth*npsi_tot), 1, gy_tmp,1)

   dphi = -abs(dphi)
   do i = 1, nth*npsi_tot
     dspar_tmp(i) = 0.
   end do

   sum_phi = 0
   do while(sum_phi > -delta_phi)
     call geom_rk4
     sum_phi = sum_phi + dphi
   end do

   !copy gx_tmp,gy_tmp to beta coordinates
   do ipsi = 1, npsi_tot
     do ith = 1, nth
       ! write(*,*) iphi,ith,ipsi,-1, indx(iphi,ith,ipsi,-1)
       gx(indx(iphi,ith,ipsi,-1)) = gx_tmp((ipsi-1)*nth+ith)
       gy(indx(iphi,ith,ipsi,-1)) = gy_tmp((ipsi-1)*nth+ith)
       gth(indx(iphi,ith,ipsi,-1)) = atan2(gy(indx(iphi,ith,ipsi,-1)),&
          & gx(indx(iphi,ith,ipsi,-1)))
       gpsi(indx(iphi,ith,ipsi,-1)) = sqrt(gx(indx(iphi,ith,ipsi,-1))**2+ &
          & gy(indx(iphi,ith,ipsi,-1))**2)
       ff(indx(iphi,ith,ipsi,-1)) = flux_function_axial_diverted( &
          & gpsi(indx(iphi,ith,ipsi,-1)),gy(indx(iphi,ith,ipsi,-1)))
       dspar(((ipsi-1)*nth+ith-1)*4+3) = dspar_tmp((ipsi-1)*nth+ith)
     end do
   end do

   ! beta_2 direction
   do while(sum_phi > - delta_phi*2)
     call geom_rk4
     sum_phi = sum_phi + dphi
   end do
   do ipsi = 1, npsi_tot
     do ith = 1, nth
       gx(indx(iphi,ith,ipsi,-2)) = gx_tmp((ipsi-1)*nth+ith)
       gy(indx(iphi,ith,ipsi,-2)) = gy_tmp((ipsi-1)*nth+ith)
       gth(indx(iphi,ith,ipsi,-2)) = atan2(gy(indx(iphi,ith,ipsi,-2)),&
          & gx(indx(iphi,ith,ipsi,-2)))
       gpsi(indx(iphi,ith,ipsi,-2)) = sqrt(gx(indx(iphi,ith,ipsi,-2))**2+ &
          & gy(indx(iphi,ith,ipsi,-2))**2)
       ff(indx(iphi,ith,ipsi,-2)) = flux_function_axial_diverted( &
          & gpsi(indx(iphi,ith,ipsi,-2)),gy(indx(iphi,ith,ipsi,-2)))
       dspar(((ipsi-1)*nth+ith-1)*4+4) = dspar_tmp((ipsi-1)*nth+ith)
     end do
   end do

   do ipsi = 1, npsi_tot
     do ith = 1, nth
       do ipp = -2,2,1
         if(abs(ff(indx(iphi,ith,ipsi,ipp))-ff(indx(iphi,ith,ipsi,0)))>tol) then
           call gkw_abort('geom/geom_diverted_pp: pp has wrong ff value')
         end if
       end do
     end do
   end do

   open (unit=20,file='geom_g_test.dat',status=open_status, position = 'append')
   ! do ipsi = 1, npsi_tot
   do ipsi = 1, 1
     do ith = 1, nth
       do ipp = -2,2,1
         write(20,*) gx(indx(iphi,ith,ipsi,0)),gy(indx(iphi,ith,ipsi,0)),&
            & gx(indx(iphi,ith,ipsi,1)),gy(indx(iphi,ith,ipsi,1)),&
            & gx(indx(iphi,ith,ipsi,2)),gy(indx(iphi,ith,ipsi,2)),&
            & gx(indx(iphi,ith,ipsi,-1)),gy(indx(iphi,ith,ipsi,-1)),&
            & gx(indx(iphi,ith,ipsi,-2)),gy(indx(iphi,ith,ipsi,-2))
       end do
     end do
   end do
   close(unit=20)

   ! do ipsi = 1, 1!npsi_tot
   !   do ith = 1+2, nth-2
   !     write(*,*) ipsi, ith, &
   !        & gth(indx(iphi,ith,ipsi,-2)),gth(indx(iphi,ith-2,ipsi,0)),&
   !        & gth(indx(iphi,ith,ipsi,-1)),gth(indx(iphi,ith-1,ipsi,0)), &
   !        & gth(indx(iphi,ith,ipsi,0)), &
   !        & gth(indx(iphi,ith,ipsi,1)),gth(indx(iphi,ith+1,ipsi,0)),&
   !        & gth(indx(iphi,ith,ipsi,2)),gth(indx(iphi,ith+2,ipsi,0))
   !   end do
   ! end do
   ! stop

 end subroutine geom_diverted_pp

 subroutine geom_rk4()

   ! use constants, only : c1

   real :: cdum, c1
   integer :: i,ith,ipsi

   integer :: nsolc
   nsolc = nth*npsi_tot

   c1 = 1.0

   ! Initialize gik
   call dcopy(nsolc, gx_tmp(1:nsolc), 1, gxk(1:nsolc,1), 1)
   call dcopy(nsolc, gy_tmp(1:nsolc), 1, gyk(1:nsolc,1), 1)
   call dcopy(nsolc, dspar_tmp(1:nsolc), 1, dspark(1:nsolc,1), 1)
   call dcopy(nsolc, gx_tmp(1:nsolc), 1, gxk(1:nsolc,2), 1)
   call dcopy(nsolc, gy_tmp(1:nsolc), 1, gyk(1:nsolc,2), 1)
   call dcopy(nsolc, dspar_tmp(1:nsolc), 1, dspark(1:nsolc,2), 1)

   ! advance a length step dphi, calculate
   call geom_calculate_rhs(gxk(1,2),gyk(1,2),dspark(1,2),&
      & rhsxk(1),rhsyk(1),rhsdspark(1))

   ! first step into solution
   cdum=c1/6.0
   call daxpy(nsolc, cdum, rhsxk(1:nsolc), 1, gx_tmp(1:nsolc), 1)
   call daxpy(nsolc, cdum, rhsyk(1:nsolc), 1, gy_tmp(1:nsolc), 1)
   call daxpy(nsolc, cdum, rhsdspark(1:nsolc), 1, dspar_tmp(1:nsolc), 1)

   ! second step initialization
   cdum=c1/2.0
   call dcopy(nsolc, gxk(1:nsolc,1), 1, gxk(1:nsolc,2), 1)
   call dcopy(nsolc, gyk(1:nsolc,1), 1, gyk(1:nsolc,2), 1)
   call dcopy(nsolc, dspark(1:nsolc,1), 1, dspark(1:nsolc,2), 1)
   call daxpy(nsolc, cdum, rhsxk(1:nsolc), 1, gxk(1:nsolc,2), 1)
   call daxpy(nsolc, cdum, rhsyk(1:nsolc), 1, gyk(1:nsolc,2), 1)
   call daxpy(nsolc, cdum, rhsdspark(1:nsolc), 1, dspark(1:nsolc,2), 1)

   ! advance a length step dphi
   call geom_calculate_rhs(gxk(1,2),gyk(1,2),dspark(1,2),&
      & rhsxk(1),rhsyk(1),rhsdspark(1))

   ! second step into solution
   cdum=c1/3.0
   call daxpy(nsolc, cdum, rhsxk(1:nsolc), 1, gx_tmp(1:nsolc), 1)
   call daxpy(nsolc, cdum, rhsyk(1:nsolc), 1, gy_tmp(1:nsolc), 1)
   call daxpy(nsolc, cdum, rhsdspark(1:nsolc), 1, dspar_tmp(1:nsolc), 1)

   ! third step initialization
   cdum=c1/2.0
   call dcopy(nsolc, gxk(1:nsolc,1), 1, gxk(1:nsolc,2), 1)
   call dcopy(nsolc, gyk(1:nsolc,1), 1, gyk(1:nsolc,2), 1)
   call dcopy(nsolc, dspark(1:nsolc,1), 1, dspark(1:nsolc,2), 1)
   call daxpy(nsolc, cdum, rhsxk(1:nsolc), 1, gxk(1:nsolc,2), 1)
   call daxpy(nsolc, cdum, rhsyk(1:nsolc), 1, gyk(1:nsolc,2), 1)
   call daxpy(nsolc, cdum, rhsdspark(1:nsolc), 1, dspark(1:nsolc,2), 1)

   ! advance a length step dphi
   call geom_calculate_rhs(gxk(1,2),gyk(1,2),dspark(1,2),&
      & rhsxk(1),rhsyk(1),rhsdspark(1))

   ! third step into solution
   cdum=c1/3.0
   call daxpy(nsolc, cdum, rhsxk(1:nsolc), 1, gx_tmp(1:nsolc), 1)
   call daxpy(nsolc, cdum, rhsyk(1:nsolc), 1, gy_tmp(1:nsolc), 1)
   call daxpy(nsolc, cdum, rhsdspark(1:nsolc), 1, dspar_tmp(1:nsolc), 1)

   ! fourth step initialization
   cdum=c1
   call dcopy(nsolc, gxk(1:nsolc,1), 1, gxk(1:nsolc,2), 1)
   call dcopy(nsolc, gyk(1:nsolc,1), 1, gyk(1:nsolc,2), 1)
   call dcopy(nsolc, dspark(1:nsolc,1), 1, dspark(1:nsolc,2), 1)
   call daxpy(nsolc, cdum, rhsxk(1:nsolc), 1, gxk(1:nsolc,2), 1)
   call daxpy(nsolc, cdum, rhsyk(1:nsolc), 1, gyk(1:nsolc,2), 1)
   call daxpy(nsolc, cdum, rhsdspark, 1, dspark(1:nsolc,2), 1)

   ! advance a length step dphi
   call geom_calculate_rhs(gxk(1,2),gyk(1,2),dspark(1,2),&
      & rhsxk(1),rhsyk(1),rhsdspark(1))

   ! fourth step into solution
   cdum=c1/6.0
   call daxpy(nsolc, cdum, rhsxk(1:nsolc), 1, gx_tmp(1:nsolc), 1)
   call daxpy(nsolc, cdum, rhsyk(1:nsolc), 1, gy_tmp(1:nsolc), 1)
   call daxpy(nsolc, cdum, rhsdspark(1:nsolc), 1, dspar_tmp(1:nsolc), 1)

end subroutine geom_rk4

subroutine geom_calculate_rhs(gxi, gyi, dspari,rhsx_tmp, rhsy_tmp,rhsdspar_tmp)

  real, intent(in) :: gxi(nth*npsi_tot), gyi(nth*npsi_tot),dspari(nth*npsi_tot)
  real, intent(out) ::  rhsx_tmp(nth*npsi_tot), rhsy_tmp(nth*npsi_tot), &
     & rhsdspar_tmp(nth*npsi_tot)

  integer :: i

  call geom_calculate_magnetic_field(gxi,gyi,bxk,byk)

  do i = 1, nth*npsi_tot
    rhsx_tmp(i) = bxk(i)*dphi
    rhsy_tmp(i) = byk(i)*dphi
    rhsdspar_tmp(i) = sqrt(1+(bxk(i))**2+(byk(i))**2)*dphi
  end do

end subroutine geom_calculate_rhs

subroutine geom_calculate_magnetic_field(gxi,gyi,bxi,byi)

  real, intent(in) :: gxi(nth*npsi_tot), gyi(nth*npsi_tot)
  real, intent(out) :: bxi(nth*npsi_tot), byi(nth*npsi_tot)

  real :: psi_tmp, qq
  real :: dpsidx, dpsidy

  integer :: i

  do i = 1, nth*npsi_tot
    psi_tmp = sqrt(gxi(i)**2+gyi(i)**2)
    call geom_q_axial(gxi(i),gyi(i),qq)
    ! qq = 1/(1/qq0  - alpha *log(psi_tmp/psi0))

    bxi(i) = - gyi(i) / qq - &
       c_wire/(gxi(i)**2 + (gyi(i)-y_wire)**2)*(gyi(i)-y_wire)
    byi(i) =   gxi(i) / qq + &
       c_wire/(gxi(i)**2 + (gyi(i)-y_wire)**2)*gxi(i)

  end do

end subroutine geom_calculate_magnetic_field

subroutine geom_diverted_dsth

  use index_function, only : indx
  use constants, only : pi

  real gx1,gx2,gy1,gy2,gth1,gth2,gpsi1,gpsi2
  real :: sum_th
  real :: dth
  integer :: iphi,ith,ipsi,ipp

  real :: tolp, tolm

  real :: maxds, sumds, maxpsi

  tolm = 0.9
  tolp = 1.1

  iphi = 1
  ipp = 0

  dth = delta_theta/ndphi

  do ipsi = 1, npsi_tot
    do ith = 1, nth
      sum_th = 0.
      gx1 = gx(indx(iphi,ith,ipsi,ipp))
      gy1 = gy(indx(iphi,ith,ipsi,ipp))
      gth1 = gth(indx(iphi,ith,ipsi,ipp))
      gpsi1 = gpsi(indx(iphi,ith,ipsi,ipp))
      do while(sum_th < delta_theta)
        gth2 = gth1 + dth
        gpsi2 = bisection(gth2,gpsi1*tolm,gpsi1*tolp,ff(indx(iphi,ith,ipsi,ipp)))
        call geom_cartesian(gth2,gpsi2,gx2,gy2)
        dsth(indx(iphi,ith,ipsi,0)) = dsth((indx(iphi,ith,ipsi,0))) + &
           & sqrt((gx1-gx2)**2+(gy1-gy2)**2)

        gx1 = gx2
        gy1 = gy2
        gth1 = gth2
        gpsi1 = gpsi2
        sum_th = sum_th + dth
      end do
    end do
  end do

  sumds = 0.
  maxds = 0.
  maxpsi = 0.
  do ith = 1, nth
    do ipsi = 1, 1 !npsi_tot
      sumds = sumds + dsth(indx(iphi,ith,ipsi,0))
      if (dsth(indx(iphi,ith,ipsi,0)) > maxds) then
        maxds = dsth(indx(iphi,ith,ipsi,0))
      end if
      if (gpsi(indx(iphi,ith,ipsi,0)) > maxpsi) then
        maxpsi = gpsi(indx(iphi,ith,ipsi,0))
      end if
    end do
  end do

  ! do ith = 1, nth
  !   do ipsi = 1, 1 !npsi_tot
  !     write(*,*) ipsi, ith, dsth(indx(iphi,ith,ipsi,0)), dsth(indx(iphi,ith,ipsi,0))/ maxds
  !   end do
  ! end do
  ! write(*,*) 'sumds =', sumds,2*pi*maxpsi !gpsi(indx(iphi,1,80,0))
  ! stop

end subroutine geom_diverted_dsth

subroutine geom_nearest_neighbours

  use index_function, only : indx
  use general,        only : gkw_abort

  !Dummy arrrays
  real, allocatable :: dum_a1(:), dum_a2(:)

  integer :: iphi,ith,ipsi,ipp,k, indx_tmp

  integer :: allocate_status = 0

  allocate(geom_nearest(nth,npsi_tot,4),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate geom_nearest'

  allocate(dum_a1(nth),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate dum_a1'
  allocate(dum_a2(nth),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate dum_a2'

  iphi = 1
  ipp = 0
  do ipsi = 1, npsi_tot
    do ith = 1, nth
      dum_a1(ith) = gx(indx(iphi,ith,ipsi,ipp))
      dum_a2(ith) = gy(indx(iphi,ith,ipsi,ipp))
    end do

    do ith = 1, nth
      do k = 1, 4
        indx_tmp = ((ipsi-1)*nth+ith-1)*4+k

        geom_nearest(ith,ipsi,k) = &
           & geom_nearest_point(gx(nth*npsi_tot+indx_tmp),&
           & gy(nth*npsi_tot+indx_tmp),dum_a1,dum_a2)
      end do
    end do
  end do

  ! do ith = 1, nth
  !   do ipp = -2, 2, 1
  !     select case (ipp)
  !     case (-2,1,1,2)
  !     case(0)
  !     case default
  !       call gkw_abort('geom/geom_nearest_neighours: wrong ipp')
  !     end select
  !   end do
  ! end do

  deallocate(dum_a1)
  deallocate(dum_a2)




end subroutine geom_nearest_neighbours

subroutine geom_diverted_dsthpp

  use index_function, only : indx
  use general,        only : gkw_abort
  use constants,      only : pi

  real :: tolp, tolm
  integer :: iphi,ith,ipsi,ipp,k, indx_tmp
  real gx1,gx2,gy1,gy2,gth1,gth2,gpsi1,gpsi2
  real :: sum_th
  real :: dth

  real :: d_theta

  integer :: allocate_status = 0


    ! Calculate the poloidal path length between each penetration point and its
  ! clockwise nearest poloidal neighbour

  tolm = 0.9
  tolp = 1.1

  iphi = 1
  ipp = 0

  dth = delta_theta/ndphi

  allocate(dsthpp(nth*npsi_tot*4),stat=allocate_status)
  if(allocate_status /=0) write (*,*) 'Could not allocate dsthpp'

  do ipsi = 1, npsi_tot; do ith = 1, nth
    do ipp = -2,2
      select case(ipp)
      case (-2)
        k = 4
      case(-1)
        k = 3
      case(0)
      case(1)
        k = 1
      case(2)
        k = 2
      case default
        call gkw_abort('geom: no proper ipp')
      end select

      select case(ipp)
      case(-2,-1,1,2)
        sum_th = 0.
        gx1 = gx(indx(iphi,ith,ipsi,ipp))
        gy1 = gy(indx(iphi,ith,ipsi,ipp))
        gth1 = gth(indx(iphi,ith,ipsi,ipp))
        gpsi1 = gpsi(indx(iphi,ith,ipsi,ipp))

        indx_tmp = ((ipsi-1)*nth+ith-1)*4+k
        d_theta= geom_d_theta(gth(nth*npsi_tot + indx_tmp),&
           & gth(indx(iphi,geom_nearest(ith,ipsi,k),ipsi,0)))

        dsthpp(indx_tmp) = 0.

        ! if (ipsi == 1) then
        !     if (ith == 126) then
        !       if(ipp  == -1) then
        !         ! write(*,*) sum_th, dsthpp(indx_tmp), sqrt((gx2-gx1)**2+(gy2-gy1)**2)
        !         write(*,*) sum_th, d_theta, &
        !            & gth(nth*npsi_tot + indx_tmp), &
        !            & gth(indx(iphi,geom_nearest(ith,ipsi,k),ipsi,0)), &
        !            & 2*pi -abs(gth(nth*npsi_tot + indx_tmp) - &
        !            & gth(indx(iphi,geom_nearest(ith,ipsi,k),ipsi,0)))
        !       end if
        !     end if
        !   end if


        do while(sum_th < d_theta)
          gth2 = gth1 + dth
          gpsi2 = bisection(gth2,gpsi1*tolm,gpsi1*tolp,&
             & ff(indx(iphi,ith,ipsi,ipp)))
          call geom_cartesian(gth2,gpsi2,gx2,gy2)

          ! if (ipsi == 1) then
          !   if (ith == 126) then
          !     if(ipp  == -1) then
          !       write(*,*) sum_th, dth, d_theta, dsthpp(indx_tmp), sqrt((gx2-gx1)**2+(gy2-gy1)**2)
          !     end if
          !   end if
          ! end if

          dsthpp(indx_tmp) = dsthpp(indx_tmp) + sqrt((gx2-gx1)**2+(gy2-gy1)**2)

          gx1 = gx2
          gy1 = gy2
          gth1 = gth2
          gpsi1 = gpsi2
          sum_th = sum_th + dth
        end do

        ! if (ipsi == 1) then
        !   if (ith == 126) then
        !     if (ipp == -1) then
        !       write(*,*) d_theta
        !       write(*,*) ith, geom_nearest(ith,ipsi,k), ipp,&
        !          & dsthpp(indx_tmp), &
        !          & d_theta, &
        !          & gth(nth*npsi_tot + indx_tmp), &
        !          & gth(indx(iphi,geom_nearest(ith,ipsi,k),ipsi,0))
        !       write(*,*) 'diff', pi,abs(gth(nth*npsi_tot + indx_tmp)- &
        !          & gth(indx(iphi,geom_nearest(ith,ipsi,k),ipsi,0))), &
        !          & 2*pi - abs(gth(nth*npsi_tot + indx_tmp)- &
        !          & gth(indx(iphi,geom_nearest(ith,ipsi,k),ipsi,0))), &
        !          & gth(nth*npsi_tot + indx_tmp), &
        !          & gth(indx(iphi,geom_nearest(ith,ipsi,k),ipsi,0))
        !     end if
        !   end if
        ! end if

        ! if (ipsi == 1) then
        !   if (ith == 5) then
        !     ! write(*,*) indx_tmp
        !     write(*,*) ipp, indx_tmp, d_theta,delta_theta, dsthpp(indx_tmp), &
        !        & indx(iphi,geom_nearest(ith,ipsi,k),ipsi,0),&
        !        & geom_nearest(ith,ipsi,k), &
        !        & nth*npsi_tot + indx_tmp, &
        !        & geom_d_theta(gth(nth*npsi_tot + indx_tmp),&
        !        & gth(indx(iphi,ith,ipsi,ipp))), &
        !        & gth(nth*npsi_tot + indx_tmp), &
        !        & gth(indx(iphi,ith,ipsi,0))
        !   end if
        ! end if


        !do i = 1, nth*npsi_tot*4
        ! indx_tmp = ((ipsi-1)*nth+ith-1)*4+k
        ! dsthpp(indx_tmp) = &
           ! & geom_d_theta(gth(nth*npsi_tot + indx_tmp),&
           ! & gth(indx(iphi,geom_nearest(ith,ipsi,k),ipsi,0))) * &
           ! & gpsi(nth*npsi_tot + indx_tmp)

      end select
    end do
  end do; end do
  ! write(*,*)
  ! write(*,*) geom_d_theta(gth(indx(1,1,1,0)),gth(indx(1,2,1,0)))
  

end subroutine geom_diverted_dsthpp

! !> Differentiate between real grid points (cfs and pfs) and ghost points
! subroutine geom_get_gp

!   use index_function, only : indx_gp, indx, indx_psi_pfs

!   integer :: ipsi, ith, iphi, ipp
!   !difference between two grid points in radial direction of the cfs
!   real :: dpsi_tmp

!   iphi = 1
!   ipp = 0

!   do ipsi = 1, npsi_tot
!     do ith = 1, nth
!       if (any(indx_psi_pfs == ipsi)) then
!         dpsi_tmp = abs(gpsi(indx(iphi,ith,ipsi-1,ipp)) - &
!            & gpsi(indx(iphi,ith,ipsi+1,ipp)))
!         if(dpsi_tmp &
!            & > pfs_val*delta_npsi) then
!           indx_gp(indx(iphi,ith,ipsi,ipp)) = .true.
!         else
!           indx_gp(indx(iphi,ith,ipsi,ipp)) = .false.
!         end if
!       else
!         indx_gp(indx(iphi,ith,ipsi,ipp)) = .true.
!       end if
!     end do
!   end do


! end subroutine geom_get_gp

end module geom
