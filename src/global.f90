module global

  implicit none

  private

  public :: global_init

  !> Global version of nth
  integer, public, save :: nth_global

  !> Global version of nth
  integer, public, save :: npsi_tot_global

  !
  ! Precision
  !
  !> i8 is for long integer
  integer, parameter, public :: i8 = selected_int_kind(R=18)
  !> dp is for doubles
  integer, parameter, public :: dp = kind(0.d0)
  !> rp is the default real precision which should be used almost everywhere

contains

subroutine global_init

end subroutine global_init

end module global
