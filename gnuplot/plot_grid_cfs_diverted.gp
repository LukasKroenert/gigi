
set palette defined ( 0 '#000090',\
                      1 '#000fff',\
                      2 '#0090ff',\
                      3 '#0fffee',\
                      4 '#90ff70',\
                      5 '#ffee00',\
                      6 '#ff7000',\
                      7 '#ee0000',\
                      8 '#7f0000')
                      
filename = 'geom_g_cfs.dat'
set output 'diagnostic/geom_g_cfs_diverted.eps'

set terminal postscript eps enhanced color font 'Helvetica,10'
# set view equal xyz
set size ratio -1

plot filename u 2:3:4 palette 