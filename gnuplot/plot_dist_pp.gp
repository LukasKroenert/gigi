#set cbrange[0:1.0]
#set zrange[0:1.0]

set palette defined ( 0 '#000090',\
                      1 '#000fff',\
                      2 '#0090ff',\
                      3 '#0fffee',\
                      4 '#90ff70',\
                      5 '#ffee00',\
                      6 '#ff7000',\
                      7 '#ee0000',\
                      8 '#7f0000')


set terminal postscript eps enhanced color font 'Helvetica,10'
#set terminal pngcairo size 350,262 enhanced font 'Verdana,10'

data_name = 'diagnostic/dist_ntime/dist_pp_time_1.dat'
output_name = 'diagnostic/dist_pp_time_1_3d.eps'
#output_name = 'diagnostic/dist_pp_time_1_3d.png'
set output output_name

splot data_name using 3:4:5 pt 7 ps 0.8 palette,\
       #data_name using 8:9:10 pt 7 ps 0.8 palette w lines,\
       #data_name using 13:14:15 pt 7 ps 0.8 palette w lines,\
       #data_name using 18:19:20 pt 7 ps 0.8 palette w lines,\
       #data_name using 23:24:25 pt 7 ps 0.8 palette w lines,\

splot data_name using 3:4:5 pt 7 ps 0.3 palette,\
       data_name using 8:9:10 pt 7 ps 0.3 palette,\
       data_name using 13:14:15 pt 7 ps 0.3 palette,\
       data_name using 18:19:20 pt 7 ps 0.3 palette,\
       data_name using 23:24:25 pt 7 ps 0.3 palette,\
