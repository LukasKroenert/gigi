# Animate the distribution function on the grid for all time steps

#c.f. http://www.gnuplotting.org/output-terminals/
#set terminal epslatex size 3.5,2.62 color colortext
#set output 'dist_ntime.tex'

set term gif animate
set output 'diagnostic/dist_ntime/dist_time_animation.gif'

#set cbrange[-0.2:1.2]
#set cbrange[0.:1]


#set cbrange[-gpval_z_max,gpval_z_max]

set palette defined ( 0 '#000090',\
                      1 '#000fff',\
                      2 '#0090ff',\
                      3 '#0fffee',\
                      4 '#90ff70',\
                      5 '#ffee00',\
                      6 '#ff7000',\
                      7 '#ee0000',\
                      8 '#7f0000')

ntime = 20

file_prefix = 'diagnostic/dist_ntime/dist_time_'
file_suffix = '.dat'

do for [i=0:ntime] {
   
   set title sprintf('time = %d',i)

   filename = sprintf('%s%d%s',file_prefix,i,file_suffix)

   stats filename u 3 nooutput
   absrange = abs(STATS_max)
   if(abs(STATS_min)>abs(STATS_max)){
        absrange = abs(STATS_min)
   }
   set cbrange[-absrange:absrange]

   plot filename using 1:2:3 pt 7 ps 0.5 palette
   #print filename
   }

set terminal postscript eps enhanced color font 'Helvetica,10'

do for [i=0:ntime] {
   plotname = sprintf('diagnostic/dist_ntime/dist_time_%d.eps',i)
   set output plotname
   
   set title sprintf('time = %d',i)
   filename = sprintf('%s%d%s',file_prefix,i,file_suffix)

   stats filename u 3 nooutput
   absrange = abs(STATS_max)
   if(abs(STATS_min)>abs(STATS_max)){
        absrange = abs(STATS_min)
   }
   set cbrange[-absrange:absrange]
   
   plot filename using 1:2:3 pt 7 ps 0.5 palette

   
   }

#set terminal pngcairo size 350,262 enhanced font 'Verdana,10'
#set terminal png size 400,300 enhanced font "Helvetica,20"
set terminal pngcairo size 350,200 enhanced font 'Verdana,10'

#do for [i=0:ntime] {
#   plotname = sprintf('diagnostic/dist_ntime/dist_time_%d.png',i)
#   set output plotname
#   set title sprintf('time = %d',i)
#   filename = sprintf('%s%d%s',file_prefix,i,file_suffix)
#   plot filename using 1:2:3 pt 7 ps 0.2 palette
#   }

# Create movie with mencoder
#ENCODER = system('which mencoder');
#if (strlen(ENCODER)==0) print '=== mencoder not found ==='; exit
#CMD = 'mencoder mf://*.png -mf fps=25:type=png -ovc lavc -lavcopts vcodec=m#peg4:mbd=2:trell -oac copy -o dist_ntime.avi'
#system(CMD)

# Create webm
#ENCODER = system('which ffmpeg')
#if (strlen(ENCODER)==0) print '=== ffmpeg not found, exit ==='; exit
#CMD = 'ffmpeg -i dist_ntime.avi dist_ntime.webm'
#system(CMD)

# Clear directory
#system('rm *.png')
#system('cd diagnostic/dist_ntime')
#system('mencoder mf://*.png -mf w=800:h=600:fps=25:type=png -ovc lavc -lavcopts vcodec=mpeg4:mbd=2:trell -oac copy -o output.avi')
#system('cd ../..')