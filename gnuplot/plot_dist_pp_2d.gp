data_name = 'diagnostic/dist_ntime/dist_pp_time_1.dat'
output_name = 'diagnostic/dist_pp_time_1_2d.eps'
set output output_name

set terminal postscript eps enhanced color font 'Helvetica,10'

#k = 10
k = 100
#plot for[i = 1:k]  data_name every ::(i-1)*k+1::i*k
#plot for[i = 1:k]  data_name using 3:5 every ::(i-1)*k+1::i*k pt 7 ps 0.5
#i = 1
#plot for[i = 1:k] data_name using 3:5 every ::(i-1)+1::i*k

#k = 40
#i = 25
#i=1

#set arrow 1 from 0,0 to 0,1. nohead

#plot data_name using 3:5 every ::(i-1)*k::i*k-1# ,\
   #data_name using 3:5 every ::1-1::1-1 pt 7 ps 0.6 ,\
   #data_name using 3:5 every ::k-1::k-1 pt 7 ps 0.6,\
   

k = 40
npsi =  9
plot for[i = 1:npsi]  data_name using 3:5 every ::(i-1)*k::i*k-1 pt 7 ps 0.5 t 'ipsi = '.i

ipsi1 = 7
ipsi2 = 8
ipsi3 = 9
ipsi4 = 1
ipsi5 = 2
plot for[i = ipsi1:ipsi1]  data_name using 3:5 every ::(i-1)*k::i*k-1 pt 7 ps 0.5 t 'ipsi = '.ipsi1 ,\
for[i = ipsi2:ipsi2]  data_name using 3:5 every ::(i-1)*k::i*k-1 pt 7 ps 0.5 t 'ipsi = '.ipsi2 ,\
for[i = ipsi3:ipsi3]  data_name using 3:5 every ::(i-1)*k::i*k-1 pt 7 ps 0.5 t 'ipsi = '.ipsi3 ,\
for[i = ipsi4:ipsi4]  data_name using 3:5 every ::(i-1)*k::i*k-1 pt 7 ps 0.5 t 'ipsi = '.ipsi4 ,\
for[i = ipsi5:ipsi5]  data_name using 3:5 every ::(i-1)*k::i*k-1 pt 7 ps 0.5 t 'ipsi = '.ipsi5 ,\